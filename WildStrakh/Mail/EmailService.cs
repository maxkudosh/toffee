﻿using System.Net;
using System.Net.Mail;
using Common;
using RazorEngine;
using RazorEngine.Templating;

namespace Mail
{
    public static class EmailService
    {
        public static void SendEmail(Email email)
        {
            var client = CreateSmtpClient();
            var mail = new MailMessage(Constants.SystemEmail, "dnb243@gmail.com")
            {
                Subject = "message",
                IsBodyHtml = true
            };
            var template = string.Empty;
            var model = new
            {
                Name = "Max"
            };
            var result = Engine.Razor.RunCompile(template, "key", null, model);
            mail.Body = result;
            client.SendMailAsync(mail);
        }

        public static SmtpClient CreateSmtpClient() => new SmtpClient
        {
            Host = Constants.EmailHost,
            Port = Constants.EmailPort,
            DeliveryMethod = SmtpDeliveryMethod.Network,
            UseDefaultCredentials = false,
            EnableSsl = true,
            Credentials =
                new NetworkCredential(Constants.SystemEmail, Constants.SystemEmailPassword)
        };
    }
}
