﻿using Domain.Entities.Dictionary;

namespace Mail
{
    public class Email
    {
        public string UserName
        {
            get;
            set;
        }

        public string Link
        {
            get;
            set;
        }

        public string To
        {
            get;
            set;
        }
        public DocumentStatus Status
        {
            get;
            set;
        }
    }
}
