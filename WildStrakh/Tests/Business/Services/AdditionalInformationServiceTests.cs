﻿using System.Linq;
using Business.Contracts;
using Business.Services;
using Data;
using Domain.Entities.Dictionary;
using Domain.Entities.Insurance;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tests.Business.Services
{
    [TestClass]
    public class AdditionalInformationServiceTests
    {
        private UnitOfWork _unitOfWork;

        private IAdditionalInformationService _subject;

        private InsuranceApplicationServiceTests _insuranceApplicationServiceTests;

        [TestInitialize]
        public void Initialize()
        {
            _unitOfWork = new UnitOfWork(new ApplicationDbContext());
            _subject = new AdditionalInformationService(_unitOfWork);
            _insuranceApplicationServiceTests = new InsuranceApplicationServiceTests();
            _insuranceApplicationServiceTests.Initialize();
        }

        [TestMethod]
        public void GetAllByUserId_NormalFlow_InformationReturned()
        {
            CreateInformationCorrectionWithResponse();
            var result =
                _subject.GetAllByUserId(_insuranceApplicationServiceTests.GetTestUserId(),
                    InformationType.InformationCorrection).Value.FirstOrDefault();
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void GetAllByUserIdGeneric_NormalFlow_InformationReturned()
        {
            CreateInformationCorrectionWithResponse();
            var result =
                _subject.GetAllByUserId<InsuranceApplication>(
                    _insuranceApplicationServiceTests.GetTestUserId(),
                    InformationType.InformationCorrection).Value.FirstOrDefault();
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void GetByDocumentId_NormalFlow_InformationReturned()
        {
            CreateInformationCorrectionWithResponse();
            var documentId =
                _unitOfWork.InsuranceApplicationRepository.Get().First(
                    info => info.AdditionalInformations != null).Id;
            var result =
                _subject.GetByDocumentId<InsuranceApplication>(documentId,
                    InformationType.InformationCorrection).Value;
            Assert.IsNotNull(result.First());
        }

        private void CreateInformationCorrectionWithResponse()
        {
            _insuranceApplicationServiceTests.ApplyApplication(PaymentOrder.Once);
            _insuranceApplicationServiceTests.RequestInformationCorrection();
            _insuranceApplicationServiceTests.RespondInformationCorrection(
                _unitOfWork.AdditionalInformationRepository.Get().First().Id);
        }
    }
}
