﻿using System.Linq;
using Business.Contracts;
using Business.Services;
using Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tests.Business.Services
{
    [TestClass]
    public class IllnessServiceTests
    {
        private const string IllnessNameExample = "Атипичный аутизм";

        private const int IllnessesCount = 12940;

        private IPersonIllnessService _subject;

        private UnitOfWork _unitOfWork;

        private ApplicationDbContext _context;

        [TestInitialize]
        public void Initialize()
        {
            _context = new ApplicationDbContext();
            _unitOfWork = new UnitOfWork(_context);
            _subject = new PersonIllnessService(_unitOfWork);
        }

        [TestMethod]
        public void GetAll_NormalFlow_CorrectRecordsReturned()
        {
            var result = _subject.GetIllnessesFromInitialDataFile();
            CollectionAssert.Contains(result,
                result.First(illness => illness.Name == IllnessNameExample));
            Assert.AreEqual(result.Count, IllnessesCount);
        }
    }
}
