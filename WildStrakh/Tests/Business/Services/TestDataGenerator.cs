﻿using System;
using System.Linq;
using Data;
using Domain.Entities.Dictionary;
using Models = Business.Models;

namespace Tests.Business.Services
{
    public class TestDataGenerator
    {
        private readonly Random _random;

        private readonly UnitOfWork _unitOfWork;

        public TestDataGenerator(UnitOfWork unitOfWork)
        {
            _random = new Random();
            _unitOfWork = unitOfWork;
        }

        public Models.InsuranceApplication CreateInsuranceApplicationModel(PaymentOrder order)
            => new Models.InsuranceApplication
            {
                Applicant = CreateApplicant(),
                Beneficiary = CreatePerson(),
                InsuranceSum = _random.Next(1, int.MaxValue),
                InsuranceType = InsuranceType.Life,
                PaymentOrder = order
            };

        public Models.InformationRequest CreateInformationRequest(Guid documentId)
            => new Models.InformationRequest
            {
                Content = "Request content.",
                DocumentId = documentId
            };

        public Models.InformationCorrection CreateInformationResponse()
            => new Models.InformationCorrection
            {
                Content = "Response content."
            };

        private Models.Applicant CreateApplicant() => new Models.Applicant
        {
            FirstName = Guid.NewGuid().ToString(),
            MiddleName = Guid.NewGuid().ToString(),
            LastName = Guid.NewGuid().ToString(),
            DateOfBirth = DateTime.Today,
            Passport = CreatePassport(),
            Illnesses = new[]
            {
                CreateIllness()
            },
            Occupation = Occupation.Driver,
            Address = CreateAddress()
        };

        private Models.PersonIllness CreateIllness() => new Models.PersonIllness
        {
            IllnessType = IllnessType.Past,
            Id = _unitOfWork.IllnessRepository.Get().First().Id
        };

        private Models.Person CreatePerson() => new Models.Person
        {
            FirstName = Guid.NewGuid().ToString(),
            MiddleName = Guid.NewGuid().ToString(),
            LastName = Guid.NewGuid().ToString(),
            DateOfBirth = DateTime.Today,
            Passport = CreatePassport()
        };

        private Models.Address CreateAddress() => new Models.Address
        {
            City = CreateCity(),
            AddressLine = Guid.NewGuid().ToString(),
            PostalCode = _random.Next(220000, 230000).ToString()
        };

        private static Models.City CreateCity() => new Models.City
        {
            District = Guid.NewGuid().ToString(),
            Name = Guid.NewGuid().ToString(),
            Region = Guid.NewGuid().ToString()
        };

        private Models.Passport CreatePassport() => new Models.Passport
        {
            IdentificationNumber = Guid.NewGuid().ToString(),
            Number = _random.Next(1000000, 9999999).ToString(),
            Series = "KH"
        };
    }
}
