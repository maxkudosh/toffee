﻿using System.Linq;
using Business.Contracts;
using Business.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tests.Business.Services
{
    [TestClass]
    public class PassportSeriesTests
    {
        private const int PassportSeriesCount = 16;

        private const string EnglishSeriesExample = "MP";

        private const string RussianSeriesExample = "МР";

        private IPassportService _subject;

        [TestInitialize]
        public void Initialize()
        {
            _subject = new PassportService(new Common.Cache.SearchableCacheFactory());
        }

        [TestMethod]
        public void GetAll_NormalFlow_CorrectRecordsReturned()
        {
            var result = _subject.GetAllSeries();
            CollectionAssert.Contains(
                result, result.First(city => city.Name == EnglishSeriesExample));
            CollectionAssert.Contains(
                result, result.First(city => city.Name == RussianSeriesExample));
            Assert.AreEqual(result.Count, PassportSeriesCount);
        }
    }
}
