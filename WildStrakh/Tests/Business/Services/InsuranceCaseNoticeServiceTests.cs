﻿using System;
using System.Linq;
using Business.Contracts;
using Business.Services;
using Data;
using Domain.Entities.Dictionary;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Models = Business.Models;

namespace Tests.Business.Services
{
    [TestClass]
    public class InsuranceCaseNoticeServiceTests
    {
        private IInsuranceCaseNoticeService _subject;

        private UnitOfWork _unitOfWork;

        private ContractServiceTests _contractServiceTests;

        [TestInitialize]
        public void Initialize()
        {
            _unitOfWork = new UnitOfWork(new ApplicationDbContext());
            _subject = new InsuranceCaseNoticeService(_unitOfWork,
                new AdditionalInformationService(_unitOfWork), new BankService(_unitOfWork));
            _contractServiceTests = new ContractServiceTests();
            _contractServiceTests.Initialize();
        }

        [TestMethod]
        public void Notify_NormalFlow_EntityCreated()
        {
            var contract = _contractServiceTests.CreateApprovedContract(PaymentOrder.Once);
            _subject.Notify(CreateNoticeModel(contract.Id));
            Assert.IsNotNull(
                _unitOfWork.InsuranceCaseNoticeRepository.Get(app => app.Contract.Id == contract.Id)
                    .FirstOrDefault());
        }

        [TestMethod]
        public void ReviewNotice_Reject_StatusesChanged()
        {
            var contract = _contractServiceTests.CreateApprovedContract(PaymentOrder.Once);
            _subject.Notify(CreateNoticeModel(contract.Id));
            var notice = _unitOfWork.InsuranceCaseNoticeRepository.Get().First();
            _subject.ReviewNotice(CreateNoticeReviewResult(false, notice.Id));
            Assert.AreEqual(DocumentStatus.Rejected,
                _unitOfWork.InsuranceCaseNoticeRepository.Get().First().Status);
        }

        [TestMethod]
        public void ReviewNotice_Approve_EntitiesUpdated()
        {
            var contract = _contractServiceTests.CreateApprovedContract(PaymentOrder.Once);
            _subject.Notify(CreateNoticeModel(contract.Id));
            var notice = _unitOfWork.InsuranceCaseNoticeRepository.Get().Last();
            _subject.ReviewNotice(CreateNoticeReviewResult(true, notice.Id));
            Assert.AreEqual(DocumentStatus.Approved,
                _unitOfWork.InsuranceCaseNoticeRepository.Get().Last().Status);
        }

        private static Models.NoticeReviewResult CreateNoticeReviewResult(bool shouldApprove,
            Guid id) => new Models.NoticeReviewResult
            {
                NoticeId = id,
                Payment = shouldApprove ? 50 : (decimal?) null,
                IsApproved = shouldApprove
            };

        private static Models.InsuranceCaseNotice CreateNoticeModel(Guid contractId)
            => new Models.InsuranceCaseNotice
            {
                Circumstances = "Fall in bathroom.",
                IncidentDate = DateTime.Now,
                Whereabouts = "At home",
                ContractId = contractId
            };
    }
}
