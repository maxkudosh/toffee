﻿using System.Linq;
using Business.Contracts;
using Business.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tests.Business.Services
{
    [TestClass]
    public class CityServiceTest
    {
        private const int CitiesCount = 27540;

        private const string CityNameExample = "Гродно";

        private ICityService _subject;

        [TestInitialize]
        public void Initialize()
        {
            _subject = new CityService(new Common.Cache.SearchableCacheFactory());
        }

        [TestMethod]
        public void GetAll_NormalFlow_CorrectRecordsReturned()
        {
            var result = _subject.GetAll();
            CollectionAssert.Contains(result, result.First(city => city.Name == CityNameExample));
            Assert.AreEqual(result.Count, CitiesCount);
        }
    }
}
