﻿using System;
using System.Linq;
using Business.Contracts;
using Business.Services;
using Data;
using Domain.Entities.Dictionary;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Models = Business.Models;

namespace Tests.Business.Services
{
    [TestClass]
    public class InsuranceApplicationServiceTests
    {
        private IInsuranceApplicationService _subject;

        private IPersonIllnessService _illnessService;

        private IAdditionalInformationService _additionalInformationService;

        private UnitOfWork _unitOfWork;

        private ApplicationDbContext _context;

        private TestDataGenerator _dataGenerator;

        [TestInitialize]
        public void Initialize()
        {
            _context = new ApplicationDbContext();
            _unitOfWork = new UnitOfWork(_context);
            _illnessService = new PersonIllnessService(_unitOfWork);
            _additionalInformationService = new AdditionalInformationService(_unitOfWork);
            _subject = new InsuranceApplicationService(_unitOfWork, _illnessService,
                _additionalInformationService);
            _dataGenerator = new TestDataGenerator(_unitOfWork);
        }

        [TestMethod]
        public void Apply_NormalFlow_DataCreated()
        {
            VerifyAppliedApplication(ApplyApplication(PaymentOrder.Once));
        }

        [TestMethod]
        public void GetAllByStatus_NormalFlow_ApplicationsReturned()
        {
            ApplyApplication(PaymentOrder.Once);
            var result = _subject.GetAllByStatus(DocumentStatus.PendingValidation);
            Assert.IsTrue(result.Value.Count > 0);
        }

        [TestMethod]
        public void RequestInformationCorrection_NormalFlow_AdditionalInformationEntityCreated()
        {
            ApplyApplication(PaymentOrder.Once);
            var request = RequestInformationCorrection();
            var entity = _unitOfWork.AdditionalInformationRepository.Get().FirstOrDefault();
            Assert.IsNotNull(entity);
            Assert.AreEqual(request.Content, entity.Request);
        }

        [TestMethod]
        public void RespondInformationCorrection_NormalFlow_AdditionalInformationEntityUpdated()
        {
            ApplyApplication(PaymentOrder.Once);
            RequestInformationCorrection();
            var entity = _unitOfWork.AdditionalInformationRepository.Get().First();
            Assert.AreEqual(RespondInformationCorrection(entity.Id).Content, entity.Response);
            Assert.IsNotNull(entity.ResponseDate);
        }

        public Models.InformationCorrection RespondInformationCorrection(
            Guid additionalInformationId)
        {
            var response = _dataGenerator.CreateInformationResponse();
            _subject.RespondInformationCorrection(additionalInformationId, response);
            return response;
        }

        public Models.InformationRequest RequestInformationCorrection()
        {
            var documentId = _unitOfWork.InsuranceApplicationRepository.Get().First().Id;
            var request = _dataGenerator.CreateInformationRequest(documentId);
            _subject.RequestInformationCorrection(request);
            return request;
        }

        public Models.InsuranceApplication ApplyApplication(PaymentOrder order)
        {
            var userId = GetTestUserId();
            var model = _dataGenerator.CreateInsuranceApplicationModel(order);
            _subject.Apply(userId, model);
            return model;
        }

        public Guid GetTestUserId()
            => Guid.Parse(_unitOfWork.GetUser(user => user.Email == "client1@gmail.com").First().Id);

        private void VerifyAppliedApplication(Models.InsuranceApplication model)
        {
            Assert.IsNotNull(_unitOfWork.InsuranceApplicationRepository.GetByInsuranceSum(model.InsuranceSum));
            Assert.IsNotNull(_unitOfWork.PersonRepository.Get(model.Applicant.FirstName));
            Assert.IsNotNull(_unitOfWork.PersonRepository.Get(model.Beneficiary.FirstName));
            Assert.IsNotNull(_unitOfWork.AddressRepository.Get(model.Applicant.Address.AddressLine));
            Assert.IsNotNull(
                _unitOfWork.PassportRepository.Get(model.Applicant.Passport.IdentificationNumber));
            Assert.IsNotNull(
                _unitOfWork.PassportRepository.Get(model.Beneficiary.Passport.IdentificationNumber));
            Assert.IsNotNull(_unitOfWork.IllnessRepository.Get(model.Applicant.Illnesses.First().Id));
            Assert.IsNotNull(
                _unitOfWork.PersonIllnessRepository.GetByIllnessIdAndPersonName(
                    model.Applicant.Illnesses.First().Id, model.Applicant.FirstName));
        }
    }
}
