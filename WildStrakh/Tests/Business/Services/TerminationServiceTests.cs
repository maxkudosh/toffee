﻿using System;
using System.Linq;
using Business.Contracts;
using Business.Services;
using Data;
using Domain.Entities.Dictionary;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Models = Business.Models;

namespace Tests.Business.Services
{
    [TestClass]
    public class TerminationServiceTests
    {
        private ITerminationService _subject;

        private UnitOfWork _unitOfWork;

        private ContractServiceTests _contractServiceTests;

        [TestInitialize]
        public void Initialize()
        {
            _unitOfWork = new UnitOfWork(new ApplicationDbContext());
            _subject = new TerminationService(_unitOfWork,
                new AdditionalInformationService(_unitOfWork));
            _contractServiceTests = new ContractServiceTests();
            _contractServiceTests.Initialize();
        }

        [TestMethod]
        public void Apply_NormalFlow_EntityCreated()
        {
            var contract = _contractServiceTests.CreateApprovedContract(PaymentOrder.Once);
            _subject.Apply(CreateTerminationApplicationModel(contract.Id));
            Assert.IsNotNull(
                _unitOfWork.TerminationApplicationRepository.Get(
                    app => app.Contract.Id == contract.Id).FirstOrDefault());
        }

        [TestMethod]
        public void Reject_NormalFlow_StatusesChanged()
        {
            var contract = _contractServiceTests.CreateApprovedContract(PaymentOrder.Once);
            _subject.Apply(CreateTerminationApplicationModel(contract.Id));
            var application = _unitOfWork.TerminationApplicationRepository.Get().First();
            _subject.Reject(application.Id);
            Assert.AreEqual(DocumentStatus.Rejected,
                _unitOfWork.TerminationApplicationRepository.Get().First().Status);
        }

        [TestMethod]
        public void Terminate_NormalFlow_EntitiesUpdated()
        {
            var contract = _contractServiceTests.CreateApprovedContract(PaymentOrder.Once);
            _subject.Apply(CreateTerminationApplicationModel(contract.Id));
            var application = _unitOfWork.TerminationApplicationRepository.Get().Last();
            _subject.Terminate(application.Id);
            Assert.AreEqual(DocumentStatus.Approved,
                _unitOfWork.TerminationApplicationRepository.Get().Last().Status);
        }

        private static Models.TerminationApplication CreateTerminationApplicationModel(
            Guid contractId) => new Models.TerminationApplication
            {
                Comment = "Terminate this",
                ContractId = contractId
            };
    }
}
