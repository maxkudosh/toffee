﻿using System;
using System.Collections.Generic;
using System.Linq;
using Business.Contracts;
using Business.Services;
using Common;
using Data;
using Domain.Entities.Dictionary;
using Domain.Entities.Insurance;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Models = Business.Models;

namespace Tests.Business.Services
{
    [TestClass]
    public class ContractServiceTests
    {
        private const decimal Amount = 15;

        private IContractService _subject;

        private UnitOfWork _unitOfWork;

        private InsuranceApplicationServiceTests _insuranceApplicationServiceTests;

        [TestInitialize]
        public void Initialize()
        {
            _unitOfWork = new UnitOfWork(new ApplicationDbContext());
            _subject = new ContractService(_unitOfWork, new BankService(_unitOfWork));
            _insuranceApplicationServiceTests = new InsuranceApplicationServiceTests();
            _insuranceApplicationServiceTests.Initialize();
        }

        [TestMethod]
        public void GenerateContract_NormalFlow_ContractCreated()
        {
            GenerateContractCandidates(PaymentOrder.Once);
            Assert.IsNotNull(_unitOfWork.ContractRepository.Get().First());
            Assert.AreEqual(DocumentStatus.PendingApproval,
                _unitOfWork.InsuranceApplicationRepository.Get().Last().Status);
        }

        [TestMethod]
        public void ApproveContract_NormalFlow_OneContractApprovedAndOthersCancelled()
        {
            var contract = CreateApprovedContract(PaymentOrder.Once);
            ValidateContractApprovalResults(contract);
        }

        [TestMethod]
        public void RejectInsuranceApplication_NormalFlow_StatusesUpdated()
        {
            GenerateContractCandidates(PaymentOrder.Once);
            var application = _unitOfWork.InsuranceApplicationRepository.Get().Last();
            _subject.RejectInsuranceApplication(application.Id);
            ValidateContractRejection(application);
        }

        [TestMethod]
        public void MakeFirstPayment_PeriodicPayments_TransactionsCreated()
        {
            var contract = CreateApprovedContract(PaymentOrder.Monthly);
            _subject.MakeFirstPayment(contract.Id, CreateCard());
            var transactions = _unitOfWork.TransactionRepository.Get().ToList();
            Assert.IsTrue(transactions.Count >= 23);
        }

        public Contract CreateApprovedContract(PaymentOrder order)
        {
            GenerateContractCandidates(order);
            var contract = _unitOfWork.ContractRepository.Get().Last();
            _subject.ApproveContract(CreateContractApprovalModel(contract.Id));
            return contract;
        }

        private static Models.BankCard CreateCard() => new Models.BankCard
        {
            ExpirationDate = new Models.ExpirationDate
            {
                Year = 2018,
                Month = 9
            },
            CardholderName = "Cardholder Name",
            CardNumber = "4444000044440000",
            Cvc = 453
        };

        private static void ValidateContractRejection(InsuranceApplication application)
        {
            Assert.AreEqual(DocumentStatus.Rejected, application.Status);
            foreach (var contractVariant in application.ContractVariants)
            {
                Assert.AreEqual(ContractStatus.Cancelled, contractVariant.Status);
            }
        }

        private void ValidateContractApprovalResults(Contract contract)
        {
            Assert.AreEqual(ContractStatus.PendingPayment,
                _unitOfWork.ContractRepository.GetById(contract.Id).Status);
            foreach (var otherContract in _unitOfWork.ContractRepository.Get().Reverse().Skip(1))
            {
                Assert.AreNotEqual(ContractStatus.PendingPayment, otherContract.Status);
            }
            Assert.AreEqual(DocumentStatus.Approved,
                _unitOfWork.InsuranceApplicationRepository.GetById(contract.InsuranceApplication.Id)
                    .Status);
        }

        private static Models.Contract CreateContractApprovalModel(Guid id) => new Models.Contract
        {
            Amount = Amount,
            EndDate = DateTime.Now.AddYears(2),
            Id = id,
            Premium = Amount,
            StartDate = DateTime.Now
        };

        private void GenerateContractCandidates(PaymentOrder order)
        {
            _insuranceApplicationServiceTests.ApplyApplication(order);
            _subject.GenerateContractCandidates(
                CreateAnswering(_unitOfWork.InsuranceApplicationRepository.Get().Last().Id));
        }

        private static Models.QuestionsAnswering CreateAnswering(Guid applicationId)
            => new Models.QuestionsAnswering
            {
                InsuranceApplicationId = applicationId,
                Answers = CreateAnswers()
            };

        private static List<Models.QuestionAnswer> CreateAnswers()
            => new List<Models.QuestionAnswer>
            {
                new Models.QuestionAnswer
                {
                    Answer = 1,
                    Question = Constants.OccupationDangerQuestion
                },
                new Models.QuestionAnswer
                {
                    Answer = 1,
                    Question = Constants.IllnessSeverityQuestion
                }
            };
    }
}
