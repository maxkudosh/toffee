﻿namespace Common
{
    public static class Constants
    {
        public const int PassportSeriesLength = 2;

        public const int PassportNumberLength = 7;

        public const int PostalCodeLength = 6;

        public const string ConnectionName = "Wild";

        public const string AdminRoleName = "Admin";

        public const string UnderwriterRoleName = "Underwriter";

        public const string SecretaryRoleName = "Secretary";

        public const string ClientRoleName = "Client";

        public const string PdfMimeType = "application/pdf";

        public const string OccupationDangerQuestion = "Оцените опасность профессии";

        public const string IllnessSeverityQuestion = "Оцените серьёзность болезни";

        public const decimal SystemAccountStartBalance = 100000000000;

        public const decimal PersonAccountStartBalance = 10000000;

        public const string NotEnoughMoneyMessage = "Недостаточно денег.";

        public const decimal StartAmount = 360000000;

        public const string SystemEmail = "pekaretz@outlook.com";

        public const string SystemEmailPassword = "Wildpassword1";

        public const string EmailHost = "smtp.live.com";

        public const int EmailPort = 587;
    }
}
