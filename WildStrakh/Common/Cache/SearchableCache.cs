﻿using System;
using System.Collections.Generic;
using System.Runtime.Caching;

namespace Common.Cache
{
    public class SearchableCache<T>
    {

        private readonly string _cacheKey;
        private readonly MemoryCache _cache;
        private readonly Func<List<T>> _initialValuesGetter;

        public SearchableCache(Func<List<T>> initialValuesGetter)
        {
            _initialValuesGetter = initialValuesGetter;
            _cacheKey = Guid.NewGuid().ToString();
            _cache= MemoryCache.Default;
        }

        public List<T> GetAll()
        {
            EnsureCityCacheInitialized();
            return (List<T>)_cache.Get(_cacheKey);
        }

        public List<T> GetByPredicate(Func<T, bool> filterPredicate)
        {
            EnsureCityCacheInitialized();
            return GetAll().FindAll(i => filterPredicate(i));
        }

        private void EnsureCityCacheInitialized()
        {
            if (!_cache.Contains(_cacheKey))
            {
                _cache.Add(_cacheKey, _initialValuesGetter(), DateTimeOffset.MaxValue);
            }
        }

    }
}
