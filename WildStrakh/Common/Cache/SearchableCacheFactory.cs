﻿using System;
using System.Collections.Generic;

namespace Common.Cache
{
    public class SearchableCacheFactory
    {
        public SearchableCache<T> Create<T>(Func<List<T>> initialValuesGetter)
        {
            return new SearchableCache<T>(initialValuesGetter);
        }
    }
}
