﻿using Domain.Entities.Dictionary;

namespace Domain.Entities
{
    public class UserFile : Entity
    {
        public byte[] Content
        {
            get;
            set;
        }

        public virtual UserFileType Type
        {
            get;
            set;
        }
    }
}
