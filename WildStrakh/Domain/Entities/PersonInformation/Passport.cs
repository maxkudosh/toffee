﻿namespace Domain.Entities.PersonInformation
{
    public class Passport : Entity
    {
        public string Series
        {
            get;
            set;
        }

        public int SerialNumber
        {
            get;
            set;
        }

        public string IdentificationNumber
        {
            get;
            set;
        }
    }
}
