﻿using System;
using Domain.Entities.Dictionary;

namespace Domain.Entities.PersonInformation
{
    public class Person : Entity
    {
        public string FirstName
        {
            get;
            set;
        }

        public string LastName
        {
            get;
            set;
        }

        public string MiddleName
        {
            get;
            set;
        }

        public DateTime DateOfBirth
        {
            get;
            set;
        }

        public virtual Passport Passport
        {
            get;
            set;
        }

        public virtual Address Address
        {
            get;
            set;
        }

        public virtual Occupation Occupation
        {
            get;
            set;
        }

        public string OccupationDetails
        {
            get;
            set;
        }

        public virtual User User
        {
            get;
            set;
        }
    }
}
