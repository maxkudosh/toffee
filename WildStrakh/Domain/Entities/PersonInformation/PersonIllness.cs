﻿using Domain.Entities.Dictionary;

namespace Domain.Entities.PersonInformation
{
    public class PersonIllness : Entity
    {
        public virtual Person Person
        {
            get;
            set;
        }

        public virtual Illness Illness
        {
            get;
            set;
        }

        public virtual IllnessType IllnessType
        {
            get;
            set;
        }
    }
}
