﻿namespace Domain.Entities.PersonInformation
{
    public class Address : Entity
    {
        public string AddressLine
        {
            get;
            set;
        }

        public string City
        {
            get;
            set;
        }

        public string Region
        {
            get;
            set;
        }

        public string District
        {
            get;
            set;
        }

        public int PostalCode
        {
            get;
            set;
        }
    }
}
