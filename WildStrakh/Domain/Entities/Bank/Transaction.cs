﻿using System;
using Domain.Entities.Dictionary;

namespace Domain.Entities.Bank
{
    public class Transaction : Entity
    {
        public PaymentOrder Type
        {
            get;
            set;
        }

        public virtual Account SourceAccount
        {
            get;
            set;
        }

        public virtual Account TargetAccount
        {
            get;
            set;
        }

        public DateTime TransactionDate
        {
            get;
            set;
        }

        public decimal Amount
        {
            get;
            set;
        }

        public virtual TransactionStatus Status
        {
            get;
            set;
        }

        public DateTime PlannedDate
        {
            get;
            set;
        }
    }
}
