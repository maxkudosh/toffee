﻿using System;
using Domain.Entities.Dictionary;
using Domain.Entities.PersonInformation;

namespace Domain.Entities.Bank
{
    public class Account : Entity
    {
        public virtual AccountType AccountType
        {
            get;
            set;
        }

        public decimal Balance
        {
            get;
            set;
        }

        public virtual Person Owner
        {
            get;
            set;
        }

        public string CardNumber
        {
            get;
            set;
        }

        public string CardholderName
        {
            get;
            set;
        }

        public DateTime ExpirationDate
        {
            get;
            set;
        }

        public int Cvc
        {
            get;
            set;
        }
    }
}
