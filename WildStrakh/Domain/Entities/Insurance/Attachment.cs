﻿namespace Domain.Entities.Insurance
{
    public class Attachment : Entity
    {
        public virtual UserFile File
        {
            get;
            set;
        }

        public string Description
        {
            get;
            set;
        }

        public virtual AdditionalInformation AdditionalInformation
        {
            get;
            set;
        }
    }
}
