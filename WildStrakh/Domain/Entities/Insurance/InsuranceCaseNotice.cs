﻿namespace Domain.Entities.Insurance
{
    public class InsuranceCaseNotice : StatementDocument
    {
        public string Place
        {
            get;
            set;
        }

        public string Description
        {
            get;
            set;
        }

        public decimal? Payment
        {
            get;
            set;
        }
    }
}
