﻿using System.Collections.Generic;
using Domain.Entities.Dictionary;
using Domain.Entities.PersonInformation;

namespace Domain.Entities.Insurance
{
    public class InsuranceApplication : StatementDocument
    {
        public virtual Person Applicant
        {
            get;
            set;
        }

        public virtual Person Beneficiary
        {
            get;
            set;
        }

        public override Person Person => Applicant;

        public virtual InsuranceType InsuranceType
        {
            get;
            set;
        }

        public virtual PaymentOrder PaymentOrder
        {
            get;
            set;
        }

        public decimal? InsuranceSum
        {
            get;
            set;
        }

        public virtual ICollection<Contract> ContractVariants
        {
            get;
            set;
        }
    }
}
