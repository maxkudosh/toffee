﻿namespace Domain.Entities.Insurance
{
    public class TerminationApplication : StatementDocument
    {
        public string Comment
        {
            get;
            set;
        }
    }
}
