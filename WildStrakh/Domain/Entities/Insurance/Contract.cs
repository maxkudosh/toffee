﻿using System;
using System.ComponentModel.DataAnnotations;
using Domain.Entities.Dictionary;
using Domain.Entities.PersonInformation;

namespace Domain.Entities.Insurance
{
    public class Contract : Entity
    {
        public virtual Person Person
        {
            get;
            set;
        }

        public virtual Person Beneficiary
        {
            get;
            set;
        }

        public DateTime StartDate
        {
            get;
            set;
        }

        public DateTime EndDate
        {
            get;
            set;
        }

        public decimal Amount
        {
            get;
            set;
        }

        public decimal Premium
        {
            get;
            set;
        }

        public virtual ContractStatus Status
        {
            get;
            set;
        }

        public InsuranceType InsuranceType
        {
            get;
            set;
        }

        public PaymentOrder PaymentOrder
        {
            get;
            set;
        }

        public DateTime StatusChangedDate
        {
            get;
            set;
        }

        [Required]
        public virtual InsuranceApplication InsuranceApplication
        {
            get;
            set;
        }

        public bool PaymentsPlanned
        {
            get;
            set;
        }
    }
}
