﻿using System;
using System.Collections.Generic;
using Domain.Contracts;
using Domain.Entities.Dictionary;
using Domain.Entities.PersonInformation;

namespace Domain.Entities.Insurance
{
    public class StatementDocument : Entity, IStatementDocument
    {
        public DateTime CreatedDate
        {
            get;
            set;
        }

        public DateTime ActualDate
        {
            get;
            set;
        }

        public virtual Person Person => Contract.Person;

        public virtual Contract Contract
        {
            get;
            set;
        }

        public virtual DocumentStatus Status
        {
            get;
            set;
        }

        public DateTime StatusChangedDate
        {
            get;
            set;
        }

        public virtual ICollection<AdditionalInformation> AdditionalInformations
        {
            get;
            set;
        }

        public virtual ICollection<Attachment> Attachments
        {
            get;
            set;
        }
    }
}
