﻿using System;
using System.Collections.Generic;
using Domain.Entities.Dictionary;

namespace Domain.Entities.Insurance
{
    public class AdditionalInformation : Entity
    {
        public string Request
        {
            get;
            set;
        }

        public DateTime RequestDate
        {
            get;
            set;
        }

        public string Response
        {
            get;
            set;
        }

        public DateTime? ResponseDate
        {
            get;
            set;
        }

        public virtual InformationType InformationType
        {
            get;
            set;
        }

        public virtual InsuranceCaseNotice InsuranceCaseNotice
        {
            get;
            set;
        }

        public virtual InsuranceApplication InsuranceApplication
        {
            get;
            set;
        }

        public virtual TerminationApplication TerminationApplication
        {
            get;
            set;
        }

        public virtual ICollection<Attachment> Attachments
        {
            get;
            set;
        }
    }
}
