﻿namespace Domain.Entities.Dictionary
{
    public enum InformationType
    {
        AdditionalInformation = 0,

        InformationCorrection = 1
    }
}
