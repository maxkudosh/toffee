﻿using Domain.Attributes;

namespace Domain.Entities.Dictionary
{
    public enum Occupation
    {
        AdministrativeJudicialOfficers = 0,

        Artist = 1,

        Architect = 2,

        Banker = 3,

        Unemployed = 4,

        Driver = 5,

        Official = 6,

        [DangerousOccupation]
        Diplomat = 7,

        Clergyman = 8,

        Journalist = 9,

        Informatic = 10,

        Other = 11,

        TradeMarketing = 12,

        [DangerousOccupation]
        Medic = 13,

        Manager = 14,

        Beauty = 15,

        [DangerousOccupation]
        Sailor = 16,

        Researcher = 17,

        Pensioner = 18,

        [DangerousOccupation]
        Politician = 19,

        [DangerousOccupation]
        Police = 20,

        [DangerousOccupation]
        Soldier = 21,

        Entrepreneur = 22,

        Craftsman = 23,

        [DangerousOccupation]
        Athlete = 24,

        Student = 25,

        Technician = 26,

        MentalWorker = 27,

        Teacher = 28,

        Farmer = 29,

        [DangerousOccupation]
        PhysicalWorker = 30,

        [DangerousOccupation]
        Chemist = 31,

        [DangerousOccupation]
        Electrician = 32,

        Lawyer = 33
    }
}
