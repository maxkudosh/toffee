﻿namespace Domain.Entities.Dictionary
{
    public enum DocumentStatus
    {
        PendingValidation = 0,

        Validated = 1,

        PendingCorrections = 2,

        Processed = 3,

        PendingApproval = 4,

        PendingAdditionalInformation = 5,

        Approved = 6,

        Rejected = 7,

        Closed = 8
    }
}
