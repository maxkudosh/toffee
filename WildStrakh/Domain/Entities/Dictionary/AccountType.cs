﻿namespace Domain.Entities.Dictionary
{
    public enum AccountType
    {
        System = 0,

        User = 1
    }
}
