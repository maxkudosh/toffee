﻿namespace Domain.Entities.Dictionary
{
    public enum ContractStatus
    {
        Created = 0,

        PendingPayment = 1,

        Cancelled = 2,

        Active = 3,

        Terminated = 4
    }
}
