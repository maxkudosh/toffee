﻿namespace Domain.Entities.Dictionary
{
    public enum PaymentOrder
    {
        Once = 0,

        Monthly = 1,

        Yearly = 2
    }
}
