﻿namespace Domain.Entities.Dictionary
{
    public enum InsuranceType
    {
        Life = 0,

        Health = 1
    }
}
