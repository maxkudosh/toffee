﻿namespace Domain.Entities.Dictionary
{
    public enum UserFileType
    {
        Attachment = 0,

        Contract = 1
    }
}
