﻿namespace Domain.Entities.Dictionary
{
    public enum TransactionStatus
    {
        Waiting = 0,

        Completed = 1
    }
}
