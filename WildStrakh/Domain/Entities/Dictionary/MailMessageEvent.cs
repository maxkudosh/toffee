﻿namespace Domain.Entities.Dictionary
{
    public enum MailMessageEvent
    {
        SuccessfulRegistrationEvent = 0,

        InsuranceApplicationEvent = 1,

        TerminationApplicationEvent = 2,

        InsuranceCaseNoticeEvent = 3,

        InsurancePaymentEvent = 4
    }
}
