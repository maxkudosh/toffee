﻿namespace Domain.Entities.Dictionary
{
    public enum IllnessType
    {
        Inherited = 0,

        Chronic = 1,

        Past = 2
    }
}
