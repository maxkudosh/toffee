﻿using System.Collections.Generic;
using Common;
using Domain.Entities.Insurance;

namespace Domain.Questions
{
    public class IllnessSeverityQuestion : IQuestion
    {
        public string Content => Constants.IllnessSeverityQuestion;

        public Dictionary<int, string> Answer
        {
            get;
            set;
        }

        public Dictionary<int, string> Answers => new Dictionary<int, string>
        {
            {
                10, "Довольно-таки серьёзная"
            },
            {
                6, "Средне"
            },
            {
                2, "Ниже среднего"
            },
            {
                0, "Не стоит беспокоиться"
            }
        };

        public bool ShouldAsk(InsuranceApplication application) => true;
    }
}
