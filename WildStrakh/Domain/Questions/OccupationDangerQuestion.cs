﻿using System.Collections.Generic;
using Common;
using Domain.Entities.Insurance;

namespace Domain.Questions
{
    public class OccupationDangerQuestion : IQuestion
    {
        public string Content => Constants.OccupationDangerQuestion;

        public Dictionary<int, string> Answer
        {
            get;
            set;
        }

        public Dictionary<int, string> Answers => new Dictionary<int, string>
        {
            {
                10, "Довольно-таки опасно"
            },
            {
                6, "Средне"
            },
            {
                2, "Ниже среднего"
            },
            {
                0, "Не стоит беспокоиться"
            }
        };

        public bool ShouldAsk(InsuranceApplication application)
            => !string.IsNullOrEmpty(application.Person.OccupationDetails);
    }
}
