﻿using System.Collections.Generic;
using Domain.Entities.Insurance;

namespace Domain.Questions
{
    public interface IQuestion
    {
        string Content
        {
            get;
        }

        Dictionary<int, string> Answer
        {
            get;
            set;
        }

        Dictionary<int, string> Answers
        {
            get;
        }

        bool ShouldAsk(InsuranceApplication application);
    }
}
