﻿using System;
using System.Collections.Generic;
using Domain.Entities.Dictionary;
using Domain.Entities.Insurance;

namespace Domain.Contracts
{
    public interface IStatementDocument : IEntity
    {
        DateTime CreatedDate
        {
            get;
            set;
        }

        DateTime ActualDate
        {
            get;
            set;
        }

        Contract Contract
        {
            get;
            set;
        }

        DocumentStatus Status
        {
            get;
            set;
        }

        DateTime StatusChangedDate
        {
            get;
            set;
        }

        ICollection<AdditionalInformation> AdditionalInformations
        {
            get;
            set;
        }

        ICollection<Attachment> Attachments
        {
            get;
            set;
        }
    }
}
