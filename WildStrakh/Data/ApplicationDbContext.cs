﻿using System.Data.Entity;
using Common;
using Domain.Entities;
using Domain.Entities.Bank;
using Domain.Entities.Insurance;
using Domain.Entities.PersonInformation;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Data
{
    public class ApplicationDbContext : IdentityDbContext<User>
    {
        public DbSet<AdditionalInformation> AdditionalInformations
        {
            get;
            set;
        }

        public DbSet<Attachment> Attachments
        {
            get;
            set;
        }

        public DbSet<Contract> Contracts
        {
            get;
            set;
        }

        public DbSet<InsuranceApplication> InsuranceApplications
        {
            get;
            set;
        }

        public DbSet<InsuranceCaseNotice> InsuranceCaseNotices
        {
            get;
            set;
        }

        public DbSet<TerminationApplication> TerminationApplications
        {
            get;
            set;
        }

        public DbSet<Address> Addresses
        {
            get;
            set;
        }

        public DbSet<Illness> Illnesses
        {
            get;
            set;
        }

        public DbSet<Passport> Passports
        {
            get;
            set;
        }

        public DbSet<Person> Persons
        {
            get;
            set;
        }

        public DbSet<PersonIllness> PersonIllnesses
        {
            get;
            set;
        }

        public DbSet<UserFile> UserFiles
        {
            get;
            set;
        }

        public DbSet<Account> Accounts
        {
            get;
            set;
        }

        public DbSet<Transaction> Transactions
        {
            get;
            set;
        }

        public ApplicationDbContext() : base(Constants.ConnectionName)
        {
            Database.SetInitializer(new ApplicationDbInitializer());
        }
    }
}
