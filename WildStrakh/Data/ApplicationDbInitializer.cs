﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using Domain.Entities;
using Domain.Entities.Bank;
using Domain.Entities.Dictionary;
using Domain.Entities.PersonInformation;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Newtonsoft.Json;
using Constants = Common.Constants;

namespace Data
{
    public class ApplicationDbInitializer : DropCreateDatabaseIfModelChanges<ApplicationDbContext>
    {
        internal class IllnessModel
        {
            public string Name
            {
                get;
                set;
            }
        }

        protected override void Seed(ApplicationDbContext context)
        {
            AddRoles(context);
            AddUsers(context);
            CreateIllnesses(context);
            CreateSystemBankAccount(context);
            base.Seed(context);
        }

        private static void CreateSystemBankAccount(ApplicationDbContext context)
        {
            var account = GetNewSystemAccount();
            context.Accounts.Add(account);
        }

        private static Account GetNewSystemAccount() => new Account
        {
            Id = Guid.NewGuid(),
            AccountType = AccountType.System,
            Balance = Constants.SystemAccountStartBalance,
            ExpirationDate = DateTime.MaxValue
        };

        private static void CreateIllnesses(ApplicationDbContext context)
            =>
                context.Illnesses.AddRange(
                    GetIllnessesFromInitialDataFile().Select(CreateIllnessEntity).ToList());

        private static Illness CreateIllnessEntity(IllnessModel model) => new Illness
        {
            Id = Guid.NewGuid(),
            Name = model.Name
        };

        private static IEnumerable<IllnessModel> GetIllnessesFromInitialDataFile()
            =>
                JsonConvert.DeserializeObject<List<IllnessModel>>(
                    Encoding.UTF8.GetString(Common.Properties.Resources.Illness).Remove(0, 1));

        private static void AddUsers(ApplicationDbContext context)
        {
            CreateUser(context, "admin1@gmail.com", Constants.AdminRoleName);
            CreateUser(context, "underwriter1@gmail.com", Constants.UnderwriterRoleName);
            CreateUser(context, "secretary1@gmail.com", Constants.SecretaryRoleName);
            CreateUser(context, "client1@gmail.com", Constants.ClientRoleName);
        }

        private static void CreateUser(ApplicationDbContext context, string email,
            params string[] roles)
        {
            var user = CreateApplicationUser(email);
            foreach (var role in roles)
            {
                AddUserToRole(context, user, role);
            }
            context.Users.Add(user);
        }

        private static void AddUserToRole(ApplicationDbContext context, User user,
            string roleName)
        {
            user.Roles.Add(new IdentityUserRole
            {
                RoleId = context.Roles.First(role => role.Name == roleName).Id,
                UserId = user.Id
            });
        }

        private static User CreateApplicationUser(string email) => new User
        {
            UserName = email,
            Email = email,
            EmailConfirmed = true,
            PasswordHash = new PasswordHasher().HashPassword("1"),
            SecurityStamp = Guid.NewGuid().ToString()
        };

        private static void AddRoles(ApplicationDbContext context)
        {
            context.Roles.Add(new IdentityRole(Constants.AdminRoleName));
            context.Roles.Add(new IdentityRole(Constants.UnderwriterRoleName));
            context.Roles.Add(new IdentityRole(Constants.SecretaryRoleName));
            context.Roles.Add(new IdentityRole(Constants.ClientRoleName));
            context.SaveChanges();
        }
    }
}
