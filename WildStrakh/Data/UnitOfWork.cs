﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Data.Repositories;
using Data.Repositories.Bank;
using Data.Repositories.Insurance;
using Data.Repositories.PersonInformation;
using Domain.Entities;

namespace Data
{
    public class UnitOfWork : IDisposable
    {
        private readonly ApplicationDbContext _context;

        private bool _disposed;

        private AdditionalInformationRepository _additionalInformationRepository;

        private AttachmentRepository _attachmentRepository;

        private ContractRepository _contractRepository;

        private InsuranceApplicationRepository _insuranceApplicationRepository;

        private InsuranceCaseNoticeRepository _insuranceCaseNoticeRepository;

        private TerminationApplicationRepository _terminationApplicationRepository;

        private AddressRepository _addressRepository;

        private IllnessRepository _illnessRepository;

        private PassportRepository _passportRepository;

        private PersonIllnessRepository _personIllnessRepository;

        private PersonRepository _personRepository;

        private UserFileRepository _userFileRepository;

        private AccountRepository _accountRepository;

        private TransactionRepository _transactionRepository;

        public AdditionalInformationRepository AdditionalInformationRepository
            =>
                _additionalInformationRepository ??
                    (_additionalInformationRepository =
                        new AdditionalInformationRepository(_context));

        public AttachmentRepository AttachmentRepository
            => _attachmentRepository ?? (_attachmentRepository = new AttachmentRepository(_context))
            ;

        public ContractRepository ContractRepository
            => _contractRepository ?? (_contractRepository = new ContractRepository(_context));

        public InsuranceApplicationRepository InsuranceApplicationRepository
            =>
                _insuranceApplicationRepository ??
                    (_insuranceApplicationRepository = new InsuranceApplicationRepository(_context))
            ;

        public InsuranceCaseNoticeRepository InsuranceCaseNoticeRepository
            =>
                _insuranceCaseNoticeRepository ??
                    (_insuranceCaseNoticeRepository = new InsuranceCaseNoticeRepository(_context));

        public TerminationApplicationRepository TerminationApplicationRepository
            =>
                _terminationApplicationRepository ??
                    (_terminationApplicationRepository =
                        new TerminationApplicationRepository(_context));

        public AddressRepository AddressRepository
            => _addressRepository ?? (_addressRepository = new AddressRepository(_context));

        public IllnessRepository IllnessRepository
            => _illnessRepository ?? (_illnessRepository = new IllnessRepository(_context));

        public PassportRepository PassportRepository
            => _passportRepository ?? (_passportRepository = new PassportRepository(_context));

        public PersonIllnessRepository PersonIllnessRepository
            =>
                _personIllnessRepository ??
                    (_personIllnessRepository = new PersonIllnessRepository(_context));

        public PersonRepository PersonRepository
            => _personRepository ?? (_personRepository = new PersonRepository(_context));

        public UserFileRepository UserFileRepository
            => _userFileRepository ?? (_userFileRepository = new UserFileRepository(_context));

        public AccountRepository AccountRepository
            => _accountRepository ?? (_accountRepository = new AccountRepository(_context));

        public TransactionRepository TransactionRepository
            =>
                _transactionRepository ??
                    (_transactionRepository = new TransactionRepository(_context));

        public UnitOfWork(ApplicationDbContext context)
        {
            _context = context;
        }

        public Repository<T> Repository<T>() where T : Entity
            =>
                (Repository<T>)
                    GetType().GetProperties().First(
                        p => p.PropertyType.IsSubclassOf(typeof (Repository<T>))).GetValue(this);

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        public IEnumerable<User> GetUser(Expression<Func<User, bool>> filter = null,
            Func<IQueryable<User>, IOrderedQueryable<User>> orderBy = null)
        {
            IQueryable<User> query = _context.Set<User>();
            if (filter != null)
            {
                query = query.Where(filter);
            }
            return orderBy?.Invoke(query).ToList() ?? query.ToList();
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed && disposing)
            {
                _context.Dispose();
            }
            _disposed = true;
        }
    }
}
