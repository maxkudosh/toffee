﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Entities.Dictionary;
using Domain.Entities.Insurance;

namespace Data.Repositories.Insurance
{
    public class TerminationApplicationRepository : Repository<TerminationApplication>
    {
        public TerminationApplicationRepository(ApplicationDbContext context) : base(context)
        {
        }

        public TerminationApplication GetById(Guid id) => Get(app => app.Id == id).FirstOrDefault();

        public IEnumerable<TerminationApplication> GetByUserId(Guid userId)
            => Get(n => n.Person.User.Id == userId.ToString());

        public IEnumerable<TerminationApplication> GetByStatus(DocumentStatus status)
            => Get(app => app.Status == status);

        public void UpdateWithStatus(TerminationApplication application, DocumentStatus status)
        {
            application.Status = status;
            application.StatusChangedDate = DateTime.Now;
            Update(application);
        }
    }
}
