﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Entities.Dictionary;
using Domain.Entities.Insurance;

namespace Data.Repositories.Insurance
{
    public class InsuranceApplicationRepository : Repository<InsuranceApplication>
    {
        public InsuranceApplicationRepository(ApplicationDbContext context) : base(context)
        {
        }

        public InsuranceApplication GetById(Guid id) => Get(app => app.Id == id).FirstOrDefault();

        public InsuranceApplication GetByInsuranceSum(decimal? insuranceSum)
            => Get(application => application.InsuranceSum == insuranceSum).FirstOrDefault();

        public IEnumerable<InsuranceApplication> GetByUserId(Guid userId)
            => Get(app => app.Applicant.User.Id == userId.ToString());

        public IEnumerable<InsuranceApplication> GetByStatus(DocumentStatus status)
            => Get(app => app.Status == status);

        public void UpdateWithStatus(InsuranceApplication application, DocumentStatus status)
        {
            application.Status = status;
            application.StatusChangedDate = DateTime.Now;
            Update(application);
        }
    }
}
