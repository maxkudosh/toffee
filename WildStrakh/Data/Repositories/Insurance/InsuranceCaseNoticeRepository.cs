﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Entities.Dictionary;
using Domain.Entities.Insurance;

namespace Data.Repositories.Insurance
{
    public class InsuranceCaseNoticeRepository : Repository<InsuranceCaseNotice>
    {
        public InsuranceCaseNoticeRepository(ApplicationDbContext context) : base(context)
        {
        }

        public IEnumerable<InsuranceCaseNotice> GetByStatus(DocumentStatus status)
            => Get(app => app.Status == status);

        public InsuranceCaseNotice GetById(Guid id) => Get(n => n.Id == id).FirstOrDefault();

        public IEnumerable<InsuranceCaseNotice> GetByUserId(Guid userId)
            => Get(n => n.Person.User.Id == userId.ToString());
    }
}
