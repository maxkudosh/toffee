﻿using System;
using System.Linq;
using Domain.Entities.Insurance;

namespace Data.Repositories.Insurance
{
    public class AdditionalInformationRepository : Repository<AdditionalInformation>
    {
        public AdditionalInformationRepository(ApplicationDbContext context) : base(context)
        {
        }

        public AdditionalInformation GetById(Guid id) => Get(info => info.Id == id).FirstOrDefault();
    }
}
