﻿using Domain.Entities.Insurance;

namespace Data.Repositories.Insurance
{
    public class AttachmentRepository : Repository<Attachment>
    {
        public AttachmentRepository(ApplicationDbContext context) : base(context)
        {
        }
    }
}
