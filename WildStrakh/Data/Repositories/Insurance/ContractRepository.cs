﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Entities.Dictionary;
using Domain.Entities.Insurance;

namespace Data.Repositories.Insurance
{
    public class ContractRepository : Repository<Contract>
    {
        public ContractRepository(ApplicationDbContext context) : base(context)
        {
        }

        public Contract GetById(Guid id) => Get(c => c.Id == id).FirstOrDefault();

        public IEnumerable<Contract> GetByUserId(Guid userId)
            => Get(c => c.Person.User.Id == userId.ToString());

        public IEnumerable<Contract> GetWithActiveStatus()
            => Get(c => c.Status == ContractStatus.Active);
    }
}
