﻿using System;
using System.Linq;
using Domain.Entities.Bank;
using Domain.Entities.Dictionary;

namespace Data.Repositories.Bank
{
    public class AccountRepository : Repository<Account>
    {
        public AccountRepository(ApplicationDbContext context) : base(context)
        {
        }

        public Account GetSystemAccount()
            => Get(acc => acc.AccountType == AccountType.System).FirstOrDefault();

        public Account GetByPersonId(Guid id) => Get(acc => acc.Owner.Id == id).FirstOrDefault();
    }
}
