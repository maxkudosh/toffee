﻿using System;
using System.Collections.Generic;
using Domain.Entities.Bank;
using Domain.Entities.Dictionary;

namespace Data.Repositories.Bank
{
    public class TransactionRepository : Repository<Transaction>
    {
        public TransactionRepository(ApplicationDbContext context) : base(context)
        {
        }

        public IEnumerable<Transaction> GetCurrentPeriodTransactionsToProcess()
            =>
                Get(
                    t =>
                        t.Status == TransactionStatus.Waiting &&
                            t.PlannedDate.Date == DateTime.Today);
    }
}
