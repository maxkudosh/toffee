﻿using System.Linq;
using Domain.Entities.PersonInformation;

namespace Data.Repositories.PersonInformation
{
    public class AddressRepository : Repository<Address>
    {
        public AddressRepository(ApplicationDbContext context) : base(context)
        {
        }

        public Address Get(string addressLine)
            => Get(address => address.AddressLine == addressLine).FirstOrDefault();
    }
}
