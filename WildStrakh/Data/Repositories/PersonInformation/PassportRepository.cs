﻿using System.Linq;
using Domain.Entities.PersonInformation;

namespace Data.Repositories.PersonInformation
{
    public class PassportRepository : Repository<Passport>
    {
        public PassportRepository(ApplicationDbContext context) : base(context)
        {
        }

        public Passport Get(string identificationNumber)
            =>
                Get(passport => passport.IdentificationNumber == identificationNumber)
                    .FirstOrDefault();
    }
}
