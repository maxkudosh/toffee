﻿using System.Linq;
using Domain.Entities.PersonInformation;

namespace Data.Repositories.PersonInformation
{
    public class PersonRepository : Repository<Person>
    {
        public PersonRepository(ApplicationDbContext context) : base(context)
        {
        }

        public Person Get(string firstName)
            => Get(person => person.FirstName == firstName).FirstOrDefault();
    }
}
