﻿using System;
using System.Linq;
using Domain.Entities.PersonInformation;

namespace Data.Repositories.PersonInformation
{
    public class IllnessRepository : Repository<Illness>
    {
        public IllnessRepository(ApplicationDbContext context) : base(context)
        {
        }

        public Illness Get(Guid id) => Get(illness => illness.Id == id).FirstOrDefault();
    }
}
