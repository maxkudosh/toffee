﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Entities.PersonInformation;

namespace Data.Repositories.PersonInformation
{
    public class PersonIllnessRepository : Repository<PersonIllness>
    {
        public PersonIllnessRepository(ApplicationDbContext context) : base(context)
        {
        }

        public PersonIllness GetByIllnessIdAndPersonName(Guid id, string name)
            => Get(pi => pi.Illness.Id == id && pi.Person.FirstName == name).FirstOrDefault();

        public IEnumerable<PersonIllness> GetByPersonId(Guid id) => Get(pi => pi.Person.Id == id);
    }
}
