﻿using Domain.Entities;

namespace Data.Repositories
{
    public class UserFileRepository : Repository<UserFile>
    {
        public UserFileRepository(ApplicationDbContext context) : base(context)
        {
        }
    }
}
