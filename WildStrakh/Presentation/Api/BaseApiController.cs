﻿using System;
using System.Web.Http;
using Business.Results;
using Microsoft.AspNet.Identity;

namespace Presentation.Api
{
    [Authorize]
    public class BaseApiController: ApiController
    {

        protected Guid UserId => Guid.Parse(User.Identity.GetUserId());

        protected IHttpActionResult ResponseDependingOnResult<T>(Result<T> result)
        {
            if (result.HasSucceded)
            {
                return Ok(result.Value);
            }

            return BadRequest(result.ErrorMessage);
        }

        protected IHttpActionResult ResponseDependingOnResult(VoidResult result)
        {
            if (result.HasSucceded)
            {
                return Ok();
            }

            return BadRequest(result.ErrorMessage);
        }

    }
}
