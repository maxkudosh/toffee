﻿using System.Web.Http;

using Business.Contracts;
using Presentation.Filters;

namespace Presentation.Api
{
    public class CityController: BaseApiController
    {

        private readonly ICityService _cityService;

        public CityController(ICityService cityService)
        {
            _cityService = cityService;
        }

        [HttpGet]
        [JsonEnumerableWrapperFilter]
        public IHttpActionResult GetAll(string nameStart)
        {
            return Ok(_cityService.GetWhereNameStartsWith(nameStart));
        }

    }
}