﻿using System;
using System.Web.Http;
using Business.Models;
using Business.Contracts;
using Presentation.Filters;

namespace Presentation.Api
{
    public class InformationCorrectionController : BaseApiController
    {

        private readonly IAdditionalInformationService _informationService;
        private readonly IInsuranceApplicationService _applicationService;
        private readonly IInsuranceCaseNoticeService _caseNoticeService;
        private readonly ITerminationService _terminationService;

        public InformationCorrectionController(IAdditionalInformationService informationService,
            IInsuranceApplicationService applicationService,
            IInsuranceCaseNoticeService caseNoticeService,
            ITerminationService terminationService)
        {
            _informationService = informationService;
            _applicationService = applicationService;
            _caseNoticeService = caseNoticeService;
            _terminationService = terminationService;
        }

        [HttpGet]
        [Route("api/informationcorrection/details/{id}/")]
        public IHttpActionResult GetRequest(Guid id)
        {
            return ResponseDependingOnResult(_informationService.GetByRequestId(id));
        }

        [HttpPost]
        [ValidateModel]
        [Route("api/informationcorrection/application/all/")]
        public IHttpActionResult CreateForApplication(InformationRequest model)
        {
            return ResponseDependingOnResult(_applicationService.RequestInformationCorrection(model));
        }


        [HttpPost]
        [ValidateModel]
        [Route("api/informationcorrection/notice/all/")]
        public IHttpActionResult CreateForInsuranceCase(InformationRequest model)
        {
            return ResponseDependingOnResult(_caseNoticeService.RequestInformationCorrection(model));
        }

        [HttpPost]
        [ValidateModel]
        [Route("api/informationcorrection/termination/all/")]
        public IHttpActionResult CreateForTerminationApplication(InformationRequest model)
        {
            return ResponseDependingOnResult(_terminationService.RequestInformationCorrection(model));
        }

        [HttpGet]
        [JsonEnumerableWrapperFilter]
        [Route("api/informationcorrection/application/all/{applicationId}")]
        public IHttpActionResult GetAllForApplication(Guid applicationId)
        {
            return
                ResponseDependingOnResult(
                    _informationService
                        .GetByDocumentId<Domain.Entities.Insurance.InsuranceApplication>(
                            applicationId,
                            Domain.Entities.Dictionary.InformationType.InformationCorrection));
        }

        [HttpGet]
        [JsonEnumerableWrapperFilter]
        [Route("api/informationcorrection/notice/all/{noticeId}")]
        public IHttpActionResult GetAllForInsurance(Guid noticeId)
        {
            return
                ResponseDependingOnResult(
                    _informationService
                        .GetByDocumentId<Domain.Entities.Insurance.InsuranceCaseNotice>(noticeId,
                            Domain.Entities.Dictionary.InformationType.InformationCorrection));
        }

        [HttpGet]
        [JsonEnumerableWrapperFilter]
        [Route("api/informationcorrection/termination/all/{terminationId}")]
        public IHttpActionResult GetAllForTermination(Guid terminationId)
        {
            return
                ResponseDependingOnResult(
                    _informationService
                        .GetByDocumentId<Domain.Entities.Insurance.TerminationApplication>(
                            terminationId,
                            Domain.Entities.Dictionary.InformationType.InformationCorrection));
        }

        [HttpPost]
        [ValidateModel]
        [Route("api/informationcorrection/application/respond/")]
        public IHttpActionResult RespondForApplication(InformationCorrection model)
        {
            return ResponseDependingOnResult(_applicationService.RespondInformationCorrection(model.Id.Value, model));
        }

        [HttpPost]
        [ValidateModel]
        [Route("api/informationcorrection/notice/respond/")]
        public IHttpActionResult RespondForNotice(InformationCorrection model)
        {
            return ResponseDependingOnResult(_caseNoticeService.RespondInformationCorrection(model.Id.Value, model));
        }

        [HttpPost]
        [ValidateModel]
        [Route("api/informationcorrection/termination/respond")]
        public IHttpActionResult RespondForTermination(InformationCorrection model)
        {
            return ResponseDependingOnResult(_terminationService.RespondInformationCorrection(model.Id.Value, model));
        }


        [HttpGet]
        [JsonEnumerableWrapperFilter]
        [Route("api/informationcorrection/application/all/")]
        public IHttpActionResult GetAllForApplication()
        {
            return
                ResponseDependingOnResult(
                    _informationService
                        .GetAllByUserId<Domain.Entities.Insurance.InsuranceApplication>(UserId,
                            Domain.Entities.Dictionary.InformationType.InformationCorrection));
        }

        [HttpGet]
        [JsonEnumerableWrapperFilter]
        [Route("api/informationcorrection/notice/all/")]
        public IHttpActionResult GetAllForCase()
        {
            return
                ResponseDependingOnResult(
                    _informationService
                        .GetAllByUserId<Domain.Entities.Insurance.InsuranceCaseNotice>(UserId,
                            Domain.Entities.Dictionary.InformationType.InformationCorrection));
        }

        [HttpGet]
        [JsonEnumerableWrapperFilter]
        [Route("api/informationcorrection/termination/all/")]
        public IHttpActionResult GetAllForTermination()
        {
            return
                ResponseDependingOnResult(
                    _informationService
                        .GetAllByUserId<Domain.Entities.Insurance.TerminationApplication>(UserId,
                            Domain.Entities.Dictionary.InformationType.InformationCorrection));
        }
    }
}
