﻿using System;
using System.Web.Http;
using Business.Contracts;
using Business.Models;
using Presentation.Filters;
using Presentation.Models.Api;

namespace Presentation.Api
{
    public class ContractController : BaseApiController
    {

        private readonly IContractService _contractService;

        public ContractController(IContractService contractService)
        {
            _contractService = contractService;
        }

        [HttpGet]
        [JsonEnumerableWrapperFilter]
        [Route("api/contract/")]
        public IHttpActionResult GetAllContracts()
        {
            return ResponseDependingOnResult(_contractService.GetContractsByUserId(UserId));
        }

        [HttpGet]
        [Route("api/contract/variants/{applicationId}")]
        public IHttpActionResult GetAllVariants(Guid applicationId)
        {
            return ResponseDependingOnResult(_contractService.GetContractVariants(applicationId));
        }

        [HttpPut]
        [Route("api/contract/approve/")]
        [ValidateModel]
        public IHttpActionResult Approve(Contract model)
        {
            return ResponseDependingOnResult(_contractService.ApproveContract(model));
        }

        [HttpPut]
        [Route("api/contract/pay/")]
        [ValidateModel]
        public IHttpActionResult Pay(ContractFirstPayment model)
        {
            return ResponseDependingOnResult(_contractService.MakeFirstPayment(model.ContractId.Value, model.Card));
        }

        [HttpPut]
        [Route("api/contract/reject/")]
        [ValidateModel]
        public IHttpActionResult Reject(ContractRejection model)
        {
            return ResponseDependingOnResult(_contractService.RejectInsuranceApplication(model.ApplicationId.Value));
        }

        [HttpPut]
        [Route("api/contract/cancel/")]
        [ValidateModel]
        public IHttpActionResult Cancel(ContractCancellation model)
        {
            return ResponseDependingOnResult(_contractService.RejectCreatedContract(model.ContractId.Value));
        }

    }
}
