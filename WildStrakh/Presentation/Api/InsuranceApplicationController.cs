﻿using System;
using System.Web.Http;
using Business.Contracts;
using Business.Models;
using Domain.Entities.Dictionary;
using Presentation.Filters;
using Presentation.Models.Api;

namespace Presentation.Api
{
    public class InsuranceApplicationController : BaseApiController
    {

        private readonly IInsuranceApplicationService _service;
        private readonly IContractService _contractService;

        public InsuranceApplicationController(IInsuranceApplicationService service, IContractService contractService)
        {
            _service = service;
            _contractService = contractService;
        }

        [HttpPost]
        [ValidateModel]
        public IHttpActionResult Create(InsuranceApplication application)
        {
            _service.Apply(UserId, application);
            return Created("", application);
        }

        [HttpGet]
        [JsonEnumerableWrapperFilter]
        public IHttpActionResult GetAll(DocumentStatus status)
        {
            return ResponseDependingOnResult(_service.GetAllByStatus(status));
        }

        [HttpGet]
        [JsonEnumerableWrapperFilter]
        public IHttpActionResult GetAll()
        {
            return ResponseDependingOnResult(_service.GetByUserId(UserId));
        }

        [HttpGet]
        public IHttpActionResult Get(Guid id)
        {
            return ResponseDependingOnResult(_service.GetById(id));
        }

        [HttpPut]
        [ValidateModel]
        public IHttpActionResult Update([FromBody] DocumentStatusChange statusChange)
        {
            if (statusChange.Status == DocumentStatus.Validated)
            {
                return ResponseDependingOnResult(_service.CompleteValidation(statusChange.Id.Value));
            }
            if (statusChange.Status == DocumentStatus.Rejected)
            {
                return ResponseDependingOnResult(_contractService.RejectInsuranceApplication(statusChange.Id.Value));
            }
            return BadRequest();
        }
    }
}
