﻿using System;
using System.Web.Http;

using Business.Models;
using Business.Contracts;
using Domain.Entities.Dictionary;
using Presentation.Filters;
using Presentation.Models.Api;

namespace Presentation.Api
{
    [Authorize]
    public class TerminationApplicationController : BaseApiController
    {

        private readonly ITerminationService _terminationService;

        public TerminationApplicationController(ITerminationService terminationService)
        {
            _terminationService = terminationService;
        }

        [HttpGet]
        [JsonEnumerableWrapperFilter]
        public IHttpActionResult GetAll(DocumentStatus status)
        {
            return ResponseDependingOnResult(_terminationService.GetAllByStatus(status));
        }

        [HttpGet]
        public IHttpActionResult Get(Guid id)
        {
            return ResponseDependingOnResult(_terminationService.GetApplication(id));
        }

        [HttpPost]
        [ValidateModel]
        public IHttpActionResult Create(TerminationApplication model)
        {
            return ResponseDependingOnResult(_terminationService.Apply(model));
        }

        [HttpPut]
        [ValidateModel]
        public IHttpActionResult Update([FromBody] DocumentStatusChange statusChange)
        {
            if (statusChange.Status == DocumentStatus.Validated)
            {
                return ResponseDependingOnResult(_terminationService.CompleteValidation(statusChange.Id.Value));
            }
            if (statusChange.Status == DocumentStatus.Approved)
            {
                return ResponseDependingOnResult(_terminationService.Terminate(statusChange.Id.Value));
            }
            if (statusChange.Status == DocumentStatus.Rejected)
            {
                return ResponseDependingOnResult(_terminationService.Reject(statusChange.Id.Value));
            }
            return BadRequest();
        }
    }
}
