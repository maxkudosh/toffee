﻿using System;
using System.Web.Http;
using Business.Contracts;
using Business.Models;
using Presentation.Filters;

namespace Presentation.Api
{
    public class AdditionalInformationController : BaseApiController
    {

        private readonly IAdditionalInformationService _informationService;
        private readonly IInsuranceApplicationService _applicationService;
        private readonly IInsuranceCaseNoticeService _caseNoticeService;
        private readonly ITerminationService _terminationService;

        public AdditionalInformationController(IAdditionalInformationService informationService,
            IInsuranceApplicationService applicationService,
            IInsuranceCaseNoticeService caseNoticeService,
            ITerminationService terminationService)
        {
            _informationService = informationService;
            _applicationService = applicationService;
            _caseNoticeService = caseNoticeService;
            _terminationService = terminationService;
        }

        [HttpGet]
        [Route("api/additionalinformation/details/{id}/")]
        public IHttpActionResult GetRequest(Guid id)
        {
            return ResponseDependingOnResult(_informationService.GetByRequestId(id));
        }

        [HttpPost]
        [ValidateModel]
        [Route("api/additionalinformation/application/all/")]
        public IHttpActionResult CreateForApplication(InformationRequest model)
        {
            return ResponseDependingOnResult(_applicationService.RequestAdditionalInformation(model));
        }


        [HttpPost]
        [ValidateModel]
        [Route("api/additionalinformation/notice/all/")]
        public IHttpActionResult CreateForInsuranceCase(InformationRequest model)
        {
            return ResponseDependingOnResult(_caseNoticeService.RequestAdditionalInformation(model));
        }

        [HttpPost]
        [ValidateModel]
        [Route("api/additionalinformation/termination/all/")]
        public IHttpActionResult CreateForTerminationApplication(InformationRequest model)
        {
            return ResponseDependingOnResult(_terminationService.RequestAdditionalInformation(model));
        }

        [HttpGet]
        [JsonEnumerableWrapperFilter]
        [Route("api/additionalinformation/application/all/{applicationId}")]
        public IHttpActionResult GetAllForApplication(Guid applicationId)
        {
            return
                ResponseDependingOnResult(
                    _informationService
                        .GetByDocumentId<Domain.Entities.Insurance.InsuranceApplication>(
                            applicationId,
                            Domain.Entities.Dictionary.InformationType.AdditionalInformation));
        }

        [HttpGet]
        [JsonEnumerableWrapperFilter]
        [Route("api/additionalinformation/notice/all/{noticeId}")]
        public IHttpActionResult GetAllForInsurance(Guid noticeId)
        {
            return
                ResponseDependingOnResult(
                    _informationService
                        .GetByDocumentId<Domain.Entities.Insurance.InsuranceCaseNotice>(noticeId,
                            Domain.Entities.Dictionary.InformationType.AdditionalInformation));
        }

        [HttpGet]
        [JsonEnumerableWrapperFilter]
        [Route("api/additionalinformation/termination/all/{terminationId}")]
        public IHttpActionResult GetAllForTermination(Guid terminationId)
        {
            return
                ResponseDependingOnResult(
                    _informationService
                        .GetByDocumentId<Domain.Entities.Insurance.TerminationApplication>(
                            terminationId,
                            Domain.Entities.Dictionary.InformationType.AdditionalInformation));
        }

        [HttpPost]
        [ValidateModel]
        [Route("api/additionalinformation/application/respond")]
        public IHttpActionResult RespondForApplication( AdditionalInformation model)
        {
            return ResponseDependingOnResult(_applicationService.RespondAdditionalInformation(model.Id.Value, model));
        }

        [HttpPost]
        [ValidateModel]
        [Route("api/additionalinformation/notice/respond")]
        public IHttpActionResult RespondForNotice(AdditionalInformation model)
        {
            return ResponseDependingOnResult(_caseNoticeService.RespondAdditionalInformation(model.Id.Value, model));
        }

        [HttpPost]
        [ValidateModel]
        [Route("api/additionalinformation/termination/respond")]
        public IHttpActionResult RespondForTermination(AdditionalInformation model)
        {
            return ResponseDependingOnResult(_terminationService.RespondAdditionalInformation(model.Id.Value, model));
        }


        [HttpGet]
        [JsonEnumerableWrapperFilter]
        [Route("api/additionalinformation/application/all/")]
        public IHttpActionResult GetAllForApplication()
        {
            return
                ResponseDependingOnResult(
                    _informationService
                        .GetAllByUserId<Domain.Entities.Insurance.InsuranceApplication>(UserId,
                            Domain.Entities.Dictionary.InformationType.AdditionalInformation));
        }

        [HttpGet]
        [JsonEnumerableWrapperFilter]
        [Route("api/additionalinformation/notice/all/")]
        public IHttpActionResult GetAllForCase()
        {
            return
                ResponseDependingOnResult(
                    _informationService
                        .GetAllByUserId<Domain.Entities.Insurance.InsuranceCaseNotice>(UserId,
                            Domain.Entities.Dictionary.InformationType.AdditionalInformation));
        }

        [HttpGet]
        [JsonEnumerableWrapperFilter]
        [Route("api/additionalinformation/termination/all/")]
        public IHttpActionResult GetAllForTermination()
        {
            return
                ResponseDependingOnResult(
                    _informationService
                        .GetAllByUserId<Domain.Entities.Insurance.TerminationApplication>(UserId,
                            Domain.Entities.Dictionary.InformationType.AdditionalInformation));
        }

    }
}
