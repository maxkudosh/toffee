﻿using System;
using System.Web.Http;

using Business.Models;
using Business.Contracts;
using Domain.Entities.Dictionary;
using Presentation.Filters;
using Presentation.Models.Api;

namespace Presentation.Api
{
    public class InsuranceCaseNoticeController : BaseApiController
    {

        private readonly IInsuranceCaseNoticeService _noticeService;

        public InsuranceCaseNoticeController(IInsuranceCaseNoticeService noticeService)
        {
            _noticeService = noticeService;
        }

        [HttpGet]
        public IHttpActionResult Get(Guid id)
        {
            return ResponseDependingOnResult(_noticeService.GetNotice(id));
        }

        [HttpGet]
        [JsonEnumerableWrapperFilter]
        public IHttpActionResult GetAll(DocumentStatus status)
        {
            return ResponseDependingOnResult(_noticeService.GetAllByStatus(status));
        }

        [HttpPost]
        [ValidateModel]
        public IHttpActionResult Create(InsuranceCaseNotice notification)
        {
            _noticeService.Notify(notification);
            return Created("created insurance case notice uri", notification);
        }

        [HttpPut]
        [ValidateModel]
        public IHttpActionResult Update([FromBody] DocumentStatusChange statusChange)
        {
            if (statusChange.Status == DocumentStatus.Validated)
            {
                return ResponseDependingOnResult(_noticeService.CompleteValidation(statusChange.Id.Value));
            }
            return BadRequest();
        }

        [HttpPost]
        [ValidateModel]
        [Route("api/insurancecasenoticereview")]
        public IHttpActionResult Update([FromBody] NoticeReviewResult review)
        {
            return ResponseDependingOnResult(_noticeService.ReviewNotice(review));
        }

    }
}
