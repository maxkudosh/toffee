﻿using System.Web.Http;

using Business.Contracts;
using Presentation.Filters;

namespace Presentation.Api
{
    public class PassportSeriesController: BaseApiController
    {

        private readonly IPassportService _passportService;

        public PassportSeriesController(IPassportService passportService)
        {
            _passportService = passportService;
        }

        [HttpGet]
        [JsonEnumerableWrapperFilter]
        public IHttpActionResult GetAll()
        {
            return Ok(_passportService.GetAllSeries());
        }
    }
}
