﻿using System;
using System.Web.Http;
using Business.Contracts;
using Business.Models;
using Business.Results;
using Presentation.Filters;

namespace Presentation.Api
{
    public class AdditionalInformationRequestController: BaseApiController
    {

        private readonly IAdditionalInformationService _informationService;

        public AdditionalInformationRequestController(IAdditionalInformationService informationService)
        {
            _informationService = informationService;
        }

        [HttpGet]
        [JsonEnumerableWrapperFilter]
        public IHttpActionResult GetAll()
        {
            return ResponseDependingOnResult(_informationService.GetAllByUserId(UserId, Domain.Entities.Dictionary.InformationType.AdditionalInformation));
        }


        [HttpGet]
        public IHttpActionResult Get(Guid id)
        {
            return ResponseDependingOnResult(_informationService.GetByRequestId(id));
        }

        [HttpPost]
        [ValidateModel]
        public IHttpActionResult PostResponse(AdditionalInformation model)
        {
            return ResponseDependingOnResult(_informationService.RespondAdditionalInformation(model.Id.Value, model));
        }

    }
}