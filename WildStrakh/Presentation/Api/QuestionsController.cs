﻿using System;
using System.Web.Http;

using Business.Contracts;
using Business.Models;
using Presentation.Filters;

namespace Presentation.Api
{
    public class QuestionsController: BaseApiController
    {

        private readonly IContractService _contractService;

        public QuestionsController(IContractService contractService)
        {
            _contractService = contractService;
        }

        [HttpGet]
        [JsonEnumerableWrapperFilter]
        public IHttpActionResult GetAll(Guid id)
        {
            return ResponseDependingOnResult(_contractService.PrepareQuestions(id));
        }

        [HttpPost]
        [ValidateModel]
        public IHttpActionResult GetAll(QuestionsAnswering answers)
        {
            return ResponseDependingOnResult(_contractService.GenerateContractCandidates(answers));
        }

    }
}