﻿using System.Web.Http;
using Business.Contracts;
using Presentation.Filters;

namespace Presentation.Api
{
    public class IllnessController: BaseApiController
    {
        private readonly IIllnessService _illnessService;

        public IllnessController(IIllnessService illnessService)
        {
            _illnessService = illnessService;
        }

        [JsonEnumerableWrapperFilter]
        public IHttpActionResult GetAll(string nameStart)
        {
            return Ok(_illnessService.GetWhereNameStartWith(nameStart));
        }
    }
}
