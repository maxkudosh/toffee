﻿using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using Business.Contracts;

namespace Presentation.Api
{
    public class AttachmentController : BaseApiController
    {

        private readonly IAdditionalInformationService _attachmentService;

        public AttachmentController(IAdditionalInformationService attachmentService)
        {
            _attachmentService = attachmentService;
        }

        [HttpGet]
        [Route("api/attachment/{id}")]
        public HttpResponseMessage Get(Guid id)
        {
            var attachment = _attachmentService.GetAttachmentContent(id);

            if (!attachment.HasSucceded)
                return new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    Content = new StringContent(attachment.ErrorMessage)
                };

            var response = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StreamContent(new MemoryStream(attachment.Value.Content))
            };

            response.Content.Headers.ContentType = new MediaTypeHeaderValue(attachment.Value.MimeType);

            return response;
        }

    }
}
