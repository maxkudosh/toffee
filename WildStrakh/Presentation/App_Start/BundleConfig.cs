﻿using System.Web.Optimization;

namespace Presentation
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/styles")
                .Include("~/public/bower_components/bootstrap/dist/css/bootstrap.css")
                .Include("~/public/bower_components/ng-file-upload/ng-file-upload.css")
                .Include("~/public/bower_components/angular-toastr/dist/angular-toastr.css")
                .Include("~/public/components/multiselect/multiselect.css")
                .Include("~/public/components/attachment/attachment.css")
                .Include("~/public/components/document-status/document-status.css")
                .Include("~/public/app.css")
                .Include("~/public/user/contracts/contracts.css")
                .Include("~/public/underwriter/applications/details/questions/questions.css")
                );

            bundles.Add(new ScriptBundle("~/scripts/vendors")
                .Include("~/public/bower_components/angular/angular.js")
                .Include("~/public/bower_components/angular-cookies/angular-cookies.js")
                .Include("~/public/bower_components/angular-resource/angular-resource.js")
                .Include("~/public/bower_components/angular-ui-router/release/angular-ui-router.js")
                .Include("~/public/bower_components/angular-bootstrap/ui-bootstrap-tpls.js")
                .Include("~/public/bower_components/ng-file-upload/ng-file-upload.js")
                .Include("~/public/bower_components/angular-toastr/dist/angular-toastr.tpls.js")
                .Include("~/public/bower_components/angular-credit-cards/release/angular-credit-cards.js")
                );

            bundles.Add(new ScriptBundle("~/scripts/app")

                .Include("~/public/app.js")
                .Include("~/public/app.controller.js")

                .Include("~/public/components/multiselect/multiselect.js")

                .Include("~/public/components/routes.js")

                .Include("~/public/components/user-info.js")

                .Include("~/public/components/open-on-click-datepicker.directive.js")

                .Include("~/public/components/controllers/controllers.js")
                .Include("~/public/components/controllers/correction-controller.factory.js")

                .Include("~/public/components/services/services.js")
                .Include("~/public/components/services/contract.service.js")
                .Include("~/public/components/services/request.service.js")
                .Include("~/public/components/services/application.service.js")
                .Include("~/public/components/services/case.service.js")
                .Include("~/public/components/services/termination.service.js")
                .Include("~/public/components/services/additional-information.service.js")
                .Include("~/public/components/services/information-correction.service.js")
                .Include("~/public/components/services/document-service.factory.js")
                .Include("~/public/components/services/cached-service.factory.js")
                .Include("~/public/components/services/service.factory.js")

                .Include("~/public/components/navbar/navbar.js")
                .Include("~/public/components/navbar/navbar.controller.js")
                .Include("~/public/components/navbar/navbar.directive.js")

                .Include("~/public/components/application/application.js")
                .Include("~/public/components/application/application.directive.js")

                .Include("~/public/components/additional-information/additional-information.js")
                .Include("~/public/components/additional-information/additional-information.directive.js")

                .Include("~/public/components/information-correction/information-correction.js")
                .Include("~/public/components/information-correction/information-correction.directive.js")

                .Include("~/public/components/termination-application/termination-application.js")
                .Include("~/public/components/termination-application/termination-application.directive.js")

                .Include("~/public/components/insurance-case-notice/insurance-case-notice.js")
                .Include("~/public/components/insurance-case-notice/insurance-case-notice.directive.js")

                .Include("~/public/components/person/person.js")
                .Include("~/public/components/person/person.directive.js")

                .Include("~/public/components/document-status/document-status.directive.js")

                .Include("~/public/components/illness/illness.js")
                .Include("~/public/components/illness/illness.controller.js")
                .Include("~/public/components/illness/illness.directive.js")
                .Include("~/public/components/illness/illness.service.js")

                .Include("~/public/components/attachment/attachment.js")
                .Include("~/public/components/attachment/attachment.controller.js")
                .Include("~/public/components/attachment/attachment.directive.js")
                .Include("~/public/components/attachment/attachment.service.js")
                .Include("~/public/components/attachment/display-attachment.directive.js")

                .Include("~/public/components/enums/enums.js")
                .Include("~/public/components/enums/converter.service.js")
                .Include("~/public/components/enums/document-status.enum.js")
                .Include("~/public/components/enums/enum-dropdown.directive.js")
                .Include("~/public/components/enums/insurance-type.enum.js")
                .Include("~/public/components/enums/occupation.enum.js")
                .Include("~/public/components/enums/illness-type.enum.js")
                .Include("~/public/components/enums/payment-order.enum.js")
                .Include("~/public/components/enums/contract-status.enum.js")

                .Include("~/public/components/address/address.js")
                .Include("~/public/components/address/address.directive.js")
                .Include("~/public/components/address/address.controller.js")
                .Include("~/public/components/address/cities.service.js")

                .Include("~/public/user/user.js")

                .Include("~/public/user/apply/apply.js")
                .Include("~/public/user/apply/personal-information.directive.js")
                .Include("~/public/user/apply/apply.service.js")
                .Include("~/public/user/apply/passport.service.js")
                .Include("~/public/user/apply/apply.controller.js")

                .Include("~/public/user/applications/applications.js")
                .Include("~/public/user/applications/applications.controller.js")

                .Include("~/public/user/applications/details/details.js")
                .Include("~/public/user/applications/details/details.controller.js")

                .Include("~/public/user/notify/notify.js")
                .Include("~/public/user/notify/select-contract.controller.js")

                .Include("~/public/user/corrections/corrections.js")
                .Include("~/public/user/corrections/corrections.controller.js")

                .Include("~/public/user/corrections/correct/correct.js")
                .Include("~/public/user/corrections/correct/correction.controller.js")

                .Include("~/public/user/contracts/contracts.js")
                .Include("~/public/user/contracts/contracts.controller.js")
                .Include("~/public/user/contracts/contract-status.directive.js")

                .Include("~/public/user/contracts/cancel/cancel.js")
                .Include("~/public/user/contracts/cancel/cancel.controller.js")
                .Include("~/public/user/contracts/cancel/termination-application.service.js")

                .Include("~/public/user/contracts/notify/notify.js")
                .Include("~/public/user/contracts/notify/notify.controller.js")
                .Include("~/public/user/contracts/notify/notify.service.js")

                .Include("~/public/user/contracts/accept/accept.js")
                .Include("~/public/user/contracts/accept/accept.controller.js")
                .Include("~/public/user/contracts/accept/accept.service.js")

                .Include("~/public/user/requests/requests.js")
                .Include("~/public/user/requests/requests.controller.js")

                .Include("~/public/user/requests/satisfy/satisfy.js")
                .Include("~/public/user/requests/satisfy/satisfy.controller.js")
                .Include("~/public/user/requests/satisfy/satisfy.service.js")

                .Include("~/public/user/requests/view/view.js")
                .Include("~/public/user/requests/view/view.controller.js")
                .Include("~/public/user/requests/view/view.service.js")

                .Include("~/public/secretary/secretary.js")

                .Include("~/public/secretary/applications/applications.js")
                .Include("~/public/secretary/applications/applications.controller.js")

                .Include("~/public/secretary/applications/details/details.js")
                .Include("~/public/secretary/applications/details/details.controller.js")

                .Include("~/public/secretary/applications/details/request/request.js")
                .Include("~/public/secretary/applications/details/request/request.controller.js")

                .Include("~/public/secretary/cases/cases.js")
                .Include("~/public/secretary/cases/cases.controller.js")

                .Include("~/public/secretary/cases/details/details.js")
                .Include("~/public/secretary/cases/details/details.controller.js")

                .Include("~/public/secretary/cases/details/request/request.js")
                .Include("~/public/secretary/cases/details/request/request.controller.js")

                .Include("~/public/secretary/terminations/terminations.js")
                .Include("~/public/secretary/terminations/terminations.controller.js")

                .Include("~/public/secretary/terminations/details/details.js")
                .Include("~/public/secretary/terminations/details/details.controller.js")

                .Include("~/public/secretary/terminations/details/request/request.js")
                .Include("~/public/secretary/terminations/details/request/request.controller.js")

                .Include("~/public/underwriter/underwriter.js")

                .Include("~/public/underwriter/applications/applications.js")
                .Include("~/public/underwriter/applications/applications.controller.js")

                .Include("~/public/underwriter/applications/details/details.js")
                .Include("~/public/underwriter/applications/details/details.controller.js")

                .Include("~/public/underwriter/applications/details/questions/questions.js")
                .Include("~/public/underwriter/applications/details/questions/questions.controller.js")
                .Include("~/public/underwriter/applications/details/questions/questions.service.js")

                .Include("~/public/underwriter/applications/details/request/request.js")
                .Include("~/public/underwriter/applications/details/request/request.controller.js")

                .Include("~/public/underwriter/applications/details/approve/approve.js")
                .Include("~/public/underwriter/applications/details/approve/approve.controller.js")
                .Include("~/public/underwriter/applications/details/approve/approval.service.js")

                .Include("~/public/underwriter/cases/cases.js")
                .Include("~/public/underwriter/cases/cases.controller.js")

                .Include("~/public/underwriter/cases/details/details.js")
                .Include("~/public/underwriter/cases/details/details.controller.js")

                .Include("~/public/underwriter/cases/details/approve/approve.js")
                .Include("~/public/underwriter/cases/details/approve/approve.controller.js")

                .Include("~/public/underwriter/cases/details/request/request.js")
                .Include("~/public/underwriter/cases/details/request/request.controller.js")

                .Include("~/public/underwriter/terminations/terminations.js")
                .Include("~/public/underwriter/terminations/terminations.controller.js")

                .Include("~/public/underwriter/terminations/details/details.js")
                .Include("~/public/underwriter/terminations/details/details.controller.js")

                .Include("~/public/underwriter/terminations/details/request/request.js")
                .Include("~/public/underwriter/terminations/details/request/request.controller.js")

                .Include("~/public/underwriter/applications/details/details.js")
                .Include("~/public/underwriter/applications/details/details.controller.js")

                );
        }
    }
}
