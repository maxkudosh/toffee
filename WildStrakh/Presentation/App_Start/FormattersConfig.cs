﻿using System.Web.Http;

using MultipartDataMediaFormatter;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Presentation
{
    public static class FormattersConfig
    {
        public static void Register(HttpConfiguration configuration)
        {
            configuration.Formatters.Add(new FormMultipartEncodedMediaTypeFormatter());
            var jsonFormatter = configuration.Formatters.JsonFormatter;
            jsonFormatter.SerializerSettings.Formatting = Formatting.Indented;
            jsonFormatter.SerializerSettings.ContractResolver =
                new CamelCasePropertyNamesContractResolver();
        }
    }
}
