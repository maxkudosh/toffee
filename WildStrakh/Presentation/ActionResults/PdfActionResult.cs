﻿using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Text;
using System.Web.Mvc;

namespace Presentation.ActionResults
{
    public class PdfActionResult : ActionResult
    {
        private const string ApiKey = "b6e5e4f7-12fb-48d5-9421-033f3a528528";

        private const string ApiUrl = "http://api.html2pdfrocket.com/pdf";

        private const int Margin = 15;

        public string ViewName
        {
            get;
            set;
        }

        public object Model
        {
            get;
            set;
        }

        public string FileDownloadName
        {
            get;
            set;
        }

        public PdfActionResult(string viewName, object model, string fileDownloadName)
        {
            ViewName = viewName;
            Model = model;
            FileDownloadName = fileDownloadName;
        }

        public PdfActionResult(object model, string fileDownloadName)
        {
            Model = model;
            FileDownloadName = fileDownloadName;
        }

        public override void ExecuteResult(ControllerContext context)
        {
            if (string.IsNullOrEmpty(ViewName))
            {
                ViewName = context.RouteData.GetRequiredString("action");
            }
            if (!string.IsNullOrEmpty(FileDownloadName))
            {
                context.HttpContext.Response.AddHeader("content-disposition",
                    "attachment; filename=" + FileDownloadName);
            }
            var fileResult = new FileContentResult(GetPdfBytes(context), "application/pdf");
            fileResult.ExecuteResult(context);
        }

        private byte[] GetPdfBytes(ControllerContext context)
        {
            using (var webClient = new WebClient())
            {
                return webClient.UploadValues(ApiUrl, GetOptions(RenderRazorView(context)));
            }
        }

        private static NameValueCollection GetOptions(string html) => new NameValueCollection
        {
            {
                "apikey", ApiKey
            },
            {
                "value", html
            },
            {
                "MarginTop", Margin.ToString()
            },
            {
                "MarginLeft", Margin.ToString()
            },
            {
                "MarginRight", Margin.ToString()
            },
            {
                "MarginBottom", Margin.ToString()
            }
        };

        private string RenderRazorView(ControllerContext context)
        {
            context.Controller.ViewData.Model = Model;
            var view = ViewEngines.Engines.FindView(context, ViewName, null).View;
            var builder = new StringBuilder();
            using (TextWriter writer = new StringWriter(builder))
            {
                view.Render(GetViewContext(context, view, writer), writer);
            }
            return builder.ToString();
        }

        private static ViewContext GetViewContext(ControllerContext context, IView view,
            TextWriter writer)
            =>
                new ViewContext(context, view, context.Controller.ViewData,
                    context.Controller.TempData, writer);
    }
}
