﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(Presentation.Startup))]
namespace Presentation
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            //GlobalConfiguration.Configuration
            ConfigureAuth(app);
        }
    }
}
