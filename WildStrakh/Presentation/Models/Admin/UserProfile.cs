﻿using System.ComponentModel.DataAnnotations;

namespace Presentation.Models.Admin
{
    public class UserProfile
    {
        [Display(Name = @"Почта")]
        public string Email
        {
            get;
            set;
        }

        [Display(Name = @"Роль")]
        public string Role
        {
            get;
            set;
        }

        [Display(Name = @"Удалить")]
        public bool ShouldDelete
        {
            get;
            set;
        }

        [Display(Name = @"Длительность блокировки")]
        public int LockoutDuration
        {
            get;
            set;
        }
    }
}
