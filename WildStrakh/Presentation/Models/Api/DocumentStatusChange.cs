﻿using System;
using System.ComponentModel.DataAnnotations;
using Domain.Entities.Dictionary;

namespace Presentation.Models.Api
{
    public class DocumentStatusChange
    {
        [Required]
        public Guid? Id { get; set; }
        [Required]
        public DocumentStatus? Status { get; set; }
    }
}