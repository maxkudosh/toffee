﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Presentation.Models.Api
{
    public class ContractCancellation
    {
        [Required]
        public Guid? ContractId { get; set; }
    }
}