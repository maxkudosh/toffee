﻿using System;
using System.ComponentModel.DataAnnotations;
using Business.Models;

namespace Presentation.Models.Api
{
    public class ContractFirstPayment
    {
        [Required]
        public Guid? ContractId { get; set; }

        public BankCard Card
        {
            get;
            set;
        }
    }
}