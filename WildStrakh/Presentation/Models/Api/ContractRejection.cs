﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Presentation.Models.Api
{
    public class ContractRejection
    {
        [Required]
        public Guid? ApplicationId { get; set; }
    }
}