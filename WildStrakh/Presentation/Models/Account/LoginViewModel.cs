﻿using System.ComponentModel.DataAnnotations;

namespace Presentation.Models.Account
{
    public class LoginViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = @"Почта")]
        public string Email
        {
            get;
            set;
        }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = @"Пароль")]
        public string Password
        {
            get;
            set;
        }
    }
}
