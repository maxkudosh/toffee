﻿using System.ComponentModel.DataAnnotations;

namespace Presentation.Models.Account
{
    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = @"Почта")]
        public string Email
        {
            get;
            set;
        }
    }
}
