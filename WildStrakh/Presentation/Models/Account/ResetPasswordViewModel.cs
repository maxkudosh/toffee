﻿namespace Presentation.Models.Account
{
    public class ResetPasswordViewModel : RegisterViewModel
    {
        public string Code
        {
            get;
            set;
        }
    }
}
