﻿using System.ComponentModel.DataAnnotations;

namespace Presentation.Models.Account
{
    public class RegisterViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = @"Почта")]
        public string Email
        {
            get;
            set;
        }

        [Required]
        [StringLength(100, ErrorMessage = @"{0} должен быть не короче {2} символа(ов).",
            MinimumLength = 1)]
        [DataType(DataType.Password)]
        [Display(Name = @"Пароль")]
        public string Password
        {
            get;
            set;
        }

        [DataType(DataType.Password)]
        [Display(Name = @"Пароль")]
        [Compare("Password", ErrorMessage = @"Пароль не совпадает с подтверждением.")]
        public string ConfirmPassword
        {
            get;
            set;
        }
    }
}
