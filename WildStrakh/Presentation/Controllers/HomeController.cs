﻿using System.Web.Mvc;

namespace Presentation.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return RedirectToAction("Manage", "Account");
        }
    }
}