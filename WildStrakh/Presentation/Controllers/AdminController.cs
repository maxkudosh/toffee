﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Domain.Entities;
using Microsoft.AspNet.Identity;
using PagedList;
using Presentation.Models.Admin;

namespace Presentation.Controllers
{
    [Authorize(Roles = Common.Constants.AdminRoleName)]
    public class AdminController : Controller
    {
        private readonly ApplicationUserManager _userManager;

        private static string _lastUserEmail;

        public AdminController(ApplicationUserManager userManager)
        {
            _userManager = userManager;
        }

        public ActionResult Users(int page = 1, int pageSize = 10)
        {
            var users = _userManager.Users.ToList();
            var roles = users.Select(GetRole).ToList();
            var models = GetUsersList(users, roles);
            var pagedList = new PagedList<UserProfile>(models, page, pageSize);
            return View(pagedList);
        }

        public ActionResult UserProfile(string email)
        {
            email = CheckEmail(email);
            var user = _userManager.FindByEmail(email);
            var model = new UserProfile
            {
                Email = email,
                Role = GetRole(user),
                LockoutDuration = GetLockountDuration(user.Id),
                ShouldDelete = false
            };
            return View(model);
        }

        public ActionResult SaveUserProfile(UserProfile model)
        {
            var user = _userManager.FindByEmail(model.Email);
            ChangeRole(user.Id, model.Role);
            SetLockoutDuration(user.Id, model.LockoutDuration);
            if (model.ShouldDelete)
            {
                DeleteAccount(user);
            }
            return RedirectToAction("Users");
        }

        private static List<UserProfile> GetUsersList(IEnumerable<User> users,
            IReadOnlyList<string> roles)
        {
            return users.Select((model, i) => new UserProfile
            {
                Email = model.Email,
                Role = roles[i]
            }).ToList();
        }

        private string GetRole(IUser<string> user)
            =>
                _userManager.IsInRole(user.Id, Common.Constants.AdminRoleName)
                    ? Common.Constants.AdminRoleName
                    : (_userManager.IsInRole(user.Id, Common.Constants.UnderwriterRoleName)
                        ? Common.Constants.UnderwriterRoleName
                        : (_userManager.IsInRole(user.Id, Common.Constants.SecretaryRoleName)
                            ? Common.Constants.SecretaryRoleName
                            : Common.Constants.ClientRoleName));

        private static string CheckEmail(string email)
        {
            if (email == null)
            {
                email = _lastUserEmail;
            }
            _lastUserEmail = email;
            return email;
        }

        private int GetLockountDuration(string id)
            => Math.Max((int) (_userManager.GetLockoutEndDate(id) - DateTime.Now).TotalMinutes, 0);

        private void DeleteAccount(User user)
        {
            RemoveUserFromRoles(user.Id);
            _userManager.Delete(user);
        }

        private void RemoveUserFromRoles(string id)
        {
            foreach (var role in _userManager.GetRoles(id))
            {
                _userManager.RemoveFromRole(id, role);
            }
        }

        private void SetLockoutDuration(string id, int lockDuration)
        {
            _userManager.SetLockoutEnabled(id, true);
            _userManager.SetLockoutEndDate(id, DateTime.Now + new TimeSpan(0, lockDuration, 0));
        }

        private void ChangeRole(string id, string role)
        {
            _userManager.RemoveFromRole(id, Common.Constants.AdminRoleName);
            _userManager.RemoveFromRole(id, Common.Constants.UnderwriterRoleName);
            _userManager.RemoveFromRole(id, Common.Constants.SecretaryRoleName);
            _userManager.RemoveFromRole(id, Common.Constants.ClientRoleName);
            _userManager.AddToRole(id, role);
        }
    }
}
