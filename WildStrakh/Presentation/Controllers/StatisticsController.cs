﻿using System;
using System.Web.Mvc;
using Business.Contracts;
using Business.ViewModels;
using Common;
using PagedList;

namespace Presentation.Controllers
{
    [Authorize(Roles = Constants.AdminRoleName)]
    public class StatisticsController : Controller
    {
        private readonly IStatisticsService _statisticsService;

        public StatisticsController(IStatisticsService statisticsService)
        {
            _statisticsService = statisticsService;
        }

        public ActionResult GetActiveContracts(int page = 1, int pageSize = 10)
        {
            var result = _statisticsService.GetActiveContracts();
            var pagedList = new PagedList<Contract>(result.Value, page, pageSize);
            return result.HasSucceded ? View(pagedList) : View("Error");
        }

        public ActionResult GetCompletedTransactions(int page = 1, int pageSize = 10)
        {
            var result = _statisticsService.GetCompletedTransactions(DateTime.MinValue, DateTime.Now);
            var pagedList = new PagedList<Transaction>(result.Value, page, pageSize);
            ViewBag.Action = "GetCompletedTransactions";
            return result.HasSucceded ? View("GetTransactions", pagedList) : View("Error");
        }

        public ActionResult GetTransactions(int page = 1, int pageSize = 10)
        {
            var result = _statisticsService.GetAllTransactions();
            var pagedList = new PagedList<Transaction>(result.Value, page, pageSize);
            ViewBag.Action = "GetTransactions";
            return result.HasSucceded ? View(pagedList) : View("Error");
        }

        public ActionResult GetAccounts(int page = 1, int pageSize = 10)
        {
            var result = _statisticsService.GetAccounts();
            var pagedList = new PagedList<Account>(result.Value, page, pageSize);
            return result.HasSucceded ? View(pagedList) : View("Error");
        }
    }
}
