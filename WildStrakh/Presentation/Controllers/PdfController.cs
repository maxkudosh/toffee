﻿using System;
using System.Web.Mvc;
using Business.Contracts;
using Presentation.ActionResults;

namespace Presentation.Controllers
{
    public class PdfController : Controller
    {

        private const int ContractCacheDuration = 60 * 60 * 4;

        private readonly IContractService _contractService;

        public PdfController(IContractService contractService)
        {
            _contractService = contractService;
        }

        [OutputCache(VaryByCustom = "id", Duration = ContractCacheDuration)]
        public ActionResult GetContract(Guid id)
        {
            var result = _contractService.GetContract(id);
            return result.HasSucceded ? new PdfActionResult(result.Value, $"{id}.pdf") : null;
        }
    }
}
