﻿using System.Linq;
using System.Security.Claims;
using System.Web.Mvc;

namespace Presentation.Controllers
{
    [Authorize]
    public class AppController : Controller
    {
        [Authorize]
        public ActionResult Index(string returnUrl)
        {
            Response.SetCookie(new System.Web.HttpCookie("username", User.Identity.Name));
            Response.SetCookie(new System.Web.HttpCookie("userrole",
                ((ClaimsIdentity) User.Identity).Claims.First(
                    c => c.Type == ((ClaimsIdentity) User.Identity).RoleClaimType).Value));
            return View();
        }
    }
}