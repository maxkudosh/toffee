﻿using System;
using System.Threading.Tasks;
using System.Web.Mvc;
using Domain.Entities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.DataProtection;
using Presentation.Models.Account;

namespace Presentation.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        private readonly ApplicationSignInManager _signInManager;

        private readonly IAuthenticationManager _authenticationManager;

        private readonly ApplicationUserManager _userManager;

        public AccountController(ApplicationUserManager userManager,
            ApplicationSignInManager signInManager, IAuthenticationManager authenticationManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _authenticationManager = authenticationManager;
            _userManager.UserTokenProvider =
                new DataProtectorTokenProvider<User>(
                    new DpapiDataProtectionProvider().Create("Email"));
        }

        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            if (CheckUserExistance(model.Email))
            {
                var user = _userManager.FindByEmail(model.Email);
                if (user != null && !(_userManager.IsEmailConfirmed(user.Id)))
                {
                    ModelState.AddModelError(string.Empty, @"Почта не подтвержена");
                    return View(model);
                }
                if (_userManager.IsLockedOut(_userManager.FindByEmail(model.Email).Id))
                {
                    ViewBag.Duration = GetLockOutDuration(model.Email);
                    return View("Lockout");
                }
                var result =
                    await _signInManager.PasswordSignInAsync(model.Email, model.Password, false, true);
                switch (result)
                {
                    case SignInStatus.Success:
                    {
                        return RedirectToAction("Manage", "Account");
                    }
                    case SignInStatus.LockedOut:
                    {
                        return View("Lockout");
                    }
                }
            }
            ModelState.AddModelError(string.Empty, @"Ошибка входа.");
            return View(model);
        }

        [AllowAnonymous]
        public ActionResult Register() => View();

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var user = new User
            {
                UserName = model.Email,
                Email = model.Email,
                EmailConfirmed = true
            };
            var result = await _userManager.CreateAsync(user, model.Password);
            if (result.Succeeded)
            {
                if (Request.Url == null)
                {
                    return View("CompleteRegistration");
                }
                var callbackUrl = await GetCallbackUrlForRegister(user.Id);
                await
                    _userManager.SendEmailAsync(user.Id, "Создание аккаунта",
                        $"Пройдите по <a href=\"{callbackUrl}\">ссылке</a>");
                ViewBag.Link = callbackUrl;
                _userManager.AddToRole(user.Id, Common.Constants.ClientRoleName);
                await _signInManager.SignInAsync(user, false, false);
                _authenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
                return View("CompleteRegistration");
            }
            AddErrors(result);
            return View(model);
        }

        [AllowAnonymous]
        public async Task<ActionResult> ConfirmEmail(string userId, string code)
            =>
            // TODO: temp
                userId == null || code == null
                    ? View("Error")
                    : View((await _userManager.ConfirmEmailAsync(userId, code)).Succeeded
                        ? "ConfirmEmail"
                        : "ConfirmEmail");

        [AllowAnonymous]
        public ActionResult ForgotPassword() => View();

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var user = await _userManager.FindByNameAsync(model.Email);
            if (user == null || !(await _userManager.IsEmailConfirmedAsync(user.Id)))
            {
                return View("ForgotPasswordConfirmation");
            }
            await
                _userManager.SendEmailAsync(user.Id, "Восстановление пароля",
                    $"Пройдите по <a href=\"{GetCallbackUrlForReset(user.Id)}\">ссылке</a>");
            return RedirectToAction("ForgotPasswordConfirmation", "Account");
        }

        [AllowAnonymous]
        public ActionResult ForgotPasswordConfirmation() => View();

        [AllowAnonymous]
        public ActionResult ResetPassword(string code) => code == null ? View("Error") : View();

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var user = await _userManager.FindByNameAsync(model.Email);
            if (user == null)
            {
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            var result = await _userManager.ResetPasswordAsync(user.Id, model.Code, model.Password);
            if (result.Succeeded)
            {
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            AddErrors(result);
            return View();
        }

        [AllowAnonymous]
        public ActionResult ResetPasswordConfirmation() => View();

        public ActionResult Manage() => View();

        public ActionResult Logout()
        {
            _authenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            return RedirectToAction("Login", "Account");
        }

        private bool CheckUserExistance(string email) => _userManager.FindByEmail(email) != null;

        private string GetLockOutDuration(string email)
        {
            var duration = _userManager.GetLockoutEndDate(_userManager.FindByEmail(email).Id) -
                DateTime.Now;
            return duration.Days > 0
                ? $"{duration.Days} {"дней"}"
                : $"{duration.Hours}:{duration.Minutes}:{duration.Seconds}";
        }

        private async Task<string> GetCallbackUrlForReset(string userId)
        {
            return Url.Action("ResetPassword", "Account", new
            {
                userId,
                code = await _userManager.GeneratePasswordResetTokenAsync(userId)
            }, Request.Url?.Scheme);
        }

        private async Task<string> GetCallbackUrlForRegister(string userId)
        {
            return Url.Action("ConfirmEmail", "Account", new
            {
                userId,
                code = await _userManager.GenerateEmailConfirmationTokenAsync(userId)
            }, Request.Url?.Scheme);
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError(string.Empty, error);
            }
        }
    }
}
