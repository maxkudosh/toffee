﻿using Microsoft.AspNet.Identity;

namespace Presentation.Services
{
    public interface IEmailSender: IIdentityMessageService
    {
    }
}