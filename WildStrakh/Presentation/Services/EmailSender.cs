﻿using System.Net.Mail;
using System.Threading.Tasks;
using Mail;
using Microsoft.AspNet.Identity;

namespace Presentation.Services
{
    public class EmailSender: IEmailSender
    {
        public Task SendAsync(IdentityMessage message)
            =>
                EmailService.CreateSmtpClient().SendMailAsync(
                    new MailMessage(Common.Constants.SystemEmail, message.Destination)
                    {
                        Subject = message.Subject,
                        Body = message.Body,
                        IsBodyHtml = true
                    });
    }
}
