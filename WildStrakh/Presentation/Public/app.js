﻿(function (angular, cookie) {

    'use strict';

    function getDependentModule() {
        if(cookie.includes('userrole=Secretary')) {
            return 'wild.secretary';
        }
        if (cookie.includes('userrole=Client')) {
            return 'wild.user';
        }
        if (cookie.includes('userrole=Underwriter')) {
            return 'wild.under';
        }
    }

    function configureRoutes($stateProvider, $urlRouterProvider, $resourceProvider, ROUTES, $provide, $httpProvider) {

        const defaultRoute = ROUTES.getDefault();
        $urlRouterProvider.otherwise(defaultRoute.path);

        $stateProvider
            .state(ROUTES.home.stateName, {
                url: ROUTES.home.path,
                templateUrl: '/public/app.html'
            });

        $resourceProvider.defaults.actions.update = { method: 'PUT' };

        $resourceProvider.defaults.actions.query = {
            method: 'GET',
            isArray: true,
            transformResponse: response => angular.fromJson(response).data
        };

        $provide.factory('httpInterceptor', ['$q', '$rootScope', function ($q, $rootScope) {
            return {
                'request': function (config) {
                    $rootScope.$broadcast('httpCallStarted', config);
                    return config || $q.when(config);
                },
                'response': function (response) {
                    $rootScope.$broadcast('httpCallEnded', response);
                    return response || $q.when(response);
                },
                'requestError': function (rejection) {
                    $rootScope.$broadcast('httpCallEnded', rejection);
                    return $q.reject(rejection);
                },
                'responseError': function (rejection) {
                    $rootScope.$broadcast('httpCallEnded', rejection);
                    return $q.reject(rejection);
                }
            };
        }]);
        $httpProvider.interceptors.push('httpInterceptor');


    }

    configureRoutes.$inject = ['$stateProvider', '$urlRouterProvider', '$resourceProvider', 'ROUTES', '$provide', '$httpProvider'];

    angular
        .module('wild', [
            'toastr',
            'ui.router',
            'wild.navbar',
            'wild.routes',
            getDependentModule()
        ])
        .config(configureRoutes);

})(angular, document.cookie);
