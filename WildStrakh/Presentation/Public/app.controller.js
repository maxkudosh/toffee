﻿(function (angular, document) {

    'use strict';

    function AppController(scope) {

        let activeRequests = 0;
        this.showSpinner = false;

        var that = this;

        scope.$on('httpCallStarted', function ()
        {
            if (activeRequests === 0)
            {
                that.showSpinner = true;
                document.body.className = 'ajax';
                //angular.forEach(document.querySelectorAll('button'), e => e.disabled = true);
            }
            activeRequests++;
        });

        scope.$on('httpCallEnded', function()
        {
            activeRequests--;
            if (activeRequests === 0)
            {
                that.showSpinner = false;
                document.body.className = '';
                //angular.forEach(document.querySelectorAll('button'), e => e.disabled = false);
            }
        });

    }

    AppController.$inject = ['$scope'];
    angular
        .module('wild')
        .controller('AppController', AppController);
})(angular, document);
