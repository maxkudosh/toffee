﻿(function (angular) {

    'use strict';

    function configureRoutes($stateProvider, ROUTES) {

        $stateProvider
            .state(ROUTES.applications.stateName, {
                url: ROUTES.applications.path,
                templateUrl: '/public/user/applications/applications.html'
            });

    }

    configureRoutes.$inject = ['$stateProvider', 'ROUTES'];

    angular
        .module('wild.user.applications', [
            'ui.router',
            'wild.routes',
            'wild.enums',
            'wild.document-status',
            'wild.user.applications.details'
        ])
        .config(configureRoutes);

})(angular);
