﻿(function (angular) {

    'use strict';

    function ApplicationsController(stateManager, routes, applicationService, DocumentStatus) {

        this.routes = routes;
        this.stateManager = stateManager;
        this.applicationService = applicationService;
        this.DocumentStatus = DocumentStatus;

        this.activate();

    }

    ApplicationsController.prototype.activate = function () {
        this.applications = this.applicationService.getAll();
    };

    ApplicationsController.prototype.goToDetails = function (application) {
        return this.stateManager.go(this.routes.application.stateName, { applicationId: application.id });
    };

    ApplicationsController.prototype.needToPay = function (application) {
        return this.DocumentStatus.APPROVED === application.status;
    };

    ApplicationsController.prototype.pay = function () {
        return this.stateManager.go(this.routes.myContracts.stateName);
    };

    ApplicationsController.prototype.needToCorrect = function (application) {
        return this.DocumentStatus.PENDING_CORRECTION === application.status;
    };

    ApplicationsController.prototype.correct = function () {
        return this.stateManager.go(this.routes.corrections.stateName);
    };

    ApplicationsController.prototype.needToAdd = function (application) {
        return this.DocumentStatus.PENDING_ADDITIONAL_INFORMATION === application.status;
    };

    ApplicationsController.prototype.add = function () {
        return this.stateManager.go(this.routes.requests.stateName);
    };

    ApplicationsController.$inject = [
        '$state',
        'ROUTES',
        'ApplicationService',
        'DocumentStatus'
    ];

    angular
        .module('wild.user.applications')
        .controller('ApplicationsController', ApplicationsController);

})(angular);
