﻿(function (angular) {

    'use strict';


    function configureRoutes($stateProvider, ROUTES) {

        $stateProvider
            .state(ROUTES.application.stateName, {
                url: ROUTES.application.path,
                templateUrl: '/public/user/applications/details/details.html'
            });

    }

    configureRoutes.$inject = ['$stateProvider', 'ROUTES'];

    angular
        .module('wild.user.applications.details', [
            'ngResource',
            'ui.router',
            'wild.attachment',
            'wild.services',
            'wild.application',
            'wild.additional-information',
            'wild.information-correction',
            'wild.routes'
        ])
        .config(configureRoutes);

})(angular);
