﻿(function (angular) {

    'use strict';

    function ApplicationDetailsController(stateManager, routes, applicationService, correctionService, informationService, toastr) {

        this.routes = routes;
        this.stateManager = stateManager;
        this.applicationService = applicationService;
        this.correctionService = correctionService;
        this.informationService = informationService;
        this.toastr = toastr;

        this.applicationId = this.stateManager.params.applicationId;

        this.activate();

    }

    ApplicationDetailsController.prototype.activate = function () {
        this.application = this.applicationService.getDetails(this.applicationId);
        this.corrections = this.correctionService.getByApplicationId(this.applicationId);
        this.informations = this.informationService.getByApplicationId(this.applicationId);
    };


    ApplicationDetailsController.$inject = [
        '$state',
        'ROUTES',
        'ApplicationService',
        'InformationCorrectionService',
        'AdditionalInformationService',
        'toastr'
    ];

    angular
        .module('wild.user.applications.details')
    .controller('ApplicationDetailsController', ApplicationDetailsController);

})(angular);
