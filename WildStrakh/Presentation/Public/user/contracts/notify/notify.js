﻿(function (angular) {

    'use strict';

    let popup;

    function configureRoutes($stateProvider, ROUTES) {

        $stateProvider
            .state(ROUTES.notifyContractNested.stateName, {
                url: ROUTES.notifyContractNested.path,
                onEnter: [
                    '$uibModal', function(modal) {
                        popup = modal.open({
                            templateUrl: '/public/user/contracts/notify/notify.html',
                            backdrop: 'static',
                            size: 'lg'
                        });
                    }
                ],
                onExit: function() {
                    popup.close();
                }
            });

    }

    configureRoutes.$inject = ['$stateProvider', 'ROUTES'];

    angular
        .module('wild.user.contracts.notify', [
            'toastr',
            'ui.bootstrap',
            'ui.router',
            'wild.datepicker',
            'wild.attachment',
            'wild.routes'
        ])
        .config(configureRoutes);

})(angular);
