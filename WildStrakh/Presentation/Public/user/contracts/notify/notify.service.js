﻿(function (angular) {

    'use strict';

    function NotifyService(apiWrapper, ResourceWithAttachmentsService) {

        this.service = new ResourceWithAttachmentsService(
            'attachments',
            '/api/insurancecasenotice/:insurancecasenoticeid/',
            apiWrapper);

    }

    NotifyService.prototype.create = function (model) {
        return this.service.api.save(model);
    };

    NotifyService.$inject = ['$resource', 'ResourceWithAttachmentsService'];

    angular
        .module('wild.user.contracts.notify')
        .service('NotifyService', NotifyService);

})(angular);
