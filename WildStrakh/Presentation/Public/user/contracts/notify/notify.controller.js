﻿(function (angular) {

    'use strict';

    function NotifyController(stateManager, routes, notifyService, toastr) {

        this.routes = routes;
        this.stateManager = stateManager;
        this.notifyService = notifyService;
        this.toastr = toastr;

        this.activate();

    }

    NotifyController.prototype.activate = function () {

        this.attachments = [];

        this.maxWhen = new Date();

    };

    NotifyController.prototype.submit = function() {

        const notice = {

            contractId: this.stateManager.params.contractId,
            whereabouts: this.where,
            circumstances: this.what,
            incidentDate: this.when.toUTCString(),
            attachments: this.attachments

        };

        this.notifyService
            .create(notice).$promise
            .then(() =>
            {
                this.stateManager.go('^', {}, { reload: true });
                this.toastr.success('Извещение отослано!');
            });

    };

    NotifyController.prototype.cancel = function () {
        this.stateManager.go('^');
    }

    NotifyController.$inject = ['$state', 'ROUTES', 'NotifyService', 'toastr'];

    angular
        .module('wild.user.contracts.notify')
        .controller('NotifyController', NotifyController);

})(angular);
