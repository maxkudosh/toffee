﻿(function (angular) {

    'use strict';

    function contractStatus(ContractStatus) {

        const statusClasses = {
    
            [ContractStatus.ACTIVE.id]: 'label-success',
            [ContractStatus.PENDING_PAYMENT.id]: 'label-info',
            [ContractStatus.CANCELLED.id]: 'label-warning',
            [ContractStatus.TERMINATED.id]: 'label-danger',
            [ContractStatus.CREATED.id]: 'label-default'

        };

        return {
        
            restrict: 'E',
            templateUrl: '/public/user/contracts/contract-status.html',
            scope: {
                status: '@'
            },
            link: function(scope) {

                scope.statusClass = statusClasses[scope.status];
                scope.statusName = Object
                    .keys(ContractStatus)
                    .map(key => ContractStatus[key])
                    .find(value => value.id === +scope.status).name;

            }

        };

    }

    contractStatus.$inject = ['ContractStatus'];

    angular
        .module('wild.user.contracts')
        .directive('contractStatus', contractStatus);

})(angular);
