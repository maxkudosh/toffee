﻿(function (angular) {

    'use strict';

    function ContractsController(stateManager, routes, ContractStatus, contractService) {

        this.routes = routes;
        this.stateManager = stateManager;
        this.ContractStatus = ContractStatus;
        this.contractService = contractService;

        this.activate();

    }

    ContractsController.prototype.activate = function () {
        this.contracts = this.contractService.getAll();
    };

    ContractsController.prototype.formatDate = function (date) {
        return date.toLocaleDateString();
    };

    ContractsController.prototype.isActive = function (contract) {
        return contract.status === this.ContractStatus.ACTIVE;
    };

    ContractsController.prototype.isPendingPayment = function (contract) {
        return contract.status === this.ContractStatus.PENDING_PAYMENT;
    };

    ContractsController.prototype.cancel = function (contract) {
        return this.stateManager.go(this.routes.cancelContract.stateName, { contractId: contract.id });
    };

    ContractsController.prototype.notify = function (contract) {
        return this.stateManager.go(this.routes.notifyContractNested.stateName, { contractId: contract.id });
    };

    ContractsController.prototype.pay = function (contract) {
        return this.stateManager.go(this.routes.acceptContract.stateName, { contractId: contract.id });
    };

    ContractsController.$inject = [
        '$state',
        'ROUTES',
        'ContractStatus',
        'ContractService'
    ];

    angular
        .module('wild.user.contracts')
        .controller('ContractsController', ContractsController);

})(angular);
