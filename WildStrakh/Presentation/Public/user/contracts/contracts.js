﻿(function (angular) {

    'use strict';

    function configureRoutes($stateProvider, ROUTES) {

        $stateProvider
            .state(ROUTES.myContracts.stateName, {
                url: ROUTES.myContracts.path,
                templateUrl: '/public/user/contracts/contracts.html'
            });

    }

    configureRoutes.$inject = ['$stateProvider', 'ROUTES'];

    angular
        .module('wild.user.contracts', [
            'ngResource',
            'ui.router',
            'wild.routes',
            'wild.enums',
            'wild.user.contracts.cancel',
            'wild.user.contracts.notify',
            'wild.user.contracts.accept'
        ])
        .config(configureRoutes);

})(angular);
