﻿(function (angular) {

    'use strict';

    function AcceptController(stateManager, routes, acceptService, toastr) {

        this.routes = routes;
        this.stateManager = stateManager;
        this.acceptService = acceptService;
        this.toastr = toastr;

        this.id = stateManager.params.contractId;

        this.activate();

    }

    AcceptController.prototype.activate = function () {
        this.card = {};
    };


    AcceptController.prototype.cancel = function () {
        this.stateManager.go('^');
    }

    AcceptController.prototype.showMonthError = function () {
        return this.card.month && !(this.card.month <= 12 && this.card.month >= 1);
    }

    AcceptController.prototype.showYearError = function () {
        return this.card.year && this.card.year < new Date().getYear();
    }

    AcceptController.prototype.reject = function () {
        this.acceptService
            .cancel(this.id).$promise
            .then(() => {
                this.stateManager.go('^', {}, { reload: true });
                this.toastr.success('Вы отказали!');
            });
    }

    AcceptController.prototype.submit = function () {

        const model = {

            contractId: this.id,
            card: {
                cardNumber: this.card.number,
                cardholderName: this.card.holderName,
                cvc: this.card.cvc,
                expirationDate: {
                    month: this.card.month,
                    year: this.card.year
                }
            }

        };

        this.acceptService
            .pay(model).$promise
            .then(() =>
            {
                this.stateManager.go('^', {}, { reload: true });
                this.toastr.success('Оплата принята! Договор теперь активен.');
            });

    };

    AcceptController.$inject = ['$state', 'ROUTES', 'AcceptService', 'toastr'];

    angular
        .module('wild.user.contracts.accept')
        .controller('AcceptController', AcceptController);

})(angular);
