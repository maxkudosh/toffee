﻿(function (angular) {

    'use strict';

    let popup;

    function configureRoutes($stateProvider, ROUTES) {

        $stateProvider
            .state(ROUTES.acceptContract.stateName, {
                url: ROUTES.acceptContract.path,
                onEnter: [
                    '$uibModal', function(modal) {
                        popup = modal.open({
                            templateUrl: '/public/user/contracts/accept/accept.html',
                            backdrop: 'static',
                            size: 'lg',
                            keyboard: false
                        });
                    }
                ],
                onExit: function() {
                    popup.close();
                }
            });

    }

    configureRoutes.$inject = ['$stateProvider', 'ROUTES'];

    angular
        .module('wild.user.contracts.accept', [
            'ui.router',
            'credit-cards',
            'wild.routes'
        ])
        .config(configureRoutes);

})(angular);
