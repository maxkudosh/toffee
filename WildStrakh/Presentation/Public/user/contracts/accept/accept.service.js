﻿(function (angular) {

    'use strict';

    function AcceptService(apiWrapper) {

        this.api = apiWrapper('api/contract/pay/');
        this.rejectApi = apiWrapper('api/contract/cancel/');

    }

    AcceptService.prototype.pay = function (model) {
        return this.api.update(model);
    };

    AcceptService.prototype.cancel = function (id) {
        return this.rejectApi.update({ contractId: id });
    };


    AcceptService.$inject = ['$resource'];

    angular
        .module('wild.user.contracts.accept')
        .service('AcceptService', AcceptService);

})(angular);
