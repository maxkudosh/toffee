﻿(function (angular) {

    'use strict';

    let popup;

    function configureRoutes($stateProvider, ROUTES) {

        $stateProvider
            .state(ROUTES.cancelContract.stateName, {
                url: ROUTES.cancelContract.path,
                onEnter: ['$uibModal', function (modal) {
                    popup = modal.open({
                        templateUrl: '/public/user/contracts/cancel/cancel.html',
                        backdrop: 'static'
                    });
                }],
                onExit: function () {
                    popup.close();
                }
            });

    }

    configureRoutes.$inject = ['$stateProvider', 'ROUTES'];

    angular
        .module('wild.user.contracts.cancel', [
            'toastr',
            'ui.router',
            'wild.attachment',
            'wild.services',
            'wild.routes'
        ])
        .config(configureRoutes);

})(angular);
