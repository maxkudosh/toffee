﻿(function (angular) {

    'use strict';

    function CancelController(stateManager, terminationApplicationService, toastr) {

        this.toastr = toastr;
        this.stateManager = stateManager;
        this.terminationApplicationService = terminationApplicationService;

        this.contractId = stateManager.params.contractId;

        this.activate();

    }

    CancelController.prototype.activate = function () {

        this.attachments = [];

    };

    CancelController.prototype.submit = function () {

        const newApplication = {
            contractId: this.contractId,
            comment: this.comment,
            attachments: this.attachments
        };

        this.terminationApplicationService
            .create(newApplication).$promise
            .then(() =>
            {
                this.stateManager.go('^', { reload: true });
                this.toastr.success('Заявление отправлено!');
            });

    };

    CancelController.$inject = ['$state', 'TerminationApplicationService', 'toastr'];

    angular
        .module('wild.user.contracts.cancel')
        .controller('CancelController', CancelController);

})(angular);
