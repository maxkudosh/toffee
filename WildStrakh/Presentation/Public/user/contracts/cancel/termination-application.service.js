﻿(function (angular) {

    'use strict';

    function TerminationApplicationService(apiWrapper, ResourceWithAttachmentsService) {

        this.service = new ResourceWithAttachmentsService(
            'attachments',
            '/api/terminationapplication/:terminationApplicationId/',
            apiWrapper);

    }

    TerminationApplicationService.prototype.create = function (model) {
        return this.service.api.save(model);
    };

    TerminationApplicationService.$inject = ['$resource', 'ResourceWithAttachmentsService'];

    angular
        .module('wild.user.contracts.cancel')
        .service('TerminationApplicationService', TerminationApplicationService);

})(angular);
