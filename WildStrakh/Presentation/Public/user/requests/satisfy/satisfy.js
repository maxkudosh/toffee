﻿(function (angular) {

    'use strict';

    let popup;

    function configureRoutes($stateProvider, ROUTES) {

        $stateProvider
            .state(ROUTES.satisfyRequest.stateName, {
                url: ROUTES.satisfyRequest.path,
                params: { type: null },
                onEnter: [
                    '$uibModal', function(modal) {
                        popup = modal.open({
                            templateUrl: '/public/user/requests/satisfy/satisfy.html',
                            backdrop: 'static',
                            size: 'lg'
                        });
                    }
                ],
                onExit: function() {
                    popup.close();
                }
            });

    }

    configureRoutes.$inject = ['$stateProvider', 'ROUTES'];

    angular
        .module('wild.user.requests.satisfy', [
            'toastr',
            'ui.router',
            'wild.attachment',
            'wild.services',
            'wild.routes'
        ])
        .config(configureRoutes);

})(angular);
