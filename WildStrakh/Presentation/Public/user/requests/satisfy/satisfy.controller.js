﻿(function (angular) {

    'use strict';

    function SatisfyController(stateManager, routes, informationService, toastr) {

        this.routes = routes;
        this.requestId = stateManager.params.requestId;
        this.type = stateManager.params.type;
        this.informationService = informationService;
        this.stateManager = stateManager;
        this.toastr = toastr;

        this.activate();

    }

    SatisfyController.prototype.activate = function () {

        this.attachments = [];
        this.content = null;

        this.request = this.informationService.getDetails(this.requestId);

    };

    SatisfyController.prototype.cancel = function () {
        this.stateManager.go('^');
    };

    SatisfyController.prototype.submit = function () {

        const satisfaction = {

            id: this.requestId,
            content: this.content,
            attachments: this.attachments

        };

        let promise;

        if (this.type === 'a') {
            promise = this.informationService.respondForApplication(satisfaction);
        } else if (this.type === 'c') {
            promise = this.informationService.respondForCase(satisfaction);
        } else {
            promise = this.informationService.respondForTermination(satisfaction);
        }

        promise.$promise.then(() => {
            this.stateManager.go('^', {}, { reload: true });
            this.toastr.success('Ответ отправлен!');
        });

    };

    SatisfyController.$inject = [
        '$state',
        'ROUTES',
        'AdditionalInformationService',
        'toastr'
    ];

    angular
        .module('wild.user.requests.satisfy')
        .controller('SatisfyController', SatisfyController);

})(angular);
