﻿(function (angular) {

    'use strict';

    function configureRoutes($stateProvider, ROUTES) {

        $stateProvider
            .state(ROUTES.requests.stateName, {
                url: ROUTES.requests.path,
                templateUrl: '/public/user/requests/requests.html'
            });

    }

    configureRoutes.$inject = ['$stateProvider', 'ROUTES'];

    angular
        .module('wild.user.requests', [
            'ui.router',
            'wild.routes',
            'wild.enums',,
            'wild.user.requests.view',
            'wild.user.requests.satisfy'
        ])
        .config(configureRoutes);

})(angular);
