﻿(function (angular) {

    'use strict';

    function RequestsController(stateManager, routes, informationService) {

        this.routes = routes;
        this.stateManager = stateManager;
        this.informationService = informationService;

        this.activate();

    }

    RequestsController.prototype.activate = function () {
        this.appInformations = this.informationService.getForApplication();
        this.caseInformations = this.informationService.getForCase();
        this.terminationInformations = this.informationService.getForTermination();
    };

    RequestsController.prototype.satisfy = function (request, type) {
        return this.stateManager.go(this.routes.satisfyRequest.stateName, { requestId: request.id, type: type });
    };

    RequestsController.prototype.viewAnswer = function (request) {
        return this.stateManager.go(this.routes.viewRequest.stateName, { requestId: request.id });
    };

    RequestsController.$inject = [
        '$state',
        'ROUTES',
        'AdditionalInformationService'
    ];

    angular
        .module('wild.user.requests')
        .controller('RequestsController', RequestsController);

})(angular);
