﻿(function (angular) {

    'use strict';

    function ViewService(apiWrapper) {

        this.api = apiWrapper('/api/additionalinformationrequest/:requestId/additionalinformation/');

    }

    ViewService.prototype.get = function (requestId) {
        return this.api.get({ requestId: requestId });
    };

    ViewService.$inject = ['$resource'];

    angular
        .module('wild.user.requests.view')
        .service('ViewService', ViewService);

})(angular);
