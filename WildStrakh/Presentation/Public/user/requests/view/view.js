﻿(function (angular) {

    'use strict';

    let popup;

    function configureRoutes($stateProvider, ROUTES) {

        $stateProvider
            .state(ROUTES.viewRequest.stateName, {
                url: ROUTES.viewRequest.path,
                onEnter: [
                    '$uibModal', function(modal) {
                        popup = modal.open({
                            templateUrl: '/public/user/requests/view/view.html',
                            backdrop: 'static',
                            size: 'lg'
                        });
                    }
                ],
                onExit: function() {
                    popup.close();
                }
            });

    }

    configureRoutes.$inject = ['$stateProvider', 'ROUTES'];

    angular
        .module('wild.user.requests.view', [
            'ngResource',
            'ui.router',
            'wild.attachment',
            'wild.routes'
        ])
        .config(configureRoutes);

})(angular);
