﻿(function (angular) {

    'use strict';

    function ViewController(stateManager, routes, requestService, viewService)
    {
        this.requestId = stateManager.params.requestId;
        this.routes = routes;
        this.stateManager = stateManager;
        this.requestService = requestService;
        this.viewService = viewService;

        this.activate();

    }

    ViewController.prototype.activate = function ()
    {
        this.request = this.requestService.get(this.requestId);
        this.response = this.viewService.get(this.requestId);
    };

    ViewController.$inject = ['$state', 'ROUTES', 'RequestService', 'ViewService'];

    angular
        .module('wild.user.requests.view')
        .controller('ViewController', ViewController);

})(angular);
