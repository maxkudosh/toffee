﻿(function (angular) {

    'use strict';

    function mapClientToServer(model) {
        
        const illnesses = Object
            .keys(model.illnesses)
            .map(key => model.illnesses[key]
                             .map(i => { return { id: i.id, name: i.name, IllnessType: +key }; }))
            .reduce((a, b) => a.concat(b), []);

        model.applicant.illnesses = illnesses;
        model.applicant.address = model.address;
        model.applicant.occupation = +model.occupation;
        model.applicant.occupationDetails = model.occupationDetails;

        return {
            insuranceType: +model.insuranceType,
            insuranceSum: model.sum,
            paymentOrder: +model.paymentOrder,
            applicant: model.applicant,
            beneficiary: (model.isLifeInsurance() || model.isBeneficiary) ? model.applicant : model.beneficiary
        };

    }

    function InsuranceApplicationService(apiWrapper) {

        this.api = apiWrapper('/api/insuranceapplication/:applicationId/');

    }

    InsuranceApplicationService.prototype.create = function(application) {

        return this.api.save(mapClientToServer(application));

    };

    InsuranceApplicationService.$inject = ['$resource'];

    angular
        .module('wild.user.apply')
        .service('InsuranceApplicationService', InsuranceApplicationService);

})(angular);
