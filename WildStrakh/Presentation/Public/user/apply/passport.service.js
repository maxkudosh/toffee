﻿(function (angular) {

    'use strict';

    const passportApiUri = '/api/passportseries/';

    function passportServiceBuilder(cachedServiceFactory) {

        return cachedServiceFactory(passportApiUri);

    }

    passportServiceBuilder.$inject = ['CachedServiceFactory'];

    angular
        .module('wild.user.apply')
        .service('PassportService', passportServiceBuilder);

})(angular);
