﻿(function (angular) {

    'use strict';

    function configureRoutes($stateProvider, ROUTES) {

        $stateProvider
            .state(ROUTES.apply.stateName, {
                url: ROUTES.apply.path,
                templateUrl: '/public/user/apply/apply.html'
            });

    }

    configureRoutes.$inject = ['$stateProvider', 'ROUTES'];

    angular
        .module('wild.user.apply', [
            'toastr',
            'ngResource',
            'ui.bootstrap',
            'ui.router',
            'wild.routes',
            'wild.services',
            'wild.address',
            'wild.illness',
            'wild.enums'
        ])
        .config(configureRoutes);

})(angular);
