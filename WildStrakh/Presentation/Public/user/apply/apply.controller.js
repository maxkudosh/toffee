﻿(function (angular) {

    'use strict';

    function InsuranceApplicationController(stateManager, applicationService, Occupation, PaymentOrder, InsuranceType, routes, toastr) {

        this.routes = routes;

        this.Occupation = Occupation;
        this.PaymentOrder = PaymentOrder;
        this.InsuranceType = InsuranceType;
        this.stateManager = stateManager;
        this.applicationService = applicationService;
        this.toastr = toastr;

        this.activate();

    }

    InsuranceApplicationController.prototype.activate = function() {

        this.applicant = {};

        this.isBeneficiary = true;

        this.beneficiary = {};

        this.address = {};

        this.illnesses = {};

    };

    InsuranceApplicationController.prototype.submit = function() {

        this.applicationService
            .create(this).$promise
            .then(() => {
                this.stateManager.go(this.routes.myContracts.stateName);
                this.toastr.success('Ваше заявление принято! Вы получите уведомление, когда оно будет рассмотрено.');
            });

    };
    
    InsuranceApplicationController.prototype.isOtherOccupation = function () {

        return +this.occupation === this.Occupation.Other.id;

    };

    InsuranceApplicationController.prototype.isLifeInsurance = function() {

        return +this.insuranceType === this.InsuranceType.LIFE.id;

    };

    InsuranceApplicationController.$inject = [
        '$state',
        'InsuranceApplicationService',
        'Occupation',
        'PaymentOrder',
        'InsuranceType',
        'ROUTES',
        'toastr'
    ];

    angular
        .module('wild.user.apply')
        .controller('InsuranceApplicationController', InsuranceApplicationController);

})(angular);
