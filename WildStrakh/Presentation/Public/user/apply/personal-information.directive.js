﻿(function (angular) {

    'use strict';

    function PersonalInformationController(passportService) {
        this.passportSeries = passportService.getAll();

        this.maxBirthDate = new Date();
        this.minBirthDate = new Date('01/01/1900');

    }

    PersonalInformationController.$inject = ['PassportService'];

    function personalInformation() {

        return {

            restrict: 'E',
            templateUrl: '/public/user/apply/personal-information.html',
            scope: {
                info: '=info'
            },
            controller: PersonalInformationController,
            controllerAs: 'controller'
        };

    }

    angular
        .module('wild.user.apply')
        .directive('personalInformation', personalInformation);

})(angular);
