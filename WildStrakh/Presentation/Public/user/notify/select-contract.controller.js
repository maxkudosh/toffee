﻿(function (angular) {

    'use strict';

    function SelectContractController(stateManager, routes, ContractStatus, contractService) {

        this.routes = routes;
        this.stateManager = stateManager;
        this.ContractStatus = ContractStatus;
        this.contractService = contractService;

        this.activate();

    }

    SelectContractController.prototype.activate = function () {

        this.contracts = this.contractService.getAll();

        this.contracts.$promise.then(() => {

            if (this.contracts.filter(c => c.isActive).length === 1) {

                const theOnlyActiveContract = this.contracts.find(c => c.isActive);

                this.stateManager.go(this.routes.notifyContractNested.stateName, { contractId: theOnlyActiveContract.id });

            }

        });

    };

	SelectContractController.prototype.isContractActive = function (contract) {
        return contract.isActive;
    };
	
    SelectContractController.prototype.notify = function (contract) {
        return this.stateManager.go(this.routes.notifyContractNested.stateName, { contractId: contract.id });
    };

    SelectContractController.$inject = [
        '$state',
        'ROUTES',
        'ContractStatus',
        'ContractService'
    ];

    angular
        .module('wild.user.notify')
        .controller('SelectContractController', SelectContractController);

})(angular);
