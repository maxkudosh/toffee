﻿(function (angular) {

    'use strict';

    function configureRoutes($stateProvider, ROUTES) {

        $stateProvider
            .state(ROUTES.notify.stateName, {
                url: ROUTES.notify.path,
                templateUrl: '/public/user/notify/select-contract.html'
            });

    }

    configureRoutes.$inject = ['$stateProvider', 'ROUTES'];

    angular
        .module('wild.user.notify', [
            'ui.router',
            'wild.routes',
            'wild.services',
            'wild.enums',
            'wild.user.contracts.notify'
        ])
        .config(configureRoutes);

})(angular);
