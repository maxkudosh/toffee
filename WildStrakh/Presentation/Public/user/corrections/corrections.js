﻿(function (angular) {

    'use strict';

    function configureRoutes($stateProvider, ROUTES) {

        $stateProvider
            .state(ROUTES.corrections.stateName, {
                url: ROUTES.corrections.path,
                templateUrl: '/public/user/corrections/corrections.html'
            });

    }

    configureRoutes.$inject = ['$stateProvider', 'ROUTES'];

    angular
        .module('wild.user.corrections', [
            'ui.router',
            'wild.routes',
            'wild.services',
            'wild.user.corrections.correct'
        ])
        .config(configureRoutes);

})(angular);
