﻿(function (angular) {

    'use strict';

    function CorrectionController(stateManager, correctionService, toastr) {

        this.stateManager = stateManager;
        this.correctionService = correctionService;

        this.id = stateManager.params.id;
        this.type = stateManager.params.type;
        this.toastr = toastr;

        this.request = this.correctionService.getDetails(this.id);

    }

    CorrectionController.prototype.submit = function () {

        const response = {
            id: this.id,
            content: this.response
        };

        let promise;

        if (this.type === 'a') {
            promise = this.correctionService.respondForApplication(response);
        } else if (this.type === 'c') {
            promise = this.correctionService.respondForCase(response);
        } else {
            promise = this.correctionService.respondForTermination(response);
        }

        promise.$promise.then(() => {
            this.stateManager.go('^', {}, { reload: true });
            this.toastr.success('Ответ отправлен!');
        });

    };

    CorrectionController.$inject = ['$state', 'InformationCorrectionService', 'toastr'];

    angular
        .module('wild.user.corrections.correct')
        .controller('CorrectionController', CorrectionController);

})(angular);
