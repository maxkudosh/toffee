﻿(function (angular) {

    'use strict';

    let popup;

    function configureRoutes($stateProvider, ROUTES) {

        $stateProvider
            .state(ROUTES.correct.stateName, {
                url: ROUTES.correct.path,
                params: { type: null },
                onEnter: ['$uibModal', function (modal) {
                    popup = modal.open({
                        templateUrl: '/public/user/corrections/correct/correction.html',
                        backdrop: 'static'
                    });
                }],
                onExit: function () {
                    popup.close();
                }
            });

    }

    configureRoutes.$inject = ['$stateProvider', 'ROUTES'];

    angular
        .module('wild.user.corrections.correct', [
            'ui.router',
            'wild.routes',
            'wild.services'
        ])
        .config(configureRoutes);

})(angular);
