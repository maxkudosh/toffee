﻿(function (angular) {

    'use strict';

    function CorrectionsController(stateManager, routes, correctionService) {

        this.routes = routes;
        this.stateManager = stateManager;
        this.correctionService = correctionService;

        this.activate();

    }

    CorrectionsController.prototype.activate = function () {

        this.appCorrections = this.correctionService.getForApplication();
        this.caseCorrections = this.correctionService.getForCase();
        this.terminationCorrections = this.correctionService.getForTermination();

    };

    CorrectionsController.prototype.empty = function () {

        return !this.appCorrections.length && !this.caseCorrections.length && !this.terminationCorrections.length;

    };

    CorrectionsController.prototype.isNotClosed = function (correction) {

        return !correction.isClosed;

    };

    CorrectionsController.prototype.formatDate = function (date) {

        return date.toLocaleDateString();

    };

    CorrectionsController.prototype.respond = function (correction, type) {
        return this.stateManager.go(this.routes.correct.stateName, { id: correction.id, type: type });
    };

    CorrectionsController.$inject = [
        '$state',
        'ROUTES',
        'InformationCorrectionService'
    ];

    angular
        .module('wild.user.corrections')
        .controller('CorrectionsController', CorrectionsController);

})(angular);
