﻿(function (angular) {

    'use strict';

    angular
    .module('wild.user', [
        'wild.user.apply',
        'wild.user.notify',
        'wild.user.contracts',
        'wild.user.corrections',
        'wild.user.applications',
        'wild.user.requests'
    ]);

})(angular);
