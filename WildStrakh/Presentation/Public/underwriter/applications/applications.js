﻿(function (angular) {

    'use strict';

    function configureRoutes($stateProvider, ROUTES) {

        $stateProvider
            .state(ROUTES.applications.stateName, {
                url: ROUTES.applications.path,
                templateUrl: '/public/underwriter/applications/applications.html'
            });

    }

    configureRoutes.$inject = ['$stateProvider', 'ROUTES'];

    angular
        .module('wild.under.applications', [
            'ui.router',
            'wild.services',
            'wild.under.applications.details'
        ])
        .config(configureRoutes);

})(angular);
