﻿(function (angular) {

    'use strict';

    function ApplicationsController(stateManager, routes, applicationService) {

        this.routes = routes;
        this.stateManager = stateManager;
        this.applicationService = applicationService;

        this.activate();

    }

    ApplicationsController.prototype.activate = function() {
        this.validated = this.applicationService.getValidated();
        this.processed = this.applicationService.getProcessed();
        this.pending  = this.applicationService.getPendingApproval();
    };

    ApplicationsController.prototype.isEmpty = function () {
        return !this.validated.length && !this.processed.length && !this.pending.length;
    };

    ApplicationsController.prototype.goToDetails = function (application) {
        return this.stateManager.go(this.routes.details.stateName, { applicationId: application.id });
    };

    ApplicationsController.prototype.goToQuestions = function (application) {
        return this.stateManager.go(this.routes.questions.stateName, { applicationId: application.id });
    };

    ApplicationsController.prototype.answerQuestions = function (application) {
        return this.stateManager.go(this.routes.questions.stateName, { applicationId: application.id });
    };

    ApplicationsController.prototype.approveOrReject = function (application) {
        return this.stateManager.go(this.routes.approve.stateName, { applicationId: application.id });
    };

    ApplicationsController.$inject = [
        '$state',
        'ROUTES',
        'ApplicationService'
    ];

    angular
        .module('wild.under.applications')
    .controller('ApplicationsController', ApplicationsController);

})(angular);
