﻿(function (angular) {

    'use strict';

    let popup;

    function configureRoutes($stateProvider, ROUTES) {

        $stateProvider
           .state(ROUTES.request.stateName, {
               url: ROUTES.request.path,
               onEnter: [
                   '$uibModal', function (modal) {
                       popup = modal.open({
                           templateUrl: '/public/underwriter/applications/details/request/request.html',
                           backdrop: 'static',
                           size: 'lg'
                       });
                   }
               ],
               onExit: function () {
                   popup.close();
               }
           });

    }

    configureRoutes.$inject = ['$stateProvider', 'ROUTES'];

    angular
        .module('wild.under.applications.details.request', [
            'wild.services',
            'wild.controllers'
        ])
    .config(configureRoutes);

})(angular);
