﻿(function (angular) {

    'use strict';

    function builder(informationService, controllerFactory) {
        return controllerFactory.create(request => informationService.requestForApplication(request), params => params.applicationId);
    }

    builder.$inject = [
        'AdditionalInformationService',
        'CorrectionControllerFactory'
    ];

    angular
        .module('wild.under.applications.details.request')
        .controller('ApplicationRequestController', builder);

})(angular);
