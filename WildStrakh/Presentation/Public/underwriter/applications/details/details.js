﻿(function (angular) {

    'use strict';


    function configureRoutes($stateProvider, ROUTES) {

        $stateProvider
            .state(ROUTES.details.stateName, {
                url: ROUTES.details.path,
                templateUrl: '/public/underwriter/applications/details/details.html'
            });

    }

    configureRoutes.$inject = ['$stateProvider', 'ROUTES'];

    angular
        .module('wild.under.applications.details', [
            'toastr',
            'wild.services',
            'wild.enums',
            'wild.application',
            'wild.additional-information',
            'wild.information-correction',
            'wild.under.applications.details.questions',
            'wild.under.applications.details.request',
            'wild.under.applications.details.approve'
        ])
        .config(configureRoutes);

})(angular);
