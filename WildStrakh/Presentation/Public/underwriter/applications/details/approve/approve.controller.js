﻿(function (angular) {

    'use strict';

    function ApproveController(stateManager, toastr, routes, approvalService) {

        this.routes = routes;
        this.toastr = toastr;
        this.stateManager = stateManager;
        this.approvalService = approvalService;

        this.id = this.stateManager.params.applicationId;

        this.activate();

    }

    ApproveController.prototype.activate = function () {
        this.contracts = this.approvalService.getVariants(this.id);
    };

    ApproveController.prototype.cancel = function () {
        return this.stateManager.go('^');
    };

    ApproveController.prototype.request = function () {
        return this.stateManager.go(this.routes.request.stateName, {contractId: this.id});
    };

    ApproveController.prototype.allowApproving = function (contract) {
        return !!contract.amount && !!contract.premium && !!contract.endDate;
    };

    ApproveController.prototype.approve = function (contract) {

        this.approvalService
            .approve(contract).$promise
            .then(() => {
                this.stateManager.go('^', {}, { reload: true });
                this.toastr.success('Контракт одобрен и будет предложен клиенту!');
            });

    };

    ApproveController.prototype.reject = function () {

        this.approvalService
            .reject(this.id).$promise
            .then(() => {
                this.stateManager.go('^', {}, { reload: true });
                this.toastr.success('Клиенту в контракте отказано!');
            });

    };

    ApproveController.$inject = [
        '$state',
        'toastr',
        'ROUTES',
        'ApprovalService'
    ];

    angular
        .module('wild.under.applications.details.approve')
        .controller('ApproveController', ApproveController);

})(angular);
