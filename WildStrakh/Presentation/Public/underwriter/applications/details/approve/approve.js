﻿(function (angular) {

    'use strict';

    let popup;

    function configureRoutes($stateProvider, ROUTES) {

        $stateProvider
           .state(ROUTES.approve.stateName, {
               url: ROUTES.approve.path,
               onEnter: [
                   '$uibModal', function (modal) {
                       popup = modal.open({
                           templateUrl: '/public/underwriter/applications/details/approve/approve.html',
                           backdrop: 'static',
                           size: 'lg'
                       });
                   }
               ],
               onExit: function () {
                   popup.close();
               }
           });

    }

    configureRoutes.$inject = ['$stateProvider', 'ROUTES'];

    angular
        .module('wild.under.applications.details.approve', [
            'ngResource',
            'toastr',
            'wild.services',
            'wild.datepicker'
        ])
    .config(configureRoutes);

})(angular);
