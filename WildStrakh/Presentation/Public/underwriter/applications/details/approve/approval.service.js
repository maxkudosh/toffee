﻿(function (angular) {

    'use strict';

    function ApprovalService(apiWrapper) {

        this.variants = apiWrapper('/api/contract/variants/:applicationId');
        this.approval = apiWrapper('/api/contract/approve/');
        this.rejection = apiWrapper('/api/contract/reject/');

    }

    ApprovalService.prototype.getVariants = function (id) {
        return this.variants.get({ applicationId: id });
    };

    ApprovalService.prototype.approve = function (contract) {
        return this.approval.update(contract);
    };

    ApprovalService.prototype.reject = function (id) {
        return this.rejection.update({ applicationId: id });
    };

    ApprovalService.$inject = [
        '$resource'
    ];

    angular
        .module('wild.under.applications.details.approve')
        .service('ApprovalService', ApprovalService);

})(angular);
