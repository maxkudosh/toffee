﻿(function (angular) {

    'use strict';

    function DetailsController(stateManager, routes, applicationService, informationService, toastr, DocumentStatus, correctionService) {

        this.routes = routes;
        this.stateManager = stateManager;
        this.applicationService = applicationService;
        this.informationService = informationService;
        this.correctionService = correctionService;
        this.toastr = toastr;
        this.DocumentStatus = DocumentStatus;

        this.id = this.stateManager.params.applicationId;

        this.activate();

    }

    DetailsController.prototype.activate = function () {
        this.application = this.applicationService.getDetails(this.id);
        this.informations = this.informationService.getByApplicationId(this.id);
        this.corrections = this.correctionService.getByApplicationId(this.id);
    };

    DetailsController.prototype.isReadyForQuestions = function () {
        return this.application.status === this.DocumentStatus.PROCESSED || this.application.status === this.DocumentStatus.VALIDATED;
    };

    DetailsController.prototype.allowActions = function () {
        return this.informations.every(i => i.isClosed);
    };

    DetailsController.prototype.goToQuestions = function () {
        return this.stateManager.go(this.routes.questions.stateName, { applicationId: this.id });
    };

    DetailsController.prototype.isReadyForApproval = function () {
        return this.application.status === this.DocumentStatus.PENDING_APPROVAL;
    };

    DetailsController.prototype.goToApproval = function () {
        return this.stateManager.go(this.routes.approve.stateName, { applicationId: this.id });
    };

    DetailsController.prototype.goToRequest = function () {
        return this.stateManager.go(this.routes.request.stateName, { applicationId: this.id });
    };

    DetailsController.$inject = [
        '$state',
        'ROUTES',
        'ApplicationService',
        'AdditionalInformationService',
        'toastr',
        'DocumentStatus',
        'InformationCorrectionService'
    ];

    angular
        .module('wild.under.applications.details')
    .controller('ApplicationDetailsController', DetailsController);

})(angular);
