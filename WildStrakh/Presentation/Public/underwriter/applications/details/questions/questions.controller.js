﻿(function (angular) {

    'use strict';

    function QuestionsController(stateManager, routes, questionsService, toastr) {

        this.routes = routes;
        this.stateManager = stateManager;
        this.questionsService = questionsService;
        this.toastr = toastr;

        this.applicationId = this.stateManager.params.applicationId;

        this.activate();

    }

    QuestionsController.prototype.activate = function () {
        this.questions = this.questionsService.getForApplication(this.applicationId);
    };

    QuestionsController.prototype.cancel = function () {
        return this.stateManager.go('^');
    };

    QuestionsController.prototype.submit = function () {
    
        function mapAnswer(question) {
            return {
                question: question.content,
                answer: question.answer
            };
        }

        const answers = {
            insuranceApplicationId: this.questions.insuranceApplication.id,
            answers: this.questions.questions.map(mapAnswer)
        };

        this.questionsService
            .answer(answers).$promise
            .then(() => {
                this.stateManager.go('^', {}, { reload: true });
                this.toastr.success('Ответы на вопросы получены!');
            });

    };

    QuestionsController.$inject = [
        '$state',
        'ROUTES',
        'QuestionsService',
        'toastr'
    ];

    angular
        .module('wild.under.applications.details.questions')
    .controller('QuestionsController', QuestionsController);

})(angular);
