﻿(function (angular) {

    'use strict';

    let popup;

    function configureRoutes($stateProvider, ROUTES) {

        $stateProvider
           .state(ROUTES.questions.stateName, {
               url: ROUTES.questions.path,
               onEnter: [
                   '$uibModal', function (modal) {
                       popup = modal.open({
                           templateUrl: '/public/underwriter/applications/details/questions/questions.html',
                           backdrop: 'static',
                           size: 'lg',
                           windowTopClass: 'questions-popup-window'
                       });
                   }
               ],
               onExit: function () {
                   popup.close();
               }
           });

    }

    configureRoutes.$inject = ['$stateProvider', 'ROUTES'];

    angular
        .module('wild.under.applications.details.questions', [
            'toastr',
            'wild.routes',
            'wild.application'
        ])
        .config(configureRoutes);

})(angular);
