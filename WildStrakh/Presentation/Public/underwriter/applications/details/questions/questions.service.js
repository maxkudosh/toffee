﻿(function (angular) {

    'use strict';

    function QuestionsService(apiWrapper) {
        this.api = apiWrapper('/api/questions/:id');
    }

    QuestionsService.prototype.getForApplication = function (id) {
        return this.api.get({ id: id });
    };

    QuestionsService.prototype.answer = function (answers) {
        return this.api.save(answers);
    };

    QuestionsService.$inject = [
        '$resource'
    ];

    angular
        .module('wild.under.applications.details.questions')
        .service('QuestionsService', QuestionsService);

})(angular);
