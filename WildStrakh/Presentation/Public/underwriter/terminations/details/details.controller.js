﻿(function (angular) {

    'use strict';

    function DetailsController(stateManager, routes, terminationsService, informationService, informationCorrectionService, toastr) {

        this.routes = routes;
        this.toastr = toastr;
        this.stateManager = stateManager;
        this.terminationsService = terminationsService;
        this.informationService = informationService;
        this.informationCorrectionService = informationCorrectionService;

        this.terminationId = this.stateManager.params.terminationId;

        this.activate();

    }

    DetailsController.prototype.activate = function () {
        this.termination = this.terminationsService.getDetails(this.terminationId);
        this.corrections = this.informationCorrectionService.getByTerminationId(this.terminationId);
        this.informations = this.informationService.getByTerminationId(this.terminationId);
    };

    DetailsController.prototype.requestCorrection = function () {
        return this.stateManager.go(this.routes.terminationRequest.stateName);
    };

    DetailsController.prototype.validate = function () {
        return this.terminationsService.approve(this.terminationId).$promise
                    .then(() => {
                        this.stateManager.go(this.routes.terminations.stateName, {}, { reload: true });
                this.toastr.success('Одобрено!');
            });
    };

    DetailsController.prototype.reject = function () {
        return this.terminationsService.reject(this.terminationId).$promise
                    .then(() => {
                        this.stateManager.go(this.routes.terminations.stateName, {}, { reload: true });
                        this.toastr.success('Отвергнуто!');
                    });
    };

    DetailsController.prototype.allowActions = function () {
        return this.informations.every(i => i.isClosed);
    };

    DetailsController.$inject = [
        '$state',
        'ROUTES',
        'TerminationService',
        'AdditionalInformationService',
        'InformationCorrectionService',
        'toastr'
    ];

    angular
        .module('wild.under.terminations.details')
    .controller('TerminationDetailsController', DetailsController);

})(angular);
