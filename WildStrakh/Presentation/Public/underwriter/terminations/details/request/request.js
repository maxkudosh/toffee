﻿(function (angular) {

    'use strict';

    let popup;

    function configureRoutes($stateProvider, ROUTES) {

        $stateProvider
           .state(ROUTES.terminationRequest.stateName, {
               url: ROUTES.terminationRequest.path,
               onEnter: [
                   '$uibModal', function (modal) {
                       popup = modal.open({
                           templateUrl: '/public/underwriter/terminations/details/request/request.html',
                           backdrop: 'static',
                           size: 'lg'
                       });
                   }
               ],
               onExit: function () {
                   popup.close();
               }
           });

    }

    configureRoutes.$inject = ['$stateProvider', 'ROUTES'];

    angular
        .module('wild.under.terminations.details.request', [
            'wild.services',
            'wild.controllers'
        ])
        .config(configureRoutes);

})(angular);
