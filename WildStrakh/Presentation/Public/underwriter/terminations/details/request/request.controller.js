﻿(function (angular) {

    'use strict';

    function builder(informationCorrectionService, controllerFactory)
    {
        return controllerFactory.create(request => informationCorrectionService.requestForTermination(request), params => params.terminationId);
    }

    builder.$inject = [
        'AdditionalInformationService',
        'CorrectionControllerFactory'
    ];

    angular
        .module('wild.under.terminations.details.request')
        .controller('TerminationRequestController', builder);

})(angular);
