﻿(function (angular) {

    'use strict';


    function configureRoutes($stateProvider, ROUTES) {

        $stateProvider
            .state(ROUTES.terminationDetails.stateName, {
                url: ROUTES.terminationDetails.path,
                templateUrl: '/public/underwriter/terminations/details/details.html'
            });

    }

    configureRoutes.$inject = ['$stateProvider', 'ROUTES'];

    angular
        .module('wild.under.terminations.details', [
            'toastr',
            'wild.services',
            'wild.termination-application',
            'wild.information-correction',
            'wild.additional-information',
            'wild.under.terminations.details.request'
        ])
        .config(configureRoutes);

})(angular);
