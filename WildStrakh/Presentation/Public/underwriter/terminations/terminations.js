﻿(function (angular) {

    'use strict';

    function configureRoutes($stateProvider, ROUTES) {

        $stateProvider
            .state(ROUTES.terminations.stateName, {
                url: ROUTES.terminations.path,
                templateUrl: '/public/underwriter/terminations/terminations.html'
            });

    }

    configureRoutes.$inject = ['$stateProvider', 'ROUTES'];

    angular
        .module('wild.under.terminations', [
            'ui.router',
            'wild.services',
            'wild.under.terminations.details'
        ])
    .config(configureRoutes);

})(angular);
