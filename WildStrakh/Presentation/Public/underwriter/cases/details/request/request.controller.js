﻿(function (angular) {

    'use strict';

    function builder(informationService, controllerFactory) {
        return controllerFactory.create(request => informationService.requestForCase(request), params => params.caseId);
    }

    builder.$inject = [
        'AdditionalInformationService',
        'CorrectionControllerFactory'
    ];

    angular
        .module('wild.under.cases.details.request')
        .controller('CaseRequestController', builder);

})(angular);
