﻿(function (angular) {

    'use strict';

    let popup;

    function configureRoutes($stateProvider, ROUTES) {

        $stateProvider
           .state(ROUTES.caseRequest.stateName, {
               url: ROUTES.caseRequest.path,
               onEnter: [
                   '$uibModal', function (modal) {
                       popup = modal.open({
                           templateUrl: '/public/underwriter/cases/details/request/request.html',
                           backdrop: 'static',
                           size: 'lg'
                       });
                   }
               ],
               onExit: function () {
                   popup.close();
               }
           });

    }

    configureRoutes.$inject = ['$stateProvider', 'ROUTES'];

    angular
        .module('wild.under.cases.details.request', [
            'wild.services',
            'wild.controllers'
        ])
        .config(configureRoutes);

})(angular);
