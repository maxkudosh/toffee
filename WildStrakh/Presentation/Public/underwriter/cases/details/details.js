﻿(function (angular) {

    'use strict';


    function configureRoutes($stateProvider, ROUTES) {

        $stateProvider
            .state(ROUTES.caseDetails.stateName, {
                url: ROUTES.caseDetails.path,
                templateUrl: '/public/underwriter/cases/details/details.html'
            });

    }

    configureRoutes.$inject = ['$stateProvider', 'ROUTES'];

    angular
        .module('wild.under.cases.details', [
            'toastr',
            'wild.services',
            'wild.additional-information',
            'wild.information-correction',
            'wild.insurance-case',
            'wild.under.cases.details.request',
            'wild.under.cases.details.approve'
        ])
        .config(configureRoutes);

})(angular);
