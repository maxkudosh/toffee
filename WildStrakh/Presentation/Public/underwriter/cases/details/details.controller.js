﻿(function (angular) {

    'use strict';

    function DetailsController(stateManager, routes, casesService, informationService, correctionService, toastr) {

        this.routes = routes;
        this.toastr = toastr;
        this.stateManager = stateManager;
        this.casesService = casesService;
        this.informationService = informationService;
        this.correctionService = correctionService;

        this.caseId = this.stateManager.params.caseId;

        this.activate();

    }

    DetailsController.prototype.activate = function () {
        this.case = this.casesService.getDetails(this.caseId);
        this.informations = this.informationService.getByCaseId(this.caseId);
        this.corrections = this.correctionService.getByCaseId(this.caseId);
    };

    DetailsController.prototype.requestCorrection = function () {
        return this.stateManager.go(this.routes.caseRequest.stateName);
    };

    DetailsController.prototype.validate = function ()
    {
        this.stateManager.go(this.routes.caseReview.stateName);
    };

    DetailsController.prototype.allowActions = function () {
        return this.informations.every(i => i.isClosed);
    };

    DetailsController.$inject = [
        '$state',
        'ROUTES',
        'CaseService',
        'AdditionalInformationService',
        'InformationCorrectionService',
        'toastr'
    ];

    angular
        .module('wild.under.cases.details')
    .controller('CaseDetailsController', DetailsController);

})(angular);
