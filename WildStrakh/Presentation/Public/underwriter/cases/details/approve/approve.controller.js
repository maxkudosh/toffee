﻿(function (angular) {

    'use strict';

    function ApproveCaseController(stateManager, routes, caseReviewService, toastr) {

        this.routes = routes;
        this.toastr = toastr;
        this.stateManager = stateManager;
        this.caseReviewService = caseReviewService;

        this.caseId = this.stateManager.params.caseId;


    }

    ApproveCaseController.prototype.cancel = function () {
        this.stateManager.go('^');
    };

    ApproveCaseController.prototype.approve = function ()
    {
        const approval = {
            noticeId: this.caseId,
            isApproved: true,
            payment: this.amount
        };

        return this.caseReviewService.review(approval).$promise
                    .then(() => {
                        this.stateManager.go(this.routes.cases.stateName, {}, { reload: true });
                        this.toastr.success('Одобрено!');
                    });

    };


    ApproveCaseController.prototype.reject = function () {
        const approval = {
            noticeId: this.caseId,
            isApproved: false
        };

        return this.caseReviewService.review(approval).$promise
                    .then(() => {
                        this.stateManager.go(this.routes.cases.stateName, {}, { reload: true });
                        this.toastr.success('Отвергнуто!');
                    });

    };

    ApproveCaseController.prototype.disableApproval = function () {
        return !this.amount;
    };

    ApproveCaseController.$inject = [
        '$state',
        'ROUTES',
        'CaseReviewService',
        'toastr'
    ];

    angular
        .module('wild.under.cases.details.approve')
    .controller('ApproveCaseController', ApproveCaseController);

})(angular);
