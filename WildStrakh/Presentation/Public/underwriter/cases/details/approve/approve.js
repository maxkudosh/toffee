﻿(function (angular) {

    'use strict';
    let popup;

    function configureRoutes($stateProvider, ROUTES) {

        $stateProvider
            .state(ROUTES.caseReview.stateName, {
                url: ROUTES.caseReview.path,
                onEnter: [
                  '$uibModal', function (modal) {
                      popup = modal.open({
                          templateUrl: '/public/underwriter/cases/details/approve/approve.html',
                          backdrop: 'static',
                          size: 'lg'
                      });
                  }
                ],
                onExit: function () {
                    popup.close();
                }
            });

    }

    configureRoutes.$inject = ['$stateProvider', 'ROUTES'];

    angular
        .module('wild.under.cases.details.approve', [
            'toastr',
            'wild.services'
        ])
        .config(configureRoutes);

})(angular);
