﻿(function (angular) {

    'use strict';

    function configureRoutes($stateProvider, ROUTES) {

        $stateProvider
            .state(ROUTES.cases.stateName, {
                url: ROUTES.cases.path,
                templateUrl: '/public/underwriter/cases/cases.html'
            });

    }

    configureRoutes.$inject = ['$stateProvider', 'ROUTES'];

    angular
        .module('wild.under.cases', [
            'ui.router',
            'wild.services',
            'wild.under.cases.details'
        ])
    .config(configureRoutes);

})(angular);
