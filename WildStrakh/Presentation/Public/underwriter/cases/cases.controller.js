﻿(function (angular) {

    'use strict';

    function CasesController(stateManager, routes, casesService) {
        
        this.routes = routes;
        this.stateManager = stateManager;
        this.casesService = casesService;

        this.activate();

    }

    CasesController.prototype.activate = function () {
        this.validated = this.casesService.getValidated();
        this.pending = this.casesService.getPendingApproval();
    };

    CasesController.prototype.goToDetails = function (iCase) {
        return this.stateManager.go(this.routes.caseDetails.stateName, { caseId: iCase.noticeId });
    };

    CasesController.$inject = [
        '$state',
        'ROUTES',
        'CaseService'
    ];

    angular
        .module('wild.under.cases')
    .controller('CasesController', CasesController);

})(angular);
