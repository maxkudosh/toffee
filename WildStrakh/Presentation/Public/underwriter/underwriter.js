﻿(function (angular) {

    'use strict';

    angular
    .module('wild.under', [
        'wild.under.applications',
        'wild.under.cases',
        'wild.under.terminations'
    ]);


})(angular);
