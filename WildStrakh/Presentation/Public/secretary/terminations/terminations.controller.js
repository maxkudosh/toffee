﻿(function (angular) {

    'use strict';

    function TerminationsController(stateManager, routes, terminationService) {
        
        this.routes = routes;
        this.stateManager = stateManager;
        this.terminationService = terminationService;

        this.activate();

    }

    TerminationsController.prototype.activate = function () {
        this.terminations = this.terminationService.getPendingValidation();
    };

    TerminationsController.prototype.goToDetails = function (termination) {
        return this.stateManager.go(this.routes.terminationDetails.stateName, { terminationId: termination.terminationId });
    };

    TerminationsController.$inject = [
        '$state',
        'ROUTES',
        'TerminationService'
    ];

    angular
        .module('wild.secretary.terminations')
    .controller('TerminationsController', TerminationsController);

})(angular);
