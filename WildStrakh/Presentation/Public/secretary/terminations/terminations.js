﻿(function (angular) {

    'use strict';

    function configureRoutes($stateProvider, ROUTES) {

        $stateProvider
            .state(ROUTES.terminations.stateName, {
                url: ROUTES.terminations.path,
                templateUrl: '/public/secretary/terminations/terminations.html'
            });

    }

    configureRoutes.$inject = ['$stateProvider', 'ROUTES'];

    angular
        .module('wild.secretary.terminations', [
            'ui.router',
            'wild.services',
            'wild.secretary.terminations.details'
        ])
    .config(configureRoutes);

})(angular);
