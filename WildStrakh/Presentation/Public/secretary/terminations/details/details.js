﻿(function (angular) {

    'use strict';


    function configureRoutes($stateProvider, ROUTES) {

        $stateProvider
            .state(ROUTES.terminationDetails.stateName, {
                url: ROUTES.terminationDetails.path,
                templateUrl: '/public/secretary/terminations/details/details.html'
            });

    }

    configureRoutes.$inject = ['$stateProvider', 'ROUTES'];

    angular
        .module('wild.secretary.terminations.details', [
            'toastr',
            'wild.services',
            'wild.termination-application',
            'wild.information-correction',
            'wild.secretary.terminations.details.request'
        ])
        .config(configureRoutes);

})(angular);
