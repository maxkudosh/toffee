﻿(function (angular) {

    'use strict';

    let popup;

    function configureRoutes($stateProvider, ROUTES) {

        $stateProvider
           .state(ROUTES.requestTerminationCorrection.stateName, {
               url: ROUTES.requestTerminationCorrection.path,
               onEnter: [
                   '$uibModal', function (modal) {
                       popup = modal.open({
                           templateUrl: '/public/secretary/terminations/details/request/request.html',
                           backdrop: 'static',
                           size: 'lg'
                       });
                   }
               ],
               onExit: function () {
                   popup.close();
               }
           });

    }

    configureRoutes.$inject = ['$stateProvider', 'ROUTES'];

    angular
        .module('wild.secretary.terminations.details.request', [
            'wild.services',
            'wild.controllers'
        ])
        .config(configureRoutes);

})(angular);
