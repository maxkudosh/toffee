﻿(function (angular) {

    'use strict';

    function builder(correctionService, controllerFactory) {

        return controllerFactory.create(request => correctionService.requestForTermination(request), params => params.terminationId);

    }

    builder.$inject = [
        'InformationCorrectionService',
        'CorrectionControllerFactory'
    ];

    angular
        .module('wild.secretary.terminations.details.request')
        .controller('TerminationRequestController', builder);

})(angular);
