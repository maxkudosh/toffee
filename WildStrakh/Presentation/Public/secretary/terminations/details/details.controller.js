﻿(function (angular) {

    'use strict';

    function TerminationDetailsController(stateManager, routes, terminationsService, informationCorrectionService, toastr) {

        this.routes = routes;
        this.toastr = toastr;
        this.stateManager = stateManager;
        this.terminationsService = terminationsService;
        this.informationCorrectionService = informationCorrectionService;

        this.terminationId = this.stateManager.params.terminationId;

        this.activate();

    }

    TerminationDetailsController.prototype.activate = function () {
        this.termination = this.terminationsService.getDetails(this.terminationId);
        this.corrections = this.informationCorrectionService.getByTerminationId(this.terminationId);
    };

    TerminationDetailsController.prototype.requestCorrection = function () {
        return this.stateManager.go(this.routes.requestTerminationCorrection.stateName);
    };

    TerminationDetailsController.prototype.allowActions = function () {
        return this.corrections.every(c => c.isClosed);
    };

    TerminationDetailsController.prototype.validate = function () {
        return this.terminationsService.validate(this.terminationId).$promise.then(() =>
        {
            this.toastr.success('Одобрено!');
            this.stateManager.go(this.routes.terminations.stateName, {}, { reload: true });
        });
    };

    TerminationDetailsController.$inject = [
        '$state',
        'ROUTES',
        'TerminationService',
        'InformationCorrectionService',
        'toastr'
    ];

    angular
        .module('wild.secretary.terminations.details')
        .controller('TerminationDetailsController', TerminationDetailsController);

})(angular);
