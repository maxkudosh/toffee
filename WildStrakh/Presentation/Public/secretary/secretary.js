﻿(function (angular) {

    'use strict';

    angular
        .module('wild.secretary', [
            'wild.secretary.applications',
            'wild.secretary.cases',
            'wild.secretary.terminations'
        ]);

})(angular);
