﻿(function (angular) {

    'use strict';

    function configureRoutes($stateProvider, ROUTES) {

        $stateProvider
            .state(ROUTES.applications.stateName, {
                url: ROUTES.applications.path,
                templateUrl: '/public/secretary/applications/applications.html'
            });

    }

    configureRoutes.$inject = ['$stateProvider', 'ROUTES'];

    angular
        .module('wild.secretary.applications', [
            'ui.router',
            'wild.services',
            'wild.secretary.applications.details'
        ])
    .config(configureRoutes);

})(angular);
