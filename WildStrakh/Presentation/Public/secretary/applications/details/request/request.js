﻿(function (angular) {

    'use strict';

    let popup;

    function configureRoutes($stateProvider, ROUTES) {

        $stateProvider
           .state(ROUTES.requestApplicationCorrection.stateName, {
               url: ROUTES.requestApplicationCorrection.path,
               onEnter: [
                   '$uibModal', function (modal) {
                       popup = modal.open({
                           templateUrl: '/public/secretary/applications/details/request/request.html',
                           backdrop: 'static',
                           size: 'lg'
                       });
                   }
               ],
               onExit: function () {
                   popup.close();
               }
           });

    }

    configureRoutes.$inject = ['$stateProvider', 'ROUTES'];

    angular
        .module('wild.secretary.applications.details.request', [
            'wild.services',
            'wild.controllers'
        ])
    .config(configureRoutes);

})(angular);
