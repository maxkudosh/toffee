﻿(function (angular) {

    'use strict';

    function builder(correctionService, controllerFactory) {

        return controllerFactory.create(request => correctionService.requestForApplication(request), params => params.applicationId);

    }

    builder.$inject = [
        'InformationCorrectionService',
        'CorrectionControllerFactory'
    ];

    angular
        .module('wild.secretary.applications.details.request')
        .controller('ApplicationRequestController', builder);

})(angular);
