﻿(function (angular) {

    'use strict';

    function DetailsController(stateManager, routes, applicationService, informationCorrectionService, toastr) {

        this.routes = routes;
        this.stateManager = stateManager;
        this.applicationService = applicationService;
        this.informationCorrectionService = informationCorrectionService;
        this.toastr = toastr;

        this.applicationId = this.stateManager.params.applicationId;

        this.activate();

    }

    DetailsController.prototype.activate = function () {
        this.application = this.applicationService.getDetails(this.applicationId);
        this.corrections = this.informationCorrectionService.getByApplicationId(this.applicationId);
    };

    DetailsController.prototype.requestCorrection = function () {
        return this.stateManager.go(this.routes.requestApplicationCorrection.stateName);
    };

    DetailsController.prototype.validate = function () {
        return this.applicationService
            .validate(this.applicationId).$promise
            .then(() => {
                this.toastr.success('Заявления одобрено!');
                this.stateManager.go(this.routes.applications.stateName, { reload: true });
            });
    };

    DetailsController.prototype.allowActions = function () {
        return this.corrections.every(c => c.isClosed);
    };

    DetailsController.$inject = [
        '$state',
        'ROUTES',
        'ApplicationService',
        'InformationCorrectionService',
        'toastr'
    ];

    angular
        .module('wild.secretary.applications.details')
    .controller('AppDetailsController', DetailsController);

})(angular);
