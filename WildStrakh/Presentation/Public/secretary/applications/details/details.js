﻿(function (angular) {

    'use strict';


    function configureRoutes($stateProvider, ROUTES) {

        $stateProvider
            .state(ROUTES.details.stateName, {
                url: ROUTES.details.path,
                templateUrl: '/public/secretary/applications/details/details.html'
            });

    }

    configureRoutes.$inject = ['$stateProvider', 'ROUTES'];

    angular
        .module('wild.secretary.applications.details', [
            'toastr',
            'wild.services',
            'wild.application',
            'wild.secretary.applications.details.request'
        ])
        .config(configureRoutes);

})(angular);
