﻿(function (angular) {

    'use strict';

    function ApplicationsController(stateManager, routes, applicationService) {
        
        this.routes = routes;
        this.stateManager = stateManager;
        this.applicationService = applicationService;

        this.activate();

    }

    ApplicationsController.prototype.activate = function() {
        this.applications = this.applicationService.getPendingValidation();
    };

    ApplicationsController.prototype.goToDetails = function (application) {
        return this.stateManager.go(this.routes.details.stateName, { applicationId: application.id });
    };

    ApplicationsController.$inject = [
        '$state',
        'ROUTES',
        'ApplicationService'
    ];

    angular
        .module('wild.secretary.applications')
    .controller('ApplicationsController', ApplicationsController);

})(angular);
