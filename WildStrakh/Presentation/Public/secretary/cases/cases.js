﻿(function (angular) {

    'use strict';

    function configureRoutes($stateProvider, ROUTES) {

        $stateProvider
            .state(ROUTES.cases.stateName, {
                url: ROUTES.cases.path,
                templateUrl: '/public/secretary/cases/cases.html'
            });

    }

    configureRoutes.$inject = ['$stateProvider', 'ROUTES'];

    angular
        .module('wild.secretary.cases', [
            'ui.router',
            'wild.services',
            'wild.secretary.cases.details'
        ])
    .config(configureRoutes);

})(angular);
