﻿(function (angular) {

    'use strict';

    function CasesController(stateManager, routes, casesService) {
        
        this.routes = routes;
        this.stateManager = stateManager;
        this.casesService = casesService;

        this.activate();

    }

    CasesController.prototype.activate = function() {
        this.cases = this.casesService.getPendingValidation();
    };

    CasesController.prototype.goToDetails = function (iCase) {
        return this.stateManager.go(this.routes.caseDetails.stateName, { caseId: iCase.noticeId });
    };

    CasesController.$inject = [
        '$state',
        'ROUTES',
        'CaseService'
    ];

    angular
        .module('wild.secretary.cases')
    .controller('CasesController', CasesController);

})(angular);
