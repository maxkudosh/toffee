﻿(function (angular) {

    'use strict';

    function builder(correctionService, controllerFactory) {

        return controllerFactory.create(request => correctionService.requestForCase(request), params => params.caseId);

    }

    builder.$inject = [
        'InformationCorrectionService',
        'CorrectionControllerFactory'
    ];

    angular
        .module('wild.secretary.cases.details.request')
        .controller('CaseRequestController', builder);

})(angular);
