﻿(function (angular) {

    'use strict';

    function CaseDetailsController(stateManager, routes, casesService, informationCorrectionService, toastr) {

        this.routes = routes;
        this.stateManager = stateManager;
        this.casesService = casesService;
        this.informationCorrectionService = informationCorrectionService;
        this.toastr = toastr;

        this.caseId = this.stateManager.params.caseId;

        this.activate();

    }

    CaseDetailsController.prototype.activate = function () {
        this.case = this.casesService.getDetails(this.caseId);
        this.corrections = this.informationCorrectionService.getByCaseId(this.caseId);
    };

    CaseDetailsController.prototype.requestCorrection = function () {
        return this.stateManager.go(this.routes.requestCaseCorrection.stateName);
    };

    CaseDetailsController.prototype.validate = function () {
        return this.casesService.validate(this.caseId).$promise
            .then(() => {
                this.toastr.success('Заявления одобрено!');
                this.stateManager.go(this.routes.cases.stateName, { reload: true });
            });
    };

    CaseDetailsController.prototype.allowActions = function () {
        return this.corrections.every(c => c.isClosed);
    };

    CaseDetailsController.$inject = [
        '$state',
        'ROUTES',
        'CaseService',
        'InformationCorrectionService',
        'toastr'
    ];

    angular
        .module('wild.secretary.cases.details')
    .controller('CaseDetailsController', CaseDetailsController);

})(angular);
