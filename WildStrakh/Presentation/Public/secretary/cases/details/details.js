﻿(function (angular) {

    'use strict';


    function configureRoutes($stateProvider, ROUTES) {

        $stateProvider
            .state(ROUTES.caseDetails.stateName, {
                url: ROUTES.caseDetails.path,
                templateUrl: '/public/secretary/cases/details/details.html'
            });

    }

    configureRoutes.$inject = ['$stateProvider', 'ROUTES'];

    angular
        .module('wild.secretary.cases.details', [
            'wild.services',
            'toastr',
            'wild.information-correction',
            'wild.insurance-case',
            'wild.secretary.cases.details.request'
        ])
        .config(configureRoutes);

})(angular);
