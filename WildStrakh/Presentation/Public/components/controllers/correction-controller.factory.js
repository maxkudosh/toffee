﻿(function (angular) {

    'use strict';

    function CorrectionController(request, stateManager, toastr, id) {

        this.toastr = toastr;
        this.stateManager = stateManager;
        this.requestMethod = request;
        this.id = id(stateManager.params);

    }

    CorrectionController.prototype.cancel = function () {
        return this.stateManager.go('^');
    };

    CorrectionController.prototype.submit = function () {

        const correctionRequest = {
            content: this.request,
            documentId: this.id
        };

        this.requestMethod(correctionRequest).$promise
            .then(() => {
                this.stateManager.go('^', {}, { reload: true });
                this.toastr.success('Запрос отправлен!');
            });

    };

    function CorrectionControllerFactory(stateManager, toastr) {
        return {
            create: function (request, id) {
                return new CorrectionController(request, stateManager, toastr, id);
            }
        }
    }

    CorrectionControllerFactory.$inject = [
        '$state',
        'toastr'
    ];

    angular
        .module('wild.controllers')
        .factory('CorrectionControllerFactory', CorrectionControllerFactory);

})(angular);
