﻿(function (angular) {

    'use strict';
    angular
        .module('wild.controllers', [
            'toastr',
            'wild.services'
        ]);
})(angular);
