﻿(function (angular) {

    'use strict';

    angular
        .module('wild.attachment', [
            'ngResource',
            'ngFileUpload'
        ]);

})(angular);
