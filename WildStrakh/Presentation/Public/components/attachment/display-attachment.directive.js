﻿(function (angular) {

    'use strict';

    function displayAttachments() {

        return {
            restrict: 'E',
            templateUrl: '/public/components/attachment/display-attachment.html',
            scope: {
                attachments: '='
            }
        };

    }

    angular
        .module('wild.attachment')
        .directive('displayAttachments', displayAttachments);

})(angular);
