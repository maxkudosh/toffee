﻿(function (angular, undefined) {

    'use strict';

    function ResourceWithAttachmentsService(attachmentsPropertyName, url, apiWrapper) {

        const actionConfig = {

            save: {
                method: 'POST',
                transformRequest: function (data) {

                    let fd = new FormData();

                    angular.forEach(data, function (value, key) {

                        if (key === attachmentsPropertyName) {

                            angular.forEach(value, function (file, index) {
                                const name = key + '[' + index + '].';
                                if (file.description) {
                                    fd.append(name + 'description', file.description);
                                }
                                fd.append(name + 'file', file);
                            });

                        } else {
                            fd.append(key, value);
                        }
                    });

                    return fd;
                },
                headers: {
                    'Content-Type': undefined,
                    'enctype': 'multipart/form-data'
                }
            }
        }

        this.api = apiWrapper(url, {}, actionConfig);

    }

    angular
        .module('wild.attachment')
        .service('ResourceWithAttachmentsService', () => ResourceWithAttachmentsService);

})(angular, undefined);
