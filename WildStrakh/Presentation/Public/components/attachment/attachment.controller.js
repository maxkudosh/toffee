﻿(function (angular) {

    'use strict';

    function AttachmentController(scope) {

        scope.addAttachment = function(files) {

            if (files) {
                scope.attachments = scope.attachments.concat(files);
            }

        };

        scope.removeAttachment = function (file) {

            scope.attachments = scope.attachments.filter(f => f !== file);

        };

    }

    AttachmentController.$inject = ['$scope'];

    angular
        .module('wild.attachment')
        .controller('AttachmentController', AttachmentController);

})(angular);
