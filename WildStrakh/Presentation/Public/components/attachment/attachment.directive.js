﻿(function (angular) {

    'use strict';

    function attachments() {

        return {
            restrict: 'E',
            templateUrl: '/public/components/attachment/attachment.html',
            controller: 'AttachmentController',
            scope: {
                attachments: '='
            }
        };

    }

    angular
        .module('wild.attachment')
        .directive('attachments', attachments);

})(angular);
