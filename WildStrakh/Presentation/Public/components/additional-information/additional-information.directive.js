﻿(function (angular) {

    'use strict';

    function hasResponse(information) {
        return information.response;
    }

    function hasNoResponse(information) {
        return !hasResponse(information);
    }

    function additionalInformation() {

        return {

            restrict: 'E',
            templateUrl: '/public/components/additional-information/additional-information.html',
            scope: {
                informations: '='
            },
            link: scope => {
                scope.hasResponse = hasResponse;
                scope.hasNoResponse = hasNoResponse;
            }

        };

    }

    angular
        .module('wild.additional-information')
        .directive('additionalInformation', additionalInformation);

})(angular);