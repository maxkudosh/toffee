﻿(function (angular) {

    'use strict';

    angular.module('wild.additional-information', ['wild.attachment']);

})(angular);