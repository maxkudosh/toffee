﻿(function (angular) {

    'use strict';

    function UserInfo(cookies) {

        this.name = cookies.get('username');
        this.role = cookies.get('userrole');

    }

    UserInfo.$inject = ['$cookies'];

    angular
        .module('wild.userinfo', ['ngCookies'])
        .service('UserInfo', UserInfo);

})(angular);
