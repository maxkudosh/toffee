﻿(function (angular) {

    'use strict';

    const UserRoles = {
        CLIENT: 'Client',
        SECRETARY: 'Secretary',
        UNDERWRITER: 'Underwriter'
    };

    function buildRoute(stateName, name, link) {

        return {
            stateName: stateName,
            name: name,
            path: link
        };

    }

    function buildNavbarRoute(stateName, name, link) {

        let route = buildRoute(stateName, name, link);

        route.navigatable = true;

        return route;

    }

    function config($provide, userInfoProvider) {

        const userRole = userInfoProvider.$get().role;

        if (userRole === UserRoles.CLIENT) {
            $provide.constant('ROUTES', createClientRoutes());
        } else if (userRole === UserRoles.SECRETARY) {
            $provide.constant('ROUTES', createSecretaryRotues());
        } else if (userRole === UserRoles.UNDERWRITER) {
            $provide.constant('ROUTES', createUnderWriterRoutes());
        }

    }


    function createClientRoutes() {

        let routes = {
            home: buildRoute('app', 'Дом', '/'),

            apply: buildNavbarRoute('app.apply', 'Подать заявление', 'apply/'),

            notify: buildNavbarRoute('app.notify', 'Что то случилось', 'notify/'),

            myContracts: buildNavbarRoute('app.myContracts', 'Мои договоры', 'contracts/'),
            acceptContract: buildRoute('app.myContracts.accept', 'Расторгнуть', ':contractId/accept/'),
            cancelContract: buildRoute('app.myContracts.cancel', 'Расторгнуть', ':contractId/cancel/'),
            notifyContractNested: buildRoute('app.myContracts.notify', 'Известить о происшествии', ':contractId/notify/'),

            requests: buildNavbarRoute('app.requests', 'Уточнения', 'requests/'),
            satisfyRequest: buildRoute('app.requests.satisfy', 'Ответить на запрос', ':requestId/satisfy/'),
            viewRequest: buildRoute('app.requests.view', 'Просмотреть ответ на запрос', ':requestId/view/'),

            corrections: buildNavbarRoute('app.corrections', 'Коррекции', 'corrections/'),
            correct: buildRoute('app.corrections.correct', 'Просмотреть ответ на запрос', ':id/'),

            applications: buildNavbarRoute('app.applications', 'Мои заявления', 'applications/'),
            application: buildRoute('app.application', 'Мои заявления', 'application/:applicationId')
        };

        routes.getDefault =  () => routes.myContracts;

        return routes;
    }

    function createSecretaryRotues() {

        let routes = {
            home: buildRoute('secretary', 'Дом', '/'),

            applications: buildNavbarRoute('secretary.applications', 'Заявления', 'applications/'),
            details: buildRoute('secretary.application', 'Подробно о заявлении', 'application/:applicationId/'),
            requestApplicationCorrection: buildRoute('secretary.application.request', '', 'request/'),

            cases: buildNavbarRoute('secretary.cases', 'Уведомления', 'cases/'),
            caseDetails: buildRoute('secretary.case', 'Подробно об уведомлении', 'case/:caseId/'),
            requestCaseCorrection: buildRoute('secretary.case.request', '', 'request/'),

            terminations: buildNavbarRoute('secretary.terminations', 'Расторжения', 'terminations/'),
            terminationDetails: buildRoute('secretary.termination', 'Подробно о расторжении', 'termination/:terminationId/'),
            requestTerminationCorrection: buildRoute('secretary.termination.request', '', 'request/')
        };

        routes.getDefault = () => routes.applications;

        return routes;
    }

    function createUnderWriterRoutes() {

        let routes = {
            home: buildRoute('under', 'Дом', '/'),

            applications: buildNavbarRoute('under.applications', 'Заявления', 'applications/'),
            details: buildRoute('under.application', 'Подробно о заявлении', 'application/:applicationId/'),
            questions: buildRoute('under.application.questions', 'Подробно о заявлении', 'questions/'),
            approve: buildRoute('under.application.approve', 'Подробно о заявлении', 'approve/'),
            request: buildRoute('under.application.request', 'Подробно о заявлении', 'request/'),

            cases: buildNavbarRoute('under.cases', 'Уведомления', 'cases/'),
            caseDetails: buildRoute('under.case', 'Подробно об уведомлении', 'case/:caseId/'),
            caseRequest: buildRoute('under.case.request', 'Подробно о расторжении', 'request/'),
            caseReview: buildRoute('under.case.review', 'Подробно о расторжении', 'review/'),

            terminations: buildNavbarRoute('under.terminations', 'Расторжения', 'terminations/'),
            terminationDetails: buildRoute('under.termination', 'Подробно о расторжении', 'termination/:terminationId/'),
            terminationRequest: buildRoute('under.termination.request', 'Подробно о расторжении', 'request/')
        };

        routes.getDefault = () => routes.applications;

        return routes;

    }

    config.$inject = ['$provide', 'UserInfoProvider'];

    angular
        .module('wild.routes', ['wild.userinfo'])
        .config(config);

})(angular);
