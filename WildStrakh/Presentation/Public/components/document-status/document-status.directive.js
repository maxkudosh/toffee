﻿(function (angular) {

    'use strict';

    function documentStatus(DocumentStatus) {

        const statusClasses = {
    
            [DocumentStatus.PENDING_VALIDATION.id]: 'label-info',
            [DocumentStatus.VALIDATED.id]: 'label-info',
            [DocumentStatus.PENDING_CORRECTION.id]: 'label-warning',
            [DocumentStatus.PROCESSED.id]: 'label-info',
            [DocumentStatus.PENDING_APPROVAL.id]: 'label-info',
            [DocumentStatus.PENDING_ADDITIONAL_INFORMATION.id]: 'label-warning',
            [DocumentStatus.APPROVED.id]: 'label-success',
            [DocumentStatus.REJECTED.id]: 'label-danger',
            [DocumentStatus.CLOSED.id]: 'label-default'

        };

        return {
        
            restrict: 'E',
            templateUrl: '/public/components/document-status/document-status.html',
            scope: {
                status: '@'
            },
            link: function(scope) {

                scope.statusClass = statusClasses[scope.status];
                scope.statusName = Object
                    .keys(DocumentStatus)
                    .map(key => DocumentStatus[key])
                    .find(value => value.id === +scope.status).name;

            }

        };

    }

    documentStatus.$inject = ['DocumentStatus'];

    angular
        .module('wild.document-status', ['wild.enums'])
        .directive('documentStatus', documentStatus);

})(angular);
