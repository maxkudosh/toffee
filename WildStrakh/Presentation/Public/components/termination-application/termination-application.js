﻿(function (angular) {

    'use strict';

    angular.module('wild.termination-application', ['wild.person']);

})(angular);