﻿(function (angular) {

    'use strict';

    function terminationApplication() {

        return {
        
            restrict: 'E',
            templateUrl: '/public/components/termination-application/termination-application.html',
            scope: {
                termination: '='
            }

        };

    }

    angular
        .module('wild.termination-application')
        .directive('terminationApplication', terminationApplication);

})(angular);