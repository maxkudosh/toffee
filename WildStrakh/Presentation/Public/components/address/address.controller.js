﻿(function (angular) {

    'use strict';

    function AddressController(citiesService) {

        this.citiesService = citiesService;

    }

    AddressController.prototype.getCities = function (nameStart) {

        return this.citiesService.getWhereNameStartsWith(nameStart).$promise;

    };

    AddressController.$inject = ['CitiesService'];

    angular
        .module('wild.address')
        .controller('AddressController', AddressController);

})(angular);
