﻿(function (angular) {

    'use strict';

    const cityApiUri = '/api/city/';

    function CitiesService(apiWrapper)
    {
        this.api = apiWrapper(cityApiUri);
    }

    CitiesService.prototype.getWhereNameStartsWith = function(name) {

        let result = [];

        result.$promise = this.api.query({ nameStart: name }).$promise.then(cities => {

            return cities.map(formatCity);

        });

        return result;
    };

    function formatCity(city) {
        
        var cityToDisplay = city.name;

        if (city.region && city.region !== '') {
            cityToDisplay += `, ${city.region} область`;
        }

        if (city.district && city.district !== '') {
            cityToDisplay += `, ${city.district} район`;
        }

        city.displayName = cityToDisplay;

        return city;

    }

    CitiesService.$inject = ['$resource'];

    angular
        .module('wild.address')
        .service('CitiesService', CitiesService);

})(angular);
