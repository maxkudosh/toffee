﻿(function (angular) {

    'use strict';

    angular
        .module('wild.address', [
            'ui.bootstrap',
            'ngResource',
            'wild.services'
        ]);

})(angular);
