﻿(function (angular) {

    'use strict';

    angular
        .module('wild.address')
        .directive('customerAddress', function () {

            return {

                restrict: 'E',
                templateUrl: '/public/components/address/address.html',
                scope: {
                    address: '='
                }

            };

        });

})(angular);
