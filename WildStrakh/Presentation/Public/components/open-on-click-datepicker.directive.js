﻿(function(angular)
{
    'use strict';
    angular
        .module('wild.datepicker', ['ui.bootstrap'])
        .directive('openOnClick', openDatepickerOnClick)
        .directive('dateValidator', dateValidator);

    function openDatepickerOnClick($parse)
    {
        function openDatepicker(datepickerScope)
        {
            datepickerScope.$apply(() => $parse('isOpen').assign(datepickerScope, 'true'));
        }
        return {
            restrict: 'A',
            link: (scope, datepicker) =>
            {
                datepicker.on('click', () => openDatepicker(datepicker.isolateScope()));
            }
        };
    }
    openDatepickerOnClick.$inject = ['$parse'];

    function dateValidator() {
        return {
            require: 'ngModel',
            restrict: 'A',
            link: function(scope, elem, attr, ngModel)
            {
                var dateParser = ngModel.$parsers[0];
                ngModel.$parsers[0] = (function (inputValue) {
                    if (inputValue == undefined) return '';
                    var transformedInput = inputValue.replace(/[^0-9\-]/g, '');
                    if (transformedInput !== inputValue) {
                        ngModel.$setViewValue(transformedInput);
                        ngModel.$render();
                    }
                    return transformedInput;
                });
                ngModel.$parsers.push(dateParser);
                function validate(value)
                {
                    if (value !== undefined && value != null)
                    {
                        ngModel.$setValidity('badDate', true);
                        if (value instanceof Date)
                        {
                            let d = Date.parse(value);
                            if (isNaN(d))
                            {
                                ngModel.$setValidity('badDate', false);
                            }
                        }
                        else
                        {
                            var myPattern = new RegExp(/^([0-9]{4})\-([0-9]{2})\-([0-9]{2})$/);
                            if (value != '' && !myPattern.test(value))
                            {
                                ngModel.$setValidity('badDate', false);
                            } else {
                                let d = Date.parse(value);
                                if (isNaN(d)) {
                                    ngModel.$setValidity('badDate', false);
                                }
                            }
                        }
                    }
                }
                scope.$watch(function()
                {
                    return ngModel.$viewValue;
                }, validate);
            }
        };
    };

})(angular);