﻿(function (angular) {

    'use strict';

    angular
        .module('wild.navbar', [
            'ui.bootstrap',
            'ui.router'
        ]);

})(angular);
