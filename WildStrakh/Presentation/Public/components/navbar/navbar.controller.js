﻿(function (angular) {

    'use strict';

    function NavbarController(stateManager, ROUTES, userInfo) {

        this.stateManager = stateManager;
        this.username = userInfo.name;

        this.menu = Object
            .keys(ROUTES)
            .map(key => ROUTES[key])
            .filter(route => route.navigatable);

    }

    NavbarController.prototype.isStateActive = function(name) {

        return this.stateManager.current.name.includes(name);

    };

    NavbarController.$inject = ['$state', 'ROUTES', 'UserInfo'];

    angular
        .module('wild.navbar')
        .controller('NavbarController', NavbarController);

})(angular);
