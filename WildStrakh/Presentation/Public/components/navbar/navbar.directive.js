﻿(function (angular) {

    'use strict';

    angular
        .module('wild.navbar')
        .directive('navigation', function () {

        return {

            restrict: 'E',
            templateUrl: '/public/components/navbar/navbar.html'

        };

    });

})(angular);
