﻿(function (angular) {

    'use strict';

    function personDirective() {

        return {
            restrict: 'E',
            scope: {
                person: '='
            },
            templateUrl: '/public/components/person/person.html'
        };

    }

    angular
        .module('wild.person')
        .directive('person', personDirective);

})(angular);
