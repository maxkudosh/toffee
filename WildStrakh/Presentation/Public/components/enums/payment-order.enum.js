﻿(function (angular) {

    'use strict';

    const PaymentOrder = {
        ONCE: {
            id: 0,
            name: 'Один раз'
        },
        CHRONIC: {
            id: 1,
            name: 'Ежемесячно'
        },
        PAST: {
            id: 2,
            name: 'Ежегодно'
        }
    };

    angular
        .module('wild.enums')
        .constant('PaymentOrder', PaymentOrder);

})(angular);
