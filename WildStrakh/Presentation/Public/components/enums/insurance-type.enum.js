﻿(function (angular) {

    'use strict';

    const InsuranceType = {
        LIFE: {
            id: 0,
            name: 'Страхование жизни'
        },
        HEALTH: {
            id: 1,
            name: 'Страхование здоровья'
        }
    };

    angular
        .module('wild.enums')
        .constant('InsuranceType', InsuranceType);

})(angular);
