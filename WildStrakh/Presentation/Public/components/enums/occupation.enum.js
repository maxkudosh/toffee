﻿(function (angular) {

    'use strict';

    const Occupation = {
        AdministrativeJudicialOfficers: {
            id: 0,
            name: 'Административно-судебный служащий'
        },
        Artist: {
            id: 1,
            name: 'Художник, артист'
        },
        Architect: {
            id: 2,
            name: 'Архитектор'
        },
        Banker: {
            id: 3,
            name: 'Банкир'
        },
        Unemployed: {
            id: 4,
            name: 'Безработный'
        },
        Driver: {
            id: 5,
            name: 'Водитель'
        },
        Official: {
            id: 6,
            name: 'Государственный служащий'
        },
        Diplomat: {
            id: 7,
            name: 'Дипломат'
        },
        Clergyman: {
            id: 8,
            name: 'Духовное лицо'
        },
        Journalist: {
            id: 9,
            name: 'Журналист'
        },
        Informatic: {
            id: 10,
            name: 'Информатик'
        },
        Other: {
            id: 11,
            name: 'Иное'
        },
        TradeMarketing: {
            id: 12,
            name: 'Сфера торговли, маркетинг'
        },
        Medic: {
            id: 13,
            name: 'Медицинские профессии'
        },
        Manager: {
            id: 14,
            name: 'Менеджер'
        },
        Beauty: {
            id: 15,
            name: 'Сфера красоты'
        },
        Sailor: {
            id: 16,
            name: 'Моряк'
        },
        Researcher: {
            id: 17,
            name: 'Научный исследователь'
        },
        Pensioner: {
            id: 18,
            name: 'Пенсионер'
        },
        Politician: {
            id: 19,
            name: 'Политик'
        },
        Police: {
            id: 20,
            name: 'Полицейский'
        },
        Soldier: {
            id: 21,
            name: 'Военнослужащий'
        },
        Entrepreneur: {
            id: 22,
            name: 'Предприниматель'
        },
        Craftsman: {
            id: 23,
            name: 'Ремесленник'
        },
        Athlete: {
            id: 24,
            name: 'Спортсмен, тренер'
        },
        Student: {
            id: 25,
            name: 'Студент, практикант, стажер'
        },
        Technician: {
            id: 26,
            name: 'Техник иной'
        },
        MentalWorker: {
            id: 27,
            name: 'Работник умственного труда'
        },
        Teacher: {
            id: 28,
            name: 'Учитель, преподаватель'
        },
        Farmer: {
            id: 29,
            name: 'Фермер'
        },
        PhysicalWorker: {
            id: 30,
            name: 'Работник физического труда'
        },
        Chemist: {
            id: 31,
            name: 'Химик'
        },
        Electrician: {
            id: 32,
            name: 'Электрик'
        },
        Lawyer: {
            id: 33,
            name: 'Юрист'
        }
    };

    angular
        .module('wild.enums')
        .constant('Occupation', Occupation);

})(angular);
