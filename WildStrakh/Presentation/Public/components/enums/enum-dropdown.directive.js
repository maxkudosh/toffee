﻿(function (angular) {

    'use strict';

    function enumDropdown(converter) {

        return {

            restrict: 'E',
            templateUrl: '/public/components/enums/enum-dropdown.html',
            scope: {
                value: '=',
                enum: '='
            },
            link: function(scope) {
                scope.enumAsArray = converter.toArray(scope.enum);
            }

        };

    }

    enumDropdown.$inject = ['EnumConverter'];

    angular
        .module('wild.enums')
        .directive('enumDropdown', enumDropdown);

})(angular);
