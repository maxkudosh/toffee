﻿(function (angular) {

    'use strict';

    function EnumConverter() {
    }

    EnumConverter.prototype.toArray = function(e) {

        return Object
            .keys(e)
            .map(key => e[key]);

    };

    EnumConverter.prototype.fromId = function (id, e) {

        return this.toArray(e).find(v => v.id === id);

    };

    angular
        .module('wild.enums')
        .service('EnumConverter', EnumConverter);

})(angular);
