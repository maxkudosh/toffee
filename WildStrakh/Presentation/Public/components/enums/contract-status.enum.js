﻿(function (angular) {

    'use strict';

    const ContractStatus = {
        CREATED: {
            id: 0,
            name: 'Ждёт одобрения'
        },
        PENDING_PAYMENT: {
            id: 1,
            name: 'Нужно оплатить'
        },
        CANCELLED: {
            id: 2,
            name: 'Отменён'
        },
        ACTIVE: {
            id: 3,
            name: 'Активен'
        },
        TERMINATED: {
            id: 4,
            name: 'Расторгнут'
        }
    };

    angular
        .module('wild.enums')
        .constant('ContractStatus', ContractStatus);

})(angular);
