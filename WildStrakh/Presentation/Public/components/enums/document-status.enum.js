﻿(function (angular) {

    'use strict';

    const DocumentStatus = {

        PENDING_VALIDATION: {
            id: 0,
            name: 'Ждёт проверки'
        },
        VALIDATED: {
            id: 1,
            name: 'Проверен'
        },
        PENDING_CORRECTION: {
            id: 2,
            name: 'Ждёт исправлений'
        },
        PROCESSED: {
            id: 3,
            name: 'Готов к рассмотрению'
        },
        PENDING_APPROVAL: {
            id: 4,
            name: 'Ждёт одобрения'
        },
        PENDING_ADDITIONAL_INFORMATION: {
            id: 5,
            name: 'Ждёт дополнительной информации'
        },
        APPROVED: {
            id: 6,
            name: 'Одобрен'
        },
        REJECTED: {
            id: 7,
            name: 'Отменён'
        },
        CLOSED: {
            id: 8,
            name: 'Закрыт'
        }
    };

    angular
        .module('wild.enums')
        .constant('DocumentStatus', DocumentStatus);

})(angular);
