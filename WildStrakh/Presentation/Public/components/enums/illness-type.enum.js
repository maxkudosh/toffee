﻿(function (angular) {

    'use strict';

    const IllnessType = {
        INHERITED: {
            id: 0,
            name: 'Наследственные'
        },
        CHRONIC: {
            id: 1,
            name: 'Хронические'
        },
        PAST: {
            id: 2,
            name: 'Перенесённые'
        }
    };

    angular
        .module('wild.enums')
        .constant('IllnessType', IllnessType);

})(angular);
