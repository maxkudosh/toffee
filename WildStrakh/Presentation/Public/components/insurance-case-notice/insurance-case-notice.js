﻿(function (angular) {

    'use strict';

    angular.module('wild.insurance-case', ['wild.person']);

})(angular);