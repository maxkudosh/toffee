﻿(function (angular) {

    'use strict';

    function insuranceCaseNotice() {

        return {
        
            restrict: 'E',
            templateUrl: '/public/components/insurance-case-notice/insurance-case-notice.html',
            scope: {
                case: '='
            }

        };

    }

    angular
        .module('wild.insurance-case')
        .directive('insuranceCaseNotice', insuranceCaseNotice);

})(angular);