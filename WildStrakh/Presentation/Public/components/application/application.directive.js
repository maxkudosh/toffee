﻿(function (angular) {

    'use strict';

    function formatCity(city) {
        if (!city) {
            return '';
        }
        return Object.keys(city).map(p => city[p]).join(',');
    }

    function applicationDirective() {

        return {
            restrict: 'E',
            scope: {
                application: '='
            },
            templateUrl: '/public/components/application/application.html',
            link: scope => scope.formatCity = formatCity
        };

    }

    angular
        .module('wild.application')
        .directive('application', applicationDirective);

})(angular);
