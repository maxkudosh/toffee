﻿(function(angular) {

    'use strict';

    angular.module('wild.application', ['wild.person']);

})(angular);