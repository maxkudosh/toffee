﻿(function (angular) {

    'use strict';

    function hasResponse(correction) {
        return correction.response;
    }

    function hasNoResponse(correction) {
        return !hasResponse(correction);
    }

    function informationCorrection() {

        return {

            restrict: 'E',
            templateUrl: '/public/components/information-correction/information-correction.html',
            scope: {
                corrections: '='
            },
            link: scope => {
                scope.hasResponse = hasResponse;
                scope.hasNoResponse = hasNoResponse;
            }

        };

    }

    angular
        .module('wild.information-correction')
        .directive('informationCorrection', informationCorrection);

})(angular);