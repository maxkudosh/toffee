﻿(function (angular) {

    'use strict';

    const localization = {
        selectAll: "Выбрать все",
        selectNone: "Развыбрать",
        reset: "Отменить",
        search: "Для поиска писать сюда",
        nothingSelected: "Ничего",
        new: 'Введите что-нибудь',
        save: 'Нажмите, чтобы сохранить'
    };

    function IllnessController(IllnessType, enumConverter, illnessService, scope) {

        this.illnessTypes = enumConverter.toArray(IllnessType);
        this.illnessService = illnessService;
        this.scope = scope;

        this.activate();

    }

    IllnessController.prototype.activate = function() {

        this.scope.illnesses = {};

        this.illnessTypes.forEach(type => {

            this.scope.illnesses[type.id] = [];
            this.scope.customerIllnesses[type.id] = [];

        });

        this.localization = localization;

    };

    IllnessController.prototype.onSearch = function (typeId, searchData) {

        this.illnessService.getWhereNameStartsWith(searchData.keyword).$promise.then(filteredIllnesses => {

            this.scope.illnesses[typeId] = filteredIllnesses;
        });

    };

    IllnessController.$inject = ['IllnessType', 'EnumConverter', 'IllnessService', '$scope'];

    angular
        .module('wild.illness')
        .controller('IllnessController', IllnessController);

})(angular);
