﻿(function (angular) {

    'use strict';

    const cityApiUri = '/api/illness/';

    function IllnessService(apiWrapper) {
        this.api = apiWrapper(cityApiUri);
    }

    IllnessService.prototype.getWhereNameStartsWith = function (name) {

        return this.api.query({ nameStart: name });

    };

    IllnessService.$inject = ['$resource'];

    angular
        .module('wild.illness')
        .service('IllnessService', IllnessService);

})(angular);
