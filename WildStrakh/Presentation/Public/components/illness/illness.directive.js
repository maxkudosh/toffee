﻿(function (angular) {

    'use strict';

    angular
        .module('wild.illness')
        .directive('customerIllness', function () {

            return {

                restrict: 'E',
                templateUrl: '/public/components/illness/illness.html',
                scope: {
                    customerIllnesses: '=illnesses'
                }

            };

        });

})(angular);
