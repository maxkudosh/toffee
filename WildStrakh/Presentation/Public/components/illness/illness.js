﻿(function (angular) {

    'use strict';

    angular
        .module('wild.illness', [
            'ui.bootstrap',
            'isteven-multi-select',
            'wild.services',
            'wild.enums'
        ]);

})(angular);
