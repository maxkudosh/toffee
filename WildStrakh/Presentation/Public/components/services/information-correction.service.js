﻿(function (angular) {

    'use strict';

    function InformationCorrectionService(apiWrapper) {

        function map(correction) {
            correction.requestDate = new Date(correction.requestDate);
            correction.responseDate = new Date(correction.responseDate);
            correction.isClosed = !!correction.response;
            return correction;
        }

        this.detailsApi = apiWrapper('/api/informationcorrection/details/:id', map);
        this.applicationApi = apiWrapper('/api/informationcorrection/application/all/:applicationId', map);
        this.applicationRespondApi = apiWrapper('/api/informationcorrection/application/respond/:id', map);
        this.noticesApi = apiWrapper('/api/informationcorrection/notice/all/:noticeId', map);
        this.noticesRespondApi = apiWrapper('/api/informationcorrection/notice/respond/:id', map);
        this.terminationApi = apiWrapper('/api/informationcorrection/termination/all/:terminationId', map);
        this.terminationRespondApi = apiWrapper('/api/informationcorrection/termination/respond/:id', map);
    }

    InformationCorrectionService.prototype.getDetails = function (id) {
        return this.detailsApi.get({ id: id });
    };

    InformationCorrectionService.prototype.requestForApplication = function (request) {
        return this.applicationApi.create(request);
    };

    InformationCorrectionService.prototype.getByApplicationId = function (applicationId) {
        return this.applicationApi.getAll({ applicationId: applicationId });
    };

    InformationCorrectionService.prototype.getForApplication = function () {
        return this.applicationApi.getAll();
    };

    InformationCorrectionService.prototype.respondForApplication = function (response) {
        return this.applicationRespondApi.create(response);
    };

    InformationCorrectionService.prototype.requestForCase = function (request) {
        return this.noticesApi.create(request);
    };

    InformationCorrectionService.prototype.getByCaseId = function (caseId) {
        return this.noticesApi.getAll({ noticeId: caseId });
    };

    InformationCorrectionService.prototype.getForCase = function () {
        return this.noticesApi.getAll();
    };

    InformationCorrectionService.prototype.respondForCase= function (response) {
        return this.noticesRespondApi.create(response);
    };

    InformationCorrectionService.prototype.requestForTermination = function (request) {
        return this.terminationApi.create(request);
    };

    InformationCorrectionService.prototype.getByTerminationId = function (terminationId) {
        return this.terminationApi.getAll({ terminationId: terminationId });
    };

    InformationCorrectionService.prototype.getForTermination = function () {
        return this.terminationApi.getAll();
    };

    InformationCorrectionService.prototype.respondForTermination = function (response) {
        return this.terminationRespondApi.create(response);
    };

    InformationCorrectionService.$inject = ['ServiceFactory'];

    angular
        .module('wild.services')
        .service('InformationCorrectionService', InformationCorrectionService);

})(angular);
