﻿(function (angular) {

    'use strict';

    function caseServiceBuilder(DocumentStatus, enumConverter, documentServiceFactory) {

        function map(caseNotice) {
            caseNotice.incidentDate = new Date(caseNotice.incidentDate);
            caseNotice.status = enumConverter.fromId(caseNotice.status, DocumentStatus);
            return caseNotice;
        }

        return documentServiceFactory.create('/api/insurancecasenotice/:id/', map);

    }

    caseServiceBuilder.$inject = [
        'DocumentStatus',
        'EnumConverter',
        'DocumentServiceFactory'
    ];

    function CaseReviewService(apiWrapper) {
        this.api = apiWrapper('/api/insurancecasenoticereview/');
    }

    CaseReviewService.prototype.review = function(model) {
        return this.api.save(model);
    };

    CaseReviewService.$inject = [
        '$resource'
    ];

    angular
        .module('wild.services')
        .service('CaseService', caseServiceBuilder)
        .service('CaseReviewService', CaseReviewService);
})(angular);
