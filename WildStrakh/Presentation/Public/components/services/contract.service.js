﻿(function (angular) {

    'use strict';

    function ContractService(serviceFactory, converter, ContractStatus)
    {
        this.converter = converter;
        this.ContractStatus = ContractStatus;
        
        this.api = serviceFactory('/api/contract/:contractId/', (model) => this.map(model));

    }

    ContractService.prototype.isContractExpired = function(contract) {

        const today = new Date();

        return !(contract.startDate <= today
            && contract.endDate >= today);

    };

    ContractService.prototype.isContractActive = function (contract) {

        return !this.isContractExpired(contract) && contract.status === this.ContractStatus.ACTIVE;

    };

    ContractService.prototype.map =  function (model)
    {
        model.startDate = new Date(model.startDate);
        model.endDate = new Date(model.endDate);
        model.person = model.person;
        model.sum = +model.sum;
        model.id = model.id;
        model.status = this.converter.fromId(model.status, this.ContractStatus);

        model.isActive = this.isContractActive(model);
        model.isExpired = this.isContractExpired(model);

        return model;

    }

    ContractService.prototype.getAll = function() {
        return this.api.getAll();
    };

    ContractService.$inject = ['ServiceFactory', 'EnumConverter', 'ContractStatus'];

    angular
        .module('wild.services')
        .service('ContractService', ContractService);

})(angular);
