﻿(function (angular) {

    'use strict';

    angular.module('wild.services', ['ngResource', 'wild.enums', 'wild.attachment']);

})(angular);
