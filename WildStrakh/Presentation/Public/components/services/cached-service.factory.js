﻿(function (angular) {

    'use strict';

     function CachedService(apiWrapper, apiUri) {

        this.api = apiWrapper(apiUri);

        this.cache = null;

    }

    CachedService.prototype.getAll = function () {

        if (this.cached) {
            return this.cached;
        }

        this.cached = this.api.query();

        return this.cached;

    };

    function CachedServiceFactory(apiWrapper) {

        return function(apiUri) {
            return new CachedService(apiWrapper, apiUri);
        }

    }

    CachedServiceFactory.$inject = ['$resource'];

    angular
        .module('wild.services')
        .factory('CachedServiceFactory', CachedServiceFactory);

})(angular);
