﻿(function (angular) {

    'use strict';


    function Service(apiWrapper, apiUri, mapper) {

        this.api = apiWrapper(apiUri);

        this.mapper = mapper;

    }

    Service.prototype.getAll = function (params) {

        let result = [];

        result.$promise = this.api.query(params).$promise;

        result.$promise.then(models => {

            result.push(...models.map(this.mapper));

        });

        return result;

    };

    Service.prototype.get = function (id) {

        let result = {};

        result.$promise = this.api.get(id).$promise;

        result.$promise.then(model => {

            Object.assign(result, this.mapper(model));

        });

        return result;

    };

    Service.prototype.update = function(data) {

        return this.api.update(data);

    };

    Service.prototype.create = function (data) {

        return this.api.save(data);

    };

    function ServiceFactory(apiWrapper) {

        return function(apiUri, mapper) {

            return new Service(apiWrapper, apiUri, mapper);

        };
    }

    ServiceFactory.$inject = ['$resource'];

    angular
        .module('wild.services')
        .factory('ServiceFactory', ServiceFactory);

})(angular);
