﻿(function (angular) {

    'use strict';

    function applicationServiceBuilder(Occupation, PaymentOrder, InsuranceType, DocumentStatus, IllnessType, converter, documentServiceFactory){
        
        function map(application) {
            application.applicant.illnesses.forEach(i => i.illnessType = converter.fromId(i.illnessType, IllnessType));
            application.applicant.dateOfBirth = new Date(application.applicant.dateOfBirth);
            application.beneficiary.dateOfBirth = new Date(application.beneficiary.dateOfBirth);
            application.insuranceType = converter.fromId(application.insuranceType, InsuranceType);
            application.paymentOrder = converter.fromId(application.paymentOrder, PaymentOrder);
            application.status = converter.fromId(application.status, DocumentStatus);
            application.applicant.occupation = converter.fromId(application.applicant.occupation, Occupation);
            return application;
        }

        return documentServiceFactory.create('/api/insuranceapplication/:id/', map);

    }

    applicationServiceBuilder.$inject = [
        'Occupation',
        'PaymentOrder',
        'InsuranceType',
        'DocumentStatus',
        'IllnessType',
        'EnumConverter',
        'DocumentServiceFactory'
    ];

    angular
        .module('wild.services')
        .service('ApplicationService', applicationServiceBuilder);

})(angular);
