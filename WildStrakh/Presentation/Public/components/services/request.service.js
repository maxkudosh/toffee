﻿(function (angular) {

    'use strict';

    function RequestService(serviceFactory) {

        this.api = serviceFactory('/api/additionalinformationrequest/:id/', map);

    }

    function map(model) {

        return {
            requestDate:  new Date(model.requestDate),
            isClosed: !!model.response,
            id: model.id,
            request: model.request,
            response: model.response,
            responseDate: new Date(model.responseDate)
        };

    };

    RequestService.prototype.getAll = function () {

        let requests = {

            active: [],
            notActive: []

        };

        requests.$promise = this.api.getAll().$promise.then(models => {

            models.reduce(
                (a, b) => !b.response ? a.active.push(b) : a.notActive.push(b), requests
            );

        });

        return requests;

    };

    RequestService.prototype.get = function (id) {
        return this.api.get({ id: id });
    };

    RequestService.$inject = ['ServiceFactory'];

    angular
        .module('wild.services')
        .service('RequestService', RequestService);

})(angular);
