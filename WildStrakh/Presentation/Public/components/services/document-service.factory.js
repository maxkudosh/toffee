﻿(function (angular) {

    'use strict';

    function DocumentService(serviceFactory, converter, DocumentStatus, uri, mapper) {

        this.DocumentStatus = DocumentStatus;
        this.mapper = mapper;

        this.api = serviceFactory(uri, mapper);

    }

    DocumentService.prototype.validate = function (id) {
        return this.api.update({ id: id, status: this.DocumentStatus.VALIDATED.id });
    };

    DocumentService.prototype.approve = function (id) {
        return this.api.update({ id: id, status: this.DocumentStatus.APPROVED.id });
    };

    DocumentService.prototype.reject = function (id) {
        return this.api.update({ id: id, status: this.DocumentStatus.REJECTED.id });
    };

    DocumentService.prototype.getPendingValidation = function () {
        return this.api.getAll({ status: this.DocumentStatus.PENDING_VALIDATION.id });
    };

    DocumentService.prototype.getValidated = function () {
        return this.api.getAll({ status: this.DocumentStatus.VALIDATED.id });
    };

    DocumentService.prototype.getProcessed = function () {
        return this.api.getAll({ status: this.DocumentStatus.PROCESSED.id });
    };

    DocumentService.prototype.getPendingApproval = function () {
        return this.api.getAll({ status: this.DocumentStatus.PENDING_APPROVAL.id });
    };

    DocumentService.prototype.getDetails = function (id) {
        return this.api.get({ id: id });
    };

    DocumentService.prototype.getAll = function () {
        return this.api.getAll();
    };

    function documentServiceFactory(serviceFactory, converter, DocumentStatus)
    {
        this.create = function (uri, mapper) {
            return new DocumentService(serviceFactory, converter, DocumentStatus, uri, mapper);
        };
    }

    documentServiceFactory.$inject = ['ServiceFactory', 'EnumConverter', 'DocumentStatus'];

    angular
        .module('wild.services')
        .service('DocumentServiceFactory', documentServiceFactory);

})(angular);
