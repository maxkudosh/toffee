﻿(function (angular) {

    'use strict';

    function map(information) {
        information.requestDate = new Date(information.requestDate);
        information.responseDate = new Date(information.responseDate);
        information.isClosed = !!information.response;
        return information;
    }

    function AdditionalInformationService(apiWrapper, AttachmentsService, serviceFactory) {
        this.detailsApi = serviceFactory('/api/additionalinformation/details/:id', map);
        this.applicationApi = serviceFactory('/api/additionalinformation/application/all/:applicationId', map);
        this.applicationRespondApi = new AttachmentsService('attachments', '/api/additionalinformation/application/respond/:id', apiWrapper);
        this.noticesApi = serviceFactory('/api/additionalinformation/notice/all/:noticeId', map);
        this.noticesRespondApi = new AttachmentsService('attachments', '/api/additionalinformation/notice/respond/:id', apiWrapper);
        this.terminationApi = serviceFactory('/api/additionalinformation/termination/all/:terminationId', map);
        this.terminationRespondApi = new AttachmentsService('attachments', '/api/additionalinformation/termination/respond/:id', apiWrapper);
    }

    AdditionalInformationService.prototype.getDetails = function (id) {
        return this.detailsApi.get({ id: id });
    };

    AdditionalInformationService.prototype.requestForApplication = function (request) {
        return this.applicationApi.create(request);
    };

    AdditionalInformationService.prototype.getByApplicationId = function (applicationId) {
        return this.applicationApi.getAll({ applicationId: applicationId });
    };

    AdditionalInformationService.prototype.getForApplication = function () {
        return this.applicationApi.getAll();
    };

    AdditionalInformationService.prototype.respondForApplication = function (response) {
        return this.applicationRespondApi.api.save(response);
    };

    AdditionalInformationService.prototype.requestForCase = function (request) {
        return this.noticesApi.create(request);
    };

    AdditionalInformationService.prototype.getByCaseId = function (caseId) {
        return this.noticesApi.getAll({ noticeId: caseId });
    };

    AdditionalInformationService.prototype.getForCase = function () {
        return this.noticesApi.getAll();
    };

    AdditionalInformationService.prototype.respondForCase = function (response) {
        return this.noticesRespondApi.api.save(response);
    };

    AdditionalInformationService.prototype.requestForTermination = function (request) {
        return this.terminationApi.create(request);
    };

    AdditionalInformationService.prototype.getByTerminationId = function (terminationId) {
        return this.terminationApi.getAll({ terminationId: terminationId });
    };

    AdditionalInformationService.prototype.getForTermination = function () {
        return this.terminationApi.getAll();
    };

    AdditionalInformationService.prototype.respondForTermination = function (response) {
        return this.terminationRespondApi.api.save(response);
    };

    AdditionalInformationService.$inject = ['$resource', 'ResourceWithAttachmentsService', 'ServiceFactory'];

    angular
        .module('wild.services')
        .service('AdditionalInformationService', AdditionalInformationService);

})(angular);
