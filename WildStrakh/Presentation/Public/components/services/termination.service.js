﻿(function (angular) {

    'use strict';

    function terminationServiceBuilder(DocumentStatus, enumConverter, documentServiceFactory) {

        function map(termination) {
            termination.status = enumConverter.fromId(termination.status, DocumentStatus);
            return termination;
        }

        return documentServiceFactory.create('/api/terminationapplication/:id/', map);

    }

    terminationServiceBuilder.$inject = [
        'DocumentStatus',
        'EnumConverter',
        'DocumentServiceFactory'
    ];

    angular
        .module('wild.services')
        .service('TerminationService', terminationServiceBuilder);

})(angular);
