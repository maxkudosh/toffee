﻿using System.Collections;
using System.Net.Http;
using System.Web.Http.Filters;

namespace Presentation.Filters
{
    public class JsonEnumerableWrapperFilter: ActionFilterAttribute
    {
        public override void OnActionExecuted(HttpActionExecutedContext actionExecutedContext)
        {

            // This action filter is used to wrap json arrays in a property to 
            // avoid XSRF issue.

            ObjectContent response = actionExecutedContext.Response.Content as ObjectContent;

            if (response?.Value is IEnumerable)
            {
                response.Value = new { data = response.Value };
            }

            base.OnActionExecuted(actionExecutedContext);

        }
    }
}
