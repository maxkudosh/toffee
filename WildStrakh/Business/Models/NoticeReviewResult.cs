﻿using System;

namespace Business.Models
{
    public class NoticeReviewResult
    {
        public Guid NoticeId
        {
            get;
            set;
        }
        public bool IsApproved
        {
            get;
            set;
        }

        public decimal? Payment
        {
            get;
            set;
        }
    }
}
