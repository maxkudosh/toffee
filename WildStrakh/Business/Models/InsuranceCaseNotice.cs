﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Business.Models
{
    public class InsuranceCaseNotice
    {
        [Required]
        public Guid? ContractId
        {
            get;
            set;
        }

        [Required]
        public DateTime? IncidentDate
        {
            get;
            set;
        }

        [Required]
        public string Whereabouts
        {
            get;
            set;
        }

        [Required]
        public string Circumstances
        {
            get;
            set;
        }

        public List<Attachment> Attachments
        {
            get;
            set;
        }
    }
}
