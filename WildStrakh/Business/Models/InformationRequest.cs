﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Business.Models
{
    public class InformationRequest
    {
        [Required]
        public string Content
        {
            get;
            set;
        }

        [Required]
        public Guid? DocumentId
        {
            get;
            set;
        }
    }
}
