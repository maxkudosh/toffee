﻿using System.Collections.Generic;

namespace Business.Models
{
    public class AdditionalInformation : InformationCorrection
    {
        public List<Attachment> Attachments
        {
            get;
            set;
        }
    }
}
