﻿using System;
using System.ComponentModel.DataAnnotations;
using Domain.Entities.Dictionary;

namespace Business.Models
{
    public class PersonIllness
    {
        [Required]
        public Guid Id
        {
            get;
            set;
        }

        [Required]
        public IllnessType? IllnessType
        {
            get;
            set;
        }
    }
}
