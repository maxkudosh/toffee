﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Business.Models
{
    public class BankCard
    {
        [Required]
        public string CardNumber
        {
            get;
            set;
        }

        [Required]
        public string CardholderName
        {
            get;
            set;
        }

        [Required]
        public ExpirationDate ExpirationDate
        {
            get;
            set;
        }

        [Required]
        public int? Cvc
        {
            get;
            set;
        }
    }
}
