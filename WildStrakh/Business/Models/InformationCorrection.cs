﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Business.Models
{
    public class InformationCorrection
    {
        [Required]
        public Guid? Id
        {
            get;
            set;
        }

        [Required]
        public string Content
        {
            get;
            set;
        }
    }
}
