﻿using System.ComponentModel.DataAnnotations;

using Common;

namespace Business.Models
{
    public class Address
    {
        [Required]
        public City City
        {
            get;
            set;
        }

        [Required]
        public string AddressLine
        {
            get;
            set;
        }

        [Required]
        [StringLength(Constants.PostalCodeLength, MinimumLength = Constants.PostalCodeLength)]
        public string PostalCode
        {
            get;
            set;
        }
    }
}
