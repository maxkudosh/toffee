﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Business.Models
{
    public class Person
    {
        [Required]
        public string FirstName
        {
            get;
            set;
        }

        [Required]
        public string MiddleName
        {
            get;
            set;
        }

        [Required]
        public string LastName
        {
            get;
            set;
        }

        [Required]
        public Passport Passport
        {
            get;
            set;
        }

        [Required]
        public DateTime? DateOfBirth
        {
            get;
            set;
        }
    }
}
