﻿using System;

namespace Business.Models
{
    public class Contract
    {
        public Guid Id
        {
            get;
            set;
        }

        public DateTime StartDate
        {
            get;
            set;
        }

        public DateTime EndDate
        {
            get;
            set;
        }

        public decimal Amount
        {
            get;
            set;
        }

        public decimal Premium
        {
            get;
            set;
        }
    }
}
