﻿using System.ComponentModel.DataAnnotations;
using Domain.Entities.Dictionary;

namespace Business.Models
{
    public class Applicant: Person
    {
        [Required]
        public Address Address
        {
            get;
            set;
        }

        [Required]
        [MinLength(0)]
        public PersonIllness[] Illnesses
        {
            get;
            set;
        }

        [Required]
        public Occupation Occupation
        {
            get;
            set;
        }

        public string OccupationDetails
        {
            get;
            set;
        }
    }
}
