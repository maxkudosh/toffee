﻿using System.ComponentModel.DataAnnotations;

namespace Business.Models
{
    public class ExpirationDate
    {
        [Required]
        public int? Month { get; set; }

        [Required]
        public int? Year { get; set; }
    }
}
