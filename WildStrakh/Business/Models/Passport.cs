﻿using System.ComponentModel.DataAnnotations;

using Common;

namespace Business.Models
{
    public class Passport
    {
        [Required]
        [StringLength(Constants.PassportSeriesLength, MinimumLength = Constants.PassportSeriesLength
            )]
        public string Series
        {
            get;
            set;
        }

        [Required]
        [StringLength(Constants.PassportNumberLength, MinimumLength = Constants.PassportNumberLength
            )]
        public string Number
        {
            get;
            set;
        }

        [Required]
        public string IdentificationNumber
        {
            get;
            set;
        }
    }
}
