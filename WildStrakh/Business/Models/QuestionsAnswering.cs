﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Business.Models
{
    public class QuestionsAnswering
    {
        [Required]
        public List<QuestionAnswer> Answers
        {
            get;
            set;
        }

        [Required]
        public Guid? InsuranceApplicationId
        {
            get;
            set;
        }
    }
}
