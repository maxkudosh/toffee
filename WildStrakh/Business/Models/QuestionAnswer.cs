﻿namespace Business.Models
{
    public class QuestionAnswer
    {
        public string Question
        {
            get;
            set;
        }

        public int Answer
        {
            get;
            set;
        }
    }
}
