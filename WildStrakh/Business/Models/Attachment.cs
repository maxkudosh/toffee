﻿using System.ComponentModel.DataAnnotations;

using MultipartDataMediaFormatter.Infrastructure;

namespace Business.Models
{
    public class Attachment
    {
        [Required]
        public HttpFile File
        {
            get;
            set;
        }

        public string Description
        {
            get;
            set;
        }
    }
}
