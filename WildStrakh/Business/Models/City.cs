﻿using System.ComponentModel.DataAnnotations;

namespace Business.Models
{
    public class City
    {
        [Required]
        public string Name
        {
            get;
            set;
        }

        public string Region
        {
            get;
            set;
        }

        public string District
        {
            get;
            set;
        }
    }
}
