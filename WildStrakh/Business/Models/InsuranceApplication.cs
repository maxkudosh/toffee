﻿using System.ComponentModel.DataAnnotations;

using Business.ValidationAttributes;
using Domain.Entities.Dictionary;

namespace Business.Models
{
    public class InsuranceApplication
    {
        [Required]
        public InsuranceType? InsuranceType
        {
            get;
            set;
        }

        [Required]
        [PositiveNumber]
        public decimal? InsuranceSum
        {
            get;
            set;
        }

        [Required]
        public PaymentOrder? PaymentOrder
        {
            get;
            set;
        }

        [Required]
        public Applicant Applicant
        {
            get;
            set;
        }

        [Required]
        public Person Beneficiary
        {
            get;
            set;
        }
    }
}
