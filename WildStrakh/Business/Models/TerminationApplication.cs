﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Business.Models
{
    public class TerminationApplication
    {
        [Required]
        public Guid? ContractId
        {
            get;
            set;
        }

        [Required]
        public string Comment
        {
            get;
            set;
        }

        public List<Attachment> Attachments
        {
            get;
            set;
        }
    }
}
