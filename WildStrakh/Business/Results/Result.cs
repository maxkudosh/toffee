﻿namespace Business.Results
{
    public class Result<TValue> : VoidResult
    {
        protected readonly TValue _value;

        public TValue Value => _value;

        protected Result(bool hasSucceeded, TValue value = default(TValue), string error = null)
            : base(hasSucceeded, error)
        {
            _value = value;
        }

        public static Result<TValue> Success(TValue value)
        {
            return new Result<TValue>(true, value);
        }

        public new static Result<TValue> Failure(string error)
        {
            return new Result<TValue>(false, error: error);
        }
    }
}
