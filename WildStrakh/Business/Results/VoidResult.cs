﻿namespace Business.Results
{
    public class VoidResult
    {
        protected readonly bool _hasSucceeded;

        protected readonly string _errorMessage;

        public bool HasSucceded => _hasSucceeded;

        public string ErrorMessage => _errorMessage;

        protected VoidResult(bool hasSucceeded, string errorMessage = null)
        {
            _hasSucceeded = hasSucceeded;
            _errorMessage = errorMessage;
        }

        public static VoidResult Success()
        {
            return new VoidResult(true);
        }

        public static VoidResult Failure(string error)
        {
            return new VoidResult(true, error);
        }
    }
}
