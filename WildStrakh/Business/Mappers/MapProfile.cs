﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common;
using Domain.Entities;
using Domain.Entities.Bank;
using Domain.Entities.Dictionary;
using Domain.Entities.Insurance;
using Domain.Entities.PersonInformation;
using Domain.Questions;
using MultipartDataMediaFormatter.Infrastructure;

namespace Business.Mappers
{
    public static class MapProfile
    {
        public static List<T> ConvertToList<T>(this T x) => new List<T>
        {
            x
        };

        public static Passport Map(this Models.Passport model) => new Passport
        {
            Id = Guid.NewGuid(),
            SerialNumber = int.Parse(model.Number),
            Series = model.Series,
            IdentificationNumber = model.IdentificationNumber
        };

        public static Address Map(this Models.Address model) => new Address
        {
            Id = Guid.NewGuid(),
            AddressLine = model.AddressLine,
            City = model.City.Name,
            Region = model.City.Region,
            District = model.City.District,
            PostalCode = int.Parse(model.PostalCode)
        };

        public static Person Map(this Models.Person model) => new Person
        {
            Id = Guid.NewGuid(),
            FirstName = model.FirstName,
            MiddleName = model.MiddleName,
            LastName = model.LastName,
            DateOfBirth = model.DateOfBirth.Value,
            Passport = model.Passport.Map()
        };

        public static ViewModels.Illness Map(this Illness illness) => new ViewModels.Illness
        {
            Name = illness.Name,
            Id = illness.Id
        };

        public static ViewModels.InsuranceApplication Map(this InsuranceApplication application)
            => new ViewModels.InsuranceApplication
            {
                Id = application.Id,
                Applicant = application.Applicant.MapToApplicant(),
                Beneficiary = application.Beneficiary.MapToPerson(),
                InsuranceType = application.InsuranceType,
                PaymentOrder = application.PaymentOrder,
                InsuranceSum = application.InsuranceSum,
                Status = application.Status
            };

        public static ViewModels.Applicant MapToApplicant(this Person person)
            => new ViewModels.Applicant
            {
                FirstName = person.FirstName,
                MiddleName = person.MiddleName,
                LastName = person.LastName,
                DateOfBirth = person.DateOfBirth,
                Passport = person.Passport.Map(),
                Address = person.Address.Map(),
                Occupation = person.Occupation,
                OccupationDetails = person.OccupationDetails
            };

        public static ViewModels.PersonIllness Map(this PersonIllness personIllness)
            => new ViewModels.PersonIllness
            {
                Id = personIllness.Illness.Id,
                IllnessType = personIllness.IllnessType,
                Name = personIllness.Illness.Name
            };

        public static ViewModels.Person MapToPerson(this Person person) => new ViewModels.Person
        {
            FirstName = person.FirstName,
            MiddleName = person.MiddleName,
            LastName = person.LastName,
            DateOfBirth = person.DateOfBirth,
            Passport = person.Passport.Map()
        };

        public static ViewModels.Passport Map(this Passport passport) => new ViewModels.Passport
        {
            IdentificationNumber = passport.IdentificationNumber,
            Number = passport.SerialNumber.ToString(),
            Series = passport.Series
        };

        public static ViewModels.Address Map(this Address address) => new ViewModels.Address
        {
            AddressLine = address.AddressLine,
            City = address.MapToCity(),
            PostalCode = address.PostalCode.ToString()
        };

        public static ViewModels.City MapToCity(this Address address) => new ViewModels.City
        {
            District = address.District,
            Name = address.City,
            Region = address.Region
        };

        public static AdditionalInformation Map(this Models.InformationRequest model,
            InformationType informationType) => new AdditionalInformation
            {
                Id = Guid.NewGuid(),
                Request = model.Content,
                RequestDate = DateTime.Now,
                InformationType = informationType
            };

        public static Attachment Map(this Models.Attachment model) => new Attachment
        {
            Id = Guid.NewGuid(),
            Description = model.Description,
            File = model.File.Map()
        };

        public static InsuranceApplication Map(this Models.InsuranceApplication model)
            => new InsuranceApplication
            {
                Id = Guid.NewGuid(),
                InsuranceType = model.InsuranceType.Value,
                PaymentOrder = model.PaymentOrder.Value,
                Status = DocumentStatus.PendingValidation,
                StatusChangedDate = DateTime.Now,
                InsuranceSum = model.InsuranceSum,
                CreatedDate = DateTime.Today,
                ActualDate = DateTime.Today
            };

        public static UserFile Map(this HttpFile file) => new UserFile
        {
            Id = Guid.NewGuid(),
            Content = file.Buffer
        };

        public static ViewModels.Attachment Map(this Attachment entity) => new ViewModels.Attachment
        {
            Id = entity.Id,
            Description = entity.Description
        };

        public static ViewModels.AttachmentContent MapContent(this Attachment entity)
            => new ViewModels.AttachmentContent
            {
                Content = entity.File.Content
            };

        public static ViewModels.AdditionalInformation Map(this AdditionalInformation entity)
            => new ViewModels.AdditionalInformation
            {
                Id = entity.Id,
                Request = entity.Request,
                RequestDate = entity.RequestDate,
                Response = entity.Response,
                ResponseDate = entity.ResponseDate,
                Attachments = entity.Attachments.Select(att => att.Map()).ToList()
            };

        public static ViewModels.QuestionsAsking MapToQuestionsAsking(
            this ViewModels.InsuranceApplication model) => new ViewModels.QuestionsAsking
            {
                InsuranceApplication = model,
                Questions = new List<ViewModels.Question>()
            };

        public static ViewModels.Question Map(this IQuestion question) => new ViewModels.Question
        {
            Content = question.Content,
            Answers = question.Answers
        };

        public static Contract MapToContract(this InsuranceApplication application) => new Contract
        {
            Person = application.Person,
            Beneficiary = application.Beneficiary,
            Id = Guid.NewGuid(),
            Status = ContractStatus.Created,
            StatusChangedDate = DateTime.Now,
            PaymentOrder = application.PaymentOrder,
            InsuranceType = application.InsuranceType,
            StartDate = DateTime.Now
        };

        public static ViewModels.Contract Map(this Contract contract) => new ViewModels.Contract
        {
            Person = contract.Person.MapToApplicant(),
            Id = contract.Id,
            Status = contract.Status,
            Amount = contract.Amount,
            EndDate = contract.EndDate,
            StartDate = contract.StartDate,
            Premium = contract.Premium,
            PaymentOrder = contract.PaymentOrder
        };

        public static ViewModels.ContractApproval Map(
            this IEnumerable<ViewModels.Contract> contracts,
            ViewModels.InsuranceApplication application) => new ViewModels.ContractApproval
            {
                ContractVariants = contracts.ToList(),
                InsuranceApplication = application
            };

        public static void MapToApproved(this Models.Contract model, Contract entity)
        {
            entity.StartDate = model.StartDate;
            entity.EndDate = model.EndDate;
            entity.Premium = model.Premium;
            entity.Amount = model.Amount;
            entity.Status = ContractStatus.PendingPayment;
            entity.StatusChangedDate = DateTime.Now;
        }

        public static TerminationApplication Map(this Models.TerminationApplication application,
            Contract contract) => new TerminationApplication
            {
                Id = Guid.NewGuid(),
                ActualDate = DateTime.Now,
                Attachments = application.Attachments?.Select(att => att.Map()).ToList(),
                Comment = application.Comment,
                CreatedDate = DateTime.Now,
                Status = DocumentStatus.PendingValidation,
                StatusChangedDate = DateTime.Now,
                Contract = contract
            };

        public static InsuranceCaseNotice Map(this Models.InsuranceCaseNotice notice,
            Contract contract) => new InsuranceCaseNotice
            {
                Id = Guid.NewGuid(),
                ActualDate = notice.IncidentDate.Value,
                Attachments = notice.Attachments?.Select(att => att.Map()).ToList(),
                Place = notice.Whereabouts,
                Description = notice.Circumstances,
                CreatedDate = DateTime.Now,
                Contract = contract,
                Status = DocumentStatus.PendingValidation,
                StatusChangedDate = DateTime.Now
            };

        public static ViewModels.InsuranceCaseNotice Map(this InsuranceCaseNotice notice)
            => new ViewModels.InsuranceCaseNotice
            {
                Contract = notice.Contract.Map(),
                Attachments = notice.Attachments?.Select(att => att.Map()).ToList(),
                NoticeId = notice.Id,
                Whereabouts = notice.Place,
                Circumstances = notice.Description,
                IncidentDate = notice.ActualDate,
                Status = notice.Status
            };

        public static ViewModels.Termination Map(this TerminationApplication entity)
            => new ViewModels.Termination
            {
                Contract = entity.Contract.Map(),
                Attachments = entity.Attachments?.Select(att => att.Map()).ToList(),
                Comment = entity.Comment,
                Status = entity.Status,
                AdditionalInformations =
                    entity.AdditionalInformations?.Select(info => info.Map()).ToList(),
                TerminationId = entity.Id
            };

        public static Account Map(this Contract contract, Models.BankCard card) => new Account
        {
            Id = Guid.NewGuid(),
            ExpirationDate = contract.EndDate,
            Balance = Constants.PersonAccountStartBalance,
            AccountType = AccountType.User,
            CardNumber = card.CardNumber,
            CardholderName = card.CardholderName,
            Cvc = card.Cvc.Value,
            Owner = contract.Person
        };

        public static ViewModels.Transaction Map(this Transaction transaction)
            => new ViewModels.Transaction
            {
                Person = transaction.SourceAccount.Owner.MapToApplicant(),
                PaymentOrder = transaction.Type,
                Status = transaction.Status,
                Amount = transaction.Amount,
                PlannedDate = transaction.PlannedDate,
                TransactionDate = transaction.TransactionDate
            };

        public static ViewModels.Account Map(this Account account) => new ViewModels.Account
        {
            Balance = account.Balance,
            AccountType = account.AccountType,
            CardNumber = account.CardNumber,
            Owner = account.Owner?.MapToApplicant()
        };
    }
}
