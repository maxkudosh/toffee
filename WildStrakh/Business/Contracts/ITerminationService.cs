﻿using System;
using System.Collections.Generic;
using Business.Results;
using Domain.Entities.Dictionary;

namespace Business.Contracts
{
    public interface ITerminationService : IStatementDocumentService
    {
        VoidResult Apply(Models.TerminationApplication application);

        Result<List<ViewModels.Termination>> GetAllByStatus(DocumentStatus status);

        Result<List<ViewModels.Termination>> GetByUserId(Guid userId);

        Result<ViewModels.Termination> GetApplication(Guid id);

        VoidResult Reject(Guid applicationId);

        VoidResult Terminate(Guid applicationId);

        VoidResult UpdateTerminationApplication(Guid applicationId,
            Models.TerminationApplication application);
    }
}
