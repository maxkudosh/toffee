﻿using System.Collections.Generic;
using Business.ViewModels;

namespace Business.Contracts
{
    public interface IIllnessService
    {
        List<Illness> GetWhereNameStartWith(string s);
    }
}
