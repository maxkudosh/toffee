﻿using System;
using System.Collections.Generic;
using Business.Results;
using Domain.Entities.Dictionary;
using Domain.Entities.Insurance;

namespace Business.Contracts
{
    public interface IAdditionalInformationService
    {
        Result<ViewModels.AdditionalInformation> GetByRequestId(Guid id);

        Result<List<ViewModels.AdditionalInformation>> GetAllByUserId(Guid userId,
            InformationType type);

        Result<List<ViewModels.AdditionalInformation>> GetAllByUserId<T>(Guid userId,
            InformationType type) where T : StatementDocument;

        VoidResult RequestInformationCorrection<T>(Models.InformationRequest request)
            where T : StatementDocument;

        VoidResult RequestAdditionalInformation<T>(Models.InformationRequest request)
            where T : StatementDocument;

        VoidResult RespondInformationCorrection<T>(Guid informationId,
            Models.InformationCorrection response) where T : StatementDocument;

        VoidResult RespondAdditionalInformation<T>(Guid informationId,
            Models.AdditionalInformation response) where T : StatementDocument;

        VoidResult RespondAdditionalInformation(Guid informationId,
            Models.AdditionalInformation response);

        Result<List<ViewModels.AdditionalInformation>> GetByDocumentId<T>(Guid documentId,
            InformationType type) where T : StatementDocument;

        Result<ViewModels.AttachmentContent> GetAttachmentContent(Guid id);
    }
}
