﻿using System;
using Business.Results;
using Domain.Entities.Insurance;

namespace Business.Contracts
{
    public interface IBankService
    {
        void CreatePersonalAccount(Contract contract, Models.BankCard card);

        VoidResult ProcessInstantPaymentToSystem(Contract contract);

        VoidResult ProcessInstantPaymentToClient(Guid noticeId);

        VoidResult ProcessScheduledPayments();

        VoidResult PlanPeriodicPayments(Contract contract);
    }
}
