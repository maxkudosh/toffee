﻿using System;
using System.Collections.Generic;
using Business.Results;

namespace Business.Contracts
{
    public interface IContractService
    {
        Result<ViewModels.ContractPdf> GetContract(Guid contractId);

        Result<List<ViewModels.Contract>> GetContractsByUserId(Guid userId);

        Result<ViewModels.QuestionsAsking> PrepareQuestions(Guid insuranceApplicationId);

        VoidResult GenerateContractCandidates(Models.QuestionsAnswering questionsAnswering);

        Result<ViewModels.ContractApproval> GetContractVariants(Guid insuranceApplicationId);

        VoidResult ApproveContract(Models.Contract model);

        VoidResult RejectInsuranceApplication(Guid insuranceApplicationId);

        VoidResult MakeFirstPayment(Guid contractId, Models.BankCard card);

        VoidResult RejectCreatedContract(Guid contractId);
    }
}
