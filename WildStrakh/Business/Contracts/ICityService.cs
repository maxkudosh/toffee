﻿using System.Collections.Generic;

using Business.ViewModels;

namespace Business.Contracts
{
    public interface ICityService
    {
        List<City> GetAll();

        List<City> GetWhereNameStartsWith(string s);
    }
}
