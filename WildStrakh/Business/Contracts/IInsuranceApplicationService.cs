﻿using System;
using System.Collections.Generic;
using Business.Results;
using Domain.Entities.Dictionary;

namespace Business.Contracts
{
    public interface IInsuranceApplicationService : IStatementDocumentService
    {
        VoidResult Apply(Guid userId, Models.InsuranceApplication application);

        Result<List<ViewModels.InsuranceApplication>> GetAllByStatus(DocumentStatus status);

        Result<List<ViewModels.InsuranceApplication>> GetByUserId(Guid userId);

        Result<ViewModels.InsuranceApplication> GetById(Guid id);

        VoidResult UpdateInsuranceApplication(Guid applicationId,
            Models.InsuranceApplication application);
    }
}
