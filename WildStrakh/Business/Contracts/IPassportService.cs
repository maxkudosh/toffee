﻿using System.Collections.Generic;

using Business.ViewModels;

namespace Business.Contracts
{
    public interface IPassportService
    {
        List<PassportSeries> GetAllSeries();
    }
}
