﻿using System;
using System.Collections.Generic;

using Domain.Entities.PersonInformation;

namespace Business.Contracts
{
    public interface IPersonIllnessService
    {
        void InsertIllnesses(Models.Applicant model, Person applicant);

        List<ViewModels.Illness> GetIllnessesFromInitialDataFile();

        List<ViewModels.PersonIllness> GetIllnessesByPersonId(Guid id);
    }
}
