﻿using System;
using System.Collections.Generic;
using Business.Results;
using Domain.Entities.Dictionary;

namespace Business.Contracts
{
    public interface IInsuranceCaseNoticeService : IStatementDocumentService
    {
        VoidResult Notify(Models.InsuranceCaseNotice notice);

        Result<List<ViewModels.InsuranceCaseNotice>> GetAllByStatus(DocumentStatus status);

        Result<List<ViewModels.InsuranceCaseNotice>> GetByUserId(Guid userId);

        Result<ViewModels.InsuranceCaseNotice> GetNotice(Guid noticeId);

        VoidResult ReviewNotice(Models.NoticeReviewResult reviewResult);

        VoidResult ReuqestPayout(Guid noticeId);

        VoidResult UpdateNotice(Guid noticeId, Models.InsuranceCaseNotice notice);
    }
}
