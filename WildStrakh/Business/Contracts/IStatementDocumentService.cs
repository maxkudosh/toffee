﻿using System;
using Business.Models;
using Business.Results;

namespace Business.Contracts
{
    public interface IStatementDocumentService
    {
        VoidResult RequestInformationCorrection(InformationRequest request);

        VoidResult RequestAdditionalInformation(InformationRequest request);

        VoidResult RespondInformationCorrection(Guid informationId, InformationCorrection response);

        VoidResult RespondAdditionalInformation(Guid informationId, AdditionalInformation response);

        VoidResult CompleteValidation(Guid documentId);
    }
}
