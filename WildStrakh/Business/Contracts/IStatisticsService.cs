﻿using System;
using System.Collections.Generic;
using Business.Results;

namespace Business.Contracts
{
    public interface IStatisticsService
    {
        Result<List<ViewModels.Contract>> GetActiveContracts();

        Result<List<ViewModels.Applicant>> GetClientsWithActiveContracts();

        Result<List<ViewModels.Transaction>> GetCompletedTransactions(DateTime startDate,
            DateTime endDate);

        Result<List<ViewModels.Transaction>> GetAllTransactions();

        Result<List<ViewModels.Account>> GetAccounts();
    }
}
