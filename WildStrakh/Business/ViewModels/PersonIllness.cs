﻿using System;
using Domain.Entities.Dictionary;

namespace Business.ViewModels
{
    public class PersonIllness
    {
        public Guid Id
        {
            get;
            set;
        }

        public string Name
        {
            get;
            set;
        }

        public IllnessType IllnessType
        {
            get;
            set;
        }
    }
}
