﻿namespace Business.ViewModels
{
    public class PassportSeries
    {
        public string Name
        {
            get;
            set;
        }

        public string Value
        {
            get;
            set;
        }
    }
}
