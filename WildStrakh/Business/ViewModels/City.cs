﻿namespace Business.ViewModels
{
    public class City
    {
        public string Name
        {
            get;
            set;
        }

        public string Region
        {
            get;
            set;
        }

        public string District
        {
            get;
            set;
        }
    }
}
