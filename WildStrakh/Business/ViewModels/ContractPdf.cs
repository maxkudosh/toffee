﻿namespace Business.ViewModels
{
    public class ContractPdf
    {
        public Contract Contract
        {
            get;
            set;
        }

        public Person Beneficiary
        {
            get;
            set;
        }
    }
}
