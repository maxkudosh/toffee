﻿using System;

namespace Business.ViewModels
{
    public class Illness
    {
        public string Name
        {
            get;
            set;
        }

        public Guid Id
        {
            get; set;
        }
    }
}
