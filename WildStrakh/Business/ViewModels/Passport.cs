﻿namespace Business.ViewModels
{
    public class Passport
    {
        public string Series
        {
            get;
            set;
        }

        public string Number
        {
            get;
            set;
        }

        public string IdentificationNumber
        {
            get;
            set;
        }
    }
}
