﻿using Common;

namespace Business.ViewModels
{
    public class AttachmentContent
    {
        public string MimeType => Constants.PdfMimeType;

        public byte[] Content
        {
            get;
            set;
        }
    }
}
