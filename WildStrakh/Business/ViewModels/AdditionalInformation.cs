﻿using System.Collections.Generic;

namespace Business.ViewModels
{
    public class AdditionalInformation : InformationCorrection
    {
        public List<Attachment> Attachments
        {
            get;
            set;
        }
    }
}
