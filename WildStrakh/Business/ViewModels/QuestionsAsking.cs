﻿using System.Collections.Generic;

namespace Business.ViewModels
{
    public class QuestionsAsking
    {
        public InsuranceApplication InsuranceApplication
        {
            get;
            set;
        }

        public List<Question> Questions
        {
            get;
            set;
        }
    }
}
