﻿using System;
using Domain.Entities.Dictionary;

namespace Business.ViewModels
{
    public class Transaction
    {
        public PaymentOrder PaymentOrder
        {
            get;
            set;
        }

        public Applicant Person
        {
            get;
            set;
        }

        public decimal Amount
        {
            get;
            set;
        }

        public DateTime TransactionDate
        {
            get;
            set;
        }

        public DateTime PlannedDate
        {
            get;
            set;
        }

        public TransactionStatus Status
        {
            get;
            set;
        }
    }
}
