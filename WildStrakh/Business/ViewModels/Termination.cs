﻿using System;
using System.Collections.Generic;
using Domain.Entities.Dictionary;

namespace Business.ViewModels
{
    public class Termination
    {
        public Guid TerminationId
        {
            get;
            set;
        }

        public Contract Contract
        {
            get;
            set;
        }

        public string Comment
        {
            get;
            set;
        }

        public List<AdditionalInformation> AdditionalInformations
        {
            get;
            set;
        }

        public List<Attachment> Attachments
        {
            get;
            set;
        }

        public DocumentStatus Status
        {
            get;
            set;
        }
    }
}
