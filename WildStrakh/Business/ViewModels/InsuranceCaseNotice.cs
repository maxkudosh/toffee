﻿using System;
using System.Collections.Generic;
using Domain.Entities.Dictionary;

namespace Business.ViewModels
{
    public class InsuranceCaseNotice
    {
        public Guid NoticeId
        {
            get;
            set;
        }

        public Contract Contract
        {
            get;
            set;
        }

        public DateTime IncidentDate
        {
            get;
            set;
        }

        public string Whereabouts
        {
            get;
            set;
        }

        public string Circumstances
        {
            get;
            set;
        }

        public List<Attachment> Attachments
        {
            get;
            set;
        }

        public DocumentStatus Status
        {
            get;
            set;
        }
    }
}
