﻿using System.Collections.Generic;

namespace Business.ViewModels
{
    public class Question
    {
        public string Content
        {
            get;
            set;
        }

        public Dictionary<int, string> Answers
        {
            get;
            set;
        }
    }
}
