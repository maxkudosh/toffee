﻿using System;

namespace Business.ViewModels
{
    public class Attachment
    {
        public Guid Id
        {
            get;
            set;
        }

        public string Description
        {
            get;
            set;
        }
    }
}
