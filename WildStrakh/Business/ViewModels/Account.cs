﻿using Domain.Entities.Dictionary;

namespace Business.ViewModels
{
    public class Account
    {
        public AccountType AccountType
        {
            get;
            set;
        }

        public decimal Balance
        {
            get;
            set;
        }

        public Applicant Owner
        {
            get;
            set;
        }

        public string CardNumber
        {
            get;
            set;
        }
    }
}
