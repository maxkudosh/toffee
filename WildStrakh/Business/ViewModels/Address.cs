﻿namespace Business.ViewModels
{
    public class Address
    {
        public City City
        {
            get;
            set;
        }

        public string AddressLine
        {
            get;
            set;
        }

        public string PostalCode
        {
            get;
            set;
        }
    }
}
