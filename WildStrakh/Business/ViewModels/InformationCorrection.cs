﻿using System;

namespace Business.ViewModels
{
    public class InformationCorrection
    {
        public Guid Id
        {
            get;
            set;
        }

        public string Request
        {
            get;
            set;
        }

        public DateTime RequestDate
        {
            get;
            set;
        }

        public string Response
        {
            get;
            set;
        }

        public DateTime? ResponseDate
        {
            get;
            set;
        }
    }
}
