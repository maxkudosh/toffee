﻿using Domain.Entities.Dictionary;

namespace Business.ViewModels
{
    public class Applicant : Person
    {
        public Address Address
        {
            get;
            set;
        }

        public PersonIllness[] Illnesses
        {
            get;
            set;
        }

        public Occupation Occupation
        {
            get;
            set;
        }

        public string OccupationDetails
        {
            get;
            set;
        }
    }
}
