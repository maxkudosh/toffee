﻿using System;
using Domain.Entities.Dictionary;

namespace Business.ViewModels
{
    public class Contract
    {
        public Guid Id
        {
            get;
            set;
        }

        public Applicant Person
        {
            get;
            set;
        }

        public DateTime StartDate
        {
            get;
            set;
        }

        public DateTime EndDate
        {
            get;
            set;
        }

        public decimal Amount
        {
            get;
            set;
        }

        public decimal Premium
        {
            get;
            set;
        }

        public ContractStatus Status
        {
            get;
            set;
        }

        public PaymentOrder PaymentOrder
        {
            get;
            set;
        }
    }
}
