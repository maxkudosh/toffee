﻿using System.Collections.Generic;

namespace Business.ViewModels
{
    public class ContractApproval
    {
        public List<Contract> ContractVariants
        {
            get;
            set;
        }

        public InsuranceApplication InsuranceApplication
        {
            get;
            set;
        }
    }
}
