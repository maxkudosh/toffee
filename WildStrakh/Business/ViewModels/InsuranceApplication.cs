﻿using System;
using Domain.Entities.Dictionary;

namespace Business.ViewModels
{
    public class InsuranceApplication
    {
        public Guid Id
        {
            get;
            set;
        }

        public InsuranceType InsuranceType
        {
            get;
            set;
        }

        public decimal? InsuranceSum
        {
            get;
            set;
        }

        public PaymentOrder? PaymentOrder
        {
            get;
            set;
        }

        public Applicant Applicant
        {
            get;
            set;
        }

        public Person Beneficiary
        {
            get;
            set;
        }

        public DocumentStatus Status
        {
            get;
            set;
        }
    }
}
