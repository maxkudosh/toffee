﻿using System.ComponentModel.DataAnnotations;

namespace Business.ValidationAttributes
{
    public class PositiveNumberAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            if (value == null)
            {
                return false;
            }

            decimal number;
            if (!decimal.TryParse(value.ToString(), out number))
            {
                return false;
            }
            return number > 0;
        }

        protected override ValidationResult IsValid(object value, ValidationContext context)
        {
            return IsValid(value) ? ValidationResult.Success : new ValidationResult("");
        }
    }
}
