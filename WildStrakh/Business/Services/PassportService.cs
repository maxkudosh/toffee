﻿using System.Collections.Generic;
using System.Text;
using Business.Contracts;
using Business.ViewModels;
using Common.Cache;
using Newtonsoft.Json;

namespace Business.Services
{
    public class PassportService : IPassportService
    {

        private readonly SearchableCache<PassportSeries> _cache;

        public PassportService(SearchableCacheFactory cacheFactory)
        {
            _cache = cacheFactory.Create(ParsePassportsJsonFile);
        }

        public List<PassportSeries> GetAllSeries()
            => _cache.GetAll();

        private static List<PassportSeries> ParsePassportsJsonFile()
            =>
                JsonConvert.DeserializeObject<List<PassportSeries>>(
                    Encoding.UTF8.GetString(Common.Properties.Resources.PassportSeries).Remove(0, 1));
    }
}
