﻿using System;
using System.Collections.Generic;
using System.Linq;
using Business.Contracts;
using Business.Mappers;
using Business.Results;
using Data;
using Domain.Entities.Dictionary;
using Domain.Entities.Insurance;
using Domain.Entities.PersonInformation;

namespace Business.Services
{
    public class InsuranceApplicationService : IInsuranceApplicationService
    {
        private readonly UnitOfWork _unitOfWork;

        private readonly IPersonIllnessService _illnessService;

        private readonly IAdditionalInformationService _additionalInformationService;

        public InsuranceApplicationService(UnitOfWork unitOfWork,
            IPersonIllnessService illnessService,
            IAdditionalInformationService additionalInformationService)
        {
            _unitOfWork = unitOfWork;
            _illnessService = illnessService;
            _additionalInformationService = additionalInformationService;
        }

        public VoidResult Apply(Guid userId, Models.InsuranceApplication model)
        {
            var applicant = CreatePersonInformation(userId, model.Applicant);
            AddApplicantSpecificInformation(model.Applicant, applicant);
            var beneficiary = CreatePersonInformation(userId, model.Beneficiary);
            var applicationEntity = CreateInsuranceApplication(model, applicant, beneficiary);
            _unitOfWork.InsuranceApplicationRepository.Insert(applicationEntity);
            _unitOfWork.Save();
            return VoidResult.Success();
        }

        public Result<List<ViewModels.InsuranceApplication>> GetAllByStatus(DocumentStatus status)
        {
            var applications =
                _unitOfWork.InsuranceApplicationRepository.GetByStatus(status).ToList();
            var models = applications.ConvertAll(app =>
            {
                var viewModel = app.Map();
                viewModel.Applicant.Illnesses = _illnessService.GetIllnessesByPersonId(app.Applicant.Id).ToArray();
                return viewModel;
            });
            return Result<List<ViewModels.InsuranceApplication>>.Success(models);
        }

        public Result<List<ViewModels.InsuranceApplication>> GetByUserId(Guid userId)
        {
            var applications =
                _unitOfWork.InsuranceApplicationRepository.GetByUserId(userId).ToList();
            var models = applications.ConvertAll(app =>
            {
                var viewModel = app.Map();
                viewModel.Applicant.Illnesses = _illnessService.GetIllnessesByPersonId(app.Applicant.Id).ToArray();
                return viewModel;
            });
            return Result<List<ViewModels.InsuranceApplication>>.Success(models);
        }

        public Result<ViewModels.InsuranceApplication> GetById(Guid applicationId)
        {
            var result = _unitOfWork.InsuranceApplicationRepository.GetById(applicationId);
            if (result == null)
            {
                return
                    Result<ViewModels.InsuranceApplication>.Failure(
                        "Insurance Application ID is not valid");
            }
            var viewModel = result.Map();
            viewModel.Applicant.Illnesses = _illnessService.GetIllnessesByPersonId(result.Applicant.Id).ToArray();
            return Result<ViewModels.InsuranceApplication>.Success(viewModel);
        }

        public VoidResult UpdateInsuranceApplication(Guid applicationId,
            Models.InsuranceApplication application)
        {
            var entity = _unitOfWork.InsuranceApplicationRepository.GetById(applicationId);
            if (entity == null)
            {
                return VoidResult.Failure("Insurance Application ID is not valid");
            }
            UpdateApplicationFields(application, entity);
            _unitOfWork.InsuranceApplicationRepository.Update(entity);
            _unitOfWork.Save();
            return VoidResult.Success();
        }

        public VoidResult CompleteValidation(Guid documentId)
            => UpdateInsuranceApplicationStatus(documentId, DocumentStatus.Validated);

        public VoidResult RequestInformationCorrection(Models.InformationRequest request)
            =>
                _additionalInformationService.RequestInformationCorrection<InsuranceApplication>(
                    request);

        public VoidResult RequestAdditionalInformation(Models.InformationRequest request)
            =>
                _additionalInformationService.RequestAdditionalInformation<InsuranceApplication>(
                    request);

        public VoidResult RespondInformationCorrection(Guid informationId,
            Models.InformationCorrection response)
            =>
                _additionalInformationService.RespondInformationCorrection<InsuranceApplication>(
                    informationId, response);

        public VoidResult RespondAdditionalInformation(Guid informationId,
            Models.AdditionalInformation response)
            =>
                _additionalInformationService.RespondAdditionalInformation<InsuranceApplication>(
                    informationId, response);

        private static void UpdateApplicationFields(Models.InsuranceApplication model,
            InsuranceApplication entity)
        {
            entity.InsuranceSum = model.InsuranceSum;
            entity.Applicant.Address.City = model.Applicant.Address.City.Name;
            entity.Applicant.Address.District = model.Applicant.Address.City.District;
            entity.Applicant.Address.Region = model.Applicant.Address.City.Region;
            entity.Applicant.Address.AddressLine = model.Applicant.Address.AddressLine;
            entity.Applicant.Address.PostalCode = int.Parse(model.Applicant.Address.PostalCode);
            entity.Applicant.OccupationDetails = model.Applicant.OccupationDetails;
            entity.Applicant.FirstName = model.Applicant.FirstName;
            entity.Applicant.LastName = model.Applicant.LastName;
            entity.Applicant.MiddleName = model.Applicant.MiddleName;
            entity.Applicant.Passport.IdentificationNumber =
                model.Applicant.Passport.IdentificationNumber;
            entity.Applicant.Passport.SerialNumber = int.Parse(model.Applicant.Passport.Number);
            entity.Applicant.Passport.Series = model.Applicant.Passport.Series;
            entity.Applicant.DateOfBirth = model.Applicant.DateOfBirth.Value;
            entity.Beneficiary.FirstName = model.Beneficiary.FirstName;
            entity.Beneficiary.LastName = model.Beneficiary.LastName;
            entity.Beneficiary.MiddleName = model.Beneficiary.MiddleName;
            entity.Beneficiary.Passport.IdentificationNumber =
                model.Beneficiary.Passport.IdentificationNumber;
            entity.Beneficiary.Passport.SerialNumber = int.Parse(model.Beneficiary.Passport.Number);
            entity.Beneficiary.Passport.Series = model.Beneficiary.Passport.Series;
            entity.Beneficiary.DateOfBirth = model.Beneficiary.DateOfBirth.Value;
        }

        private VoidResult UpdateInsuranceApplicationStatus(Guid id, DocumentStatus status)
        {
            var application = _unitOfWork.InsuranceApplicationRepository.GetById(id);
            if (application == null)
            {
                return VoidResult.Failure("Insurance Application ID is not valid");
            }
            application.Status = status;
            application.StatusChangedDate = DateTime.Now;
            _unitOfWork.InsuranceApplicationRepository.Update(application);
            _unitOfWork.Save();
            return VoidResult.Success();
        }

        private Person CreatePersonInformation(Guid userId, Models.Person model)
        {
            var applicant = model.Map();
            applicant.User =
                _unitOfWork.GetUser(user => user.Id == userId.ToString()).FirstOrDefault();
            return applicant;
        }

        private void AddApplicantSpecificInformation(Models.Applicant model, Person applicant)
        {
            applicant.Address = model.Address.Map();
            applicant.Occupation = model.Occupation;
            applicant.OccupationDetails = model.OccupationDetails;
            _illnessService.InsertIllnesses(model, applicant);
        }

        private static InsuranceApplication CreateInsuranceApplication(
            Models.InsuranceApplication model, Person applicant, Person beneficiary)
        {
            var entity = model.Map();
            entity.Applicant = applicant;
            entity.Beneficiary = beneficiary;
            entity.ContractVariants = new List<Contract>();
            return entity;
        }
    }
}
