﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Business.Contracts;
using Business.Mappers;
using Domain.Entities.PersonInformation;
using Data;
using Newtonsoft.Json;

namespace Business.Services
{
    public class PersonIllnessService : IPersonIllnessService
    {
        private readonly UnitOfWork _unitOfWork;

        public PersonIllnessService(UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public List<ViewModels.Illness> GetIllnesses()
            => _unitOfWork.IllnessRepository.Get().Select(illness => illness.Map()).ToList();

        public List<ViewModels.Illness> GetIllnessesFromInitialDataFile()
            =>
                JsonConvert.DeserializeObject<List<ViewModels.Illness>>(
                    Encoding.UTF8.GetString(Common.Properties.Resources.Illness).Remove(0, 1));

        public void InsertIllnesses(Models.Applicant model, Person applicant)
        {
            foreach (var personIllness in model.Illnesses)
            {
                _unitOfWork.PersonIllnessRepository.Insert(CreatePersonIllnessEntity(personIllness,
                    applicant));
            }
        }

        public List<ViewModels.PersonIllness> GetIllnessesByPersonId(Guid id)
            => _unitOfWork.PersonIllnessRepository.GetByPersonId(id).Select(pi => pi.Map()).ToList();

        private PersonIllness CreatePersonIllnessEntity(Models.PersonIllness personIllness,
            Person applicant) => new PersonIllness
            {
                Id = Guid.NewGuid(),
                Person = applicant,
                IllnessType = personIllness.IllnessType.Value,
                Illness = _unitOfWork.IllnessRepository.Get(personIllness.Id)
            };
    }
}
