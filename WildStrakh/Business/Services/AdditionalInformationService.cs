﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Business.Contracts;
using Business.Mappers;
using Business.Results;
using Data;
using Domain.Entities.Dictionary;
using Domain.Entities.Insurance;

namespace Business.Services
{
    public class AdditionalInformationService : IAdditionalInformationService
    {
        private readonly UnitOfWork _unitOfWork;

        public AdditionalInformationService(UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public Result<ViewModels.AdditionalInformation> GetByRequestId(Guid id)
        {
            var entity = _unitOfWork.AdditionalInformationRepository.GetById(id);
            return entity == null
                ? Result<ViewModels.AdditionalInformation>.Failure("Information ID is not valid")
                : Result<ViewModels.AdditionalInformation>.Success(entity.Map());
        }

        public Result<List<ViewModels.AdditionalInformation>> GetAllByUserId(Guid userId,
            InformationType type)
            =>
                !_unitOfWork.PersonRepository.Get(p => p.User.Id == userId.ToString()).Any()
                    ? Result<List<ViewModels.AdditionalInformation>>.Failure("User ID is not valid")
                    : Result<List<ViewModels.AdditionalInformation>>.Success(
                        GetAllByUserId<InsuranceApplication>(userId, type).Value.Concat(
                            GetAllByUserId<InsuranceCaseNotice>(userId, type).Value).Concat(
                                GetAllByUserId<TerminationApplication>(userId, type).Value).ToList());

        public Result<List<ViewModels.AdditionalInformation>> GetAllByUserId<T>(Guid userId,
            InformationType type) where T : StatementDocument
        {
            var user = _unitOfWork.GetUser(u => u.Id == userId.ToString());
            return user == null
                    ? Result<List<ViewModels.AdditionalInformation>>.Failure("User ID is not valid")
                    : Result<List<ViewModels.AdditionalInformation>>.Success(
                        _unitOfWork.AdditionalInformationRepository.Get().Where(
                            info =>
                                GetRelatedEntityPropertyValue<T>(info)?.Person.User.Id ==
                                    userId.ToString() && info.InformationType == type).Select(
                                        info => info.Map()).ToList());
        }

        public VoidResult RequestInformationCorrection<T>(Models.InformationRequest request)
            where T : StatementDocument
            =>
                RequestInformation<T>(request, DocumentStatus.PendingCorrections,
                    InformationType.InformationCorrection);

        public VoidResult RequestAdditionalInformation<T>(Models.InformationRequest request)
            where T : StatementDocument
            =>
                RequestInformation<T>(request, DocumentStatus.PendingAdditionalInformation,
                    InformationType.AdditionalInformation);

        public VoidResult RespondInformationCorrection<T>(Guid informationId,
            Models.InformationCorrection response) where T : StatementDocument
        {
            if (_unitOfWork.AdditionalInformationRepository.GetById(informationId) == null)
            {
                return VoidResult.Failure("Information correction ID is not valid");
            }
            var entity = GetAdditionalInformationWithResponseProperties<T>(informationId, response,
                DocumentStatus.PendingValidation);
            _unitOfWork.AdditionalInformationRepository.Update(entity);
            _unitOfWork.Save();
            return VoidResult.Success();
        }

        public VoidResult RespondAdditionalInformation<T>(Guid informationId,
            Models.AdditionalInformation response) where T : StatementDocument
        {
            if (_unitOfWork.AdditionalInformationRepository.GetById(informationId) == null)
            {
                return VoidResult.Failure("Additional Information ID is not valid");
            }
            var entity = GetAdditionalInformationWithResponseProperties<T>(informationId, response,
                DocumentStatus.PendingApproval);
            entity.Attachments =
                response.Attachments?.Select(
                    attachmentModel => CreateAttachment(attachmentModel, entity)).ToList();
            _unitOfWork.AdditionalInformationRepository.Update(entity);
            _unitOfWork.Save();
            return VoidResult.Success();
        }

        public VoidResult RespondAdditionalInformation(Guid informationId,
            Models.AdditionalInformation response)
        {
            var entity = _unitOfWork.AdditionalInformationRepository.GetById(informationId);

            if (entity == null)
            {
                return VoidResult.Failure("Additional Information ID is not valid");
            }

            if (entity.InsuranceApplication != null)
            {
                return RespondAdditionalInformation<InsuranceApplication>(informationId, response);
            }
            if (entity.InsuranceCaseNotice != null)
            {
                return RespondAdditionalInformation<InsuranceCaseNotice>(informationId, response);
            }
            if (entity.TerminationApplication != null)
            {
                return RespondAdditionalInformation<TerminationApplication>(informationId, response);
            }
            return VoidResult.Failure("Additional Information is not valid");
        }

        public Result<List<ViewModels.AdditionalInformation>> GetByDocumentId<T>(Guid documentId,
            InformationType type) where T : StatementDocument
            =>
                _unitOfWork.Repository<T>().Get(d => d.Id == documentId).FirstOrDefault() == null
                    ? Result<List<ViewModels.AdditionalInformation>>.Failure(
                        "Document ID is not valid")
                    : Result<List<ViewModels.AdditionalInformation>>.Success(
                        _unitOfWork.AdditionalInformationRepository.Get().Where(
                            info =>
                                GetRelatedEntityPropertyValue<T>(info)?.Id == documentId &&
                                    info.InformationType == type).Select(info => info.Map()).ToList());

        public Result<ViewModels.AttachmentContent> GetAttachmentContent(Guid id)
        {
            var result = _unitOfWork.AttachmentRepository.Get(att => att.Id == id).FirstOrDefault();
            return result == null
                ? Result<ViewModels.AttachmentContent>.Failure("Attachment ID is not valid")
                : Result<ViewModels.AttachmentContent>.Success(result.MapContent());
        }

        private static Attachment CreateAttachment(Models.Attachment model,
            AdditionalInformation additionalInformation)
        {
            var attachment = model.Map();
            attachment.AdditionalInformation = additionalInformation;
            attachment.File.Type = UserFileType.Attachment;
            return attachment;
        }

        private AdditionalInformation GetAdditionalInformationWithResponseProperties<T>(
            Guid informationId, Models.InformationCorrection response, DocumentStatus status)
            where T : StatementDocument
        {
            var entity = _unitOfWork.AdditionalInformationRepository.GetById(informationId);
            entity.Response = response.Content;
            entity.ResponseDate = DateTime.Now;
            SetRelatedDocumentWithStatus<T>(entity, status);
            return entity;
        }

        private VoidResult RequestInformation<T>(Models.InformationRequest request,
            DocumentStatus status, InformationType informationType) where T : StatementDocument
        {
            var entity = request.Map(informationType);
            if (!request.DocumentId.HasValue ||
                !_unitOfWork.Repository<T>().Get(d => d.Id == request.DocumentId.Value).Any())
            {
                return VoidResult.Failure("Document ID is not valid");
            }
            SetRelatedDocumentWithStatus<T>(entity, status, request.DocumentId.Value);
            _unitOfWork.AdditionalInformationRepository.Insert(entity);
            _unitOfWork.Save();
            return VoidResult.Success();
        }

        private static PropertyInfo GetRelatedEntityProperty<T>(AdditionalInformation entity)
            where T : StatementDocument
            => entity.GetType().GetProperties().First(p => p.PropertyType == typeof (T));

        private static T GetRelatedEntityPropertyValue<T>(AdditionalInformation entity)
            where T : StatementDocument => (T) GetRelatedEntityProperty<T>(entity).GetValue(entity);

        private void SetRelatedDocumentWithStatus<T>(AdditionalInformation entity,
            DocumentStatus status, Guid documentId = default(Guid)) where T : StatementDocument
        {
            var document = GetRelatedEntityPropertyValue<T>(entity) ??
                _unitOfWork.Repository<T>().Get(doc => doc.Id == documentId).First();
            document.Status = status;
            document.StatusChangedDate = DateTime.Now;
            GetRelatedEntityProperty<T>(entity).SetValue(entity, document);
            _unitOfWork.Repository<T>().Update(document);
            _unitOfWork.Save();
        }
    }
}
