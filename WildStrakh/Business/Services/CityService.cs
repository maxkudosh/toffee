﻿using System.Collections.Generic;
using System.Text;
using Business.Contracts;
using Business.ViewModels;
using Common.Cache;
using Newtonsoft.Json;

namespace Business.Services
{
    public class CityService : ICityService
    {

        private readonly SearchableCache<City> _cache;

        public CityService(SearchableCacheFactory cacheFactory)
        {
            _cache = cacheFactory.Create(GetCities);
        }

        public List<City> GetAll()
        {
            return _cache.GetAll();
        }

        public List<City> GetWhereNameStartsWith(string s)
        {
            var lowered = s.ToLower();
            return _cache.GetByPredicate(c => c.Name.ToLower().StartsWith(lowered));
        }

        private static List<City> GetCities()
        {
            return
                JsonConvert.DeserializeObject<List<City>>(
                    Encoding.UTF8.GetString(Common.Properties.Resources.City).Remove(0, 1));
        }

    }
}
