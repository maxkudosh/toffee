﻿using System;
using System.Collections.Generic;
using System.Linq;
using Business.Contracts;
using Business.Mappers;
using Business.Results;
using Data;
using Domain.Entities.Dictionary;

namespace Business.Services
{
    public class StatisticsService : IStatisticsService
    {
        private readonly UnitOfWork _unitOfWork;

        public StatisticsService(UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public Result<List<ViewModels.Contract>> GetActiveContracts()
            =>
                Result<List<ViewModels.Contract>>.Success(
                    _unitOfWork.ContractRepository.GetWithActiveStatus().Select(c => c.Map()).ToList
                        ());

        public Result<List<ViewModels.Applicant>> GetClientsWithActiveContracts()
            =>
                Result<List<ViewModels.Applicant>>.Success(
                    _unitOfWork.ContractRepository.GetWithActiveStatus().Select(
                        c => c.Person.MapToApplicant()).ToList());

        public Result<List<ViewModels.Transaction>> GetCompletedTransactions(DateTime startDate,
            DateTime endDate)
            =>
                Result<List<ViewModels.Transaction>>.Success(
                    _unitOfWork.TransactionRepository.Get(
                        t =>
                            t.TransactionDate >= startDate && t.TransactionDate <= endDate &&
                                t.Status == TransactionStatus.Completed).Select(t => t.Map()).ToList
                        ());

        public Result<List<ViewModels.Transaction>> GetAllTransactions()
            =>
                Result<List<ViewModels.Transaction>>.Success(
                    _unitOfWork.TransactionRepository.Get().Select(t => t.Map()).ToList());

        public Result<List<ViewModels.Account>> GetAccounts()
            =>
                Result<List<ViewModels.Account>>.Success(
                    _unitOfWork.AccountRepository.Get().Select(a => a.Map()).ToList());
    }
}
