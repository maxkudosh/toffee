﻿using System;
using System.Collections.Generic;
using System.Linq;
using Business.Contracts;
using Business.Mappers;
using Business.Results;
using Data;
using Domain.Entities.Dictionary;
using Domain.Entities.Insurance;

namespace Business.Services
{
    public class InsuranceCaseNoticeService : IInsuranceCaseNoticeService
    {
        private readonly UnitOfWork _unitOfWork;

        private readonly IAdditionalInformationService _additionalInformationService;

        private readonly IBankService _bankService;

        public InsuranceCaseNoticeService(UnitOfWork unitOfWork,
            IAdditionalInformationService additionalInformationService,
            IBankService bankService)
        {
            _unitOfWork = unitOfWork;
            _additionalInformationService = additionalInformationService;
            _bankService = bankService;
        }

        public VoidResult Notify(Models.InsuranceCaseNotice notice)
        {
            if (!notice.ContractId.HasValue)
            {
                return VoidResult.Failure("Incorrect Contract ID");
            }
            var contract = _unitOfWork.ContractRepository.GetById(notice.ContractId.Value);
            if (contract == null)
            {
                return VoidResult.Failure("Incorrect Contract ID");
            }
            var noticeEntity = notice.Map(contract);
            _unitOfWork.InsuranceCaseNoticeRepository.Insert(noticeEntity);
            _unitOfWork.Save();
            return VoidResult.Success();
        }

        public Result<List<ViewModels.InsuranceCaseNotice>> GetAllByStatus(DocumentStatus status)
            =>
                Result<List<ViewModels.InsuranceCaseNotice>>.Success(
                    _unitOfWork.InsuranceCaseNoticeRepository.GetByStatus(status).ToList().Select(
                        app => app.Map()).ToList());

        public Result<List<ViewModels.InsuranceCaseNotice>> GetByUserId(Guid userId)
            =>
                Result<List<ViewModels.InsuranceCaseNotice>>.Success(
                    _unitOfWork.InsuranceCaseNoticeRepository.GetByUserId(userId).Select(
                        app => app.Map()).ToList());

        public Result<ViewModels.InsuranceCaseNotice> GetNotice(Guid noticeId)
        {
            var result = _unitOfWork.InsuranceCaseNoticeRepository.GetById(noticeId);
            return result == null
                ? Result<ViewModels.InsuranceCaseNotice>.Failure("Notice ID is not valid")
                : Result<ViewModels.InsuranceCaseNotice>.Success(result.Map());
        }

        public VoidResult ReviewNotice(Models.NoticeReviewResult reviewResult)
        {
            var notice = _unitOfWork.InsuranceCaseNoticeRepository.GetById(reviewResult.NoticeId);
            if (notice == null)
            {
                return VoidResult.Failure("Notice ID is not valid");
            }
            notice.Status = reviewResult.IsApproved
                ? DocumentStatus.Approved
                : DocumentStatus.Rejected;
            notice.StatusChangedDate = DateTime.Now;
            notice.Payment = reviewResult.Payment;
            _unitOfWork.InsuranceCaseNoticeRepository.Update(notice);
            _unitOfWork.Save();
            ReuqestPayout(reviewResult.NoticeId);
            return VoidResult.Success();
        }

        public VoidResult ReuqestPayout(Guid noticeId)
            =>
                _unitOfWork.InsuranceCaseNoticeRepository.GetById(noticeId) == null
                    ? VoidResult.Failure("Notice ID is not valid")
                    : _bankService.ProcessInstantPaymentToClient(noticeId);

        public VoidResult UpdateNotice(Guid noticeId, Models.InsuranceCaseNotice notice)
        {
            var entity = _unitOfWork.InsuranceCaseNoticeRepository.GetById(noticeId);
            if (entity == null)
            {
                return VoidResult.Failure("Notice ID is not valid");
            }
            UpdateNoticeFields(notice, entity);
            _unitOfWork.InsuranceCaseNoticeRepository.Update(entity);
            _unitOfWork.Save();
            return VoidResult.Success();
        }

        public VoidResult RequestInformationCorrection(Models.InformationRequest request)
            =>
                _additionalInformationService.RequestInformationCorrection<InsuranceCaseNotice>(
                    request);

        public VoidResult RequestAdditionalInformation(Models.InformationRequest request)
            =>
                _additionalInformationService.RequestAdditionalInformation<InsuranceCaseNotice>(
                    request);

        public VoidResult RespondInformationCorrection(Guid informationId,
            Models.InformationCorrection response)
            =>
                _additionalInformationService.RespondInformationCorrection<InsuranceCaseNotice>(
                    informationId, response);

        public VoidResult RespondAdditionalInformation(Guid informationId,
            Models.AdditionalInformation response)
            =>
                _additionalInformationService.RespondAdditionalInformation<InsuranceCaseNotice>(
                    informationId, response);

        public VoidResult CompleteValidation(Guid documentId)
            => UpdateInsuranceCaseNoticeStatus(documentId, DocumentStatus.Validated);

        private static void UpdateNoticeFields(Models.InsuranceCaseNotice notice,
            InsuranceCaseNotice entity)
        {
            entity.Description = notice.Circumstances;
            entity.Place = notice.Whereabouts;
            entity.ActualDate = notice.IncidentDate.Value;
        }

        private VoidResult UpdateInsuranceCaseNoticeStatus(Guid documentId, DocumentStatus status)
        {
            var notice = _unitOfWork.InsuranceCaseNoticeRepository.GetById(documentId);
            if (notice == null)
            {
                return VoidResult.Failure("Notice ID is not valid");
            }
            notice.Status = status;
            notice.StatusChangedDate = DateTime.Now;
            _unitOfWork.InsuranceCaseNoticeRepository.Update(notice);
            _unitOfWork.Save();
            return VoidResult.Success();
        }
    }
}
