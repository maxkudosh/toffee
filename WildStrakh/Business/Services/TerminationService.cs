﻿using System;
using System.Collections.Generic;
using System.Linq;
using Business.Contracts;
using Business.Mappers;
using Business.Results;
using Business.ViewModels;
using Data;
using Domain.Entities.Dictionary;
using Domain.Entities.Insurance;

namespace Business.Services
{
    public class TerminationService : ITerminationService
    {
        private readonly UnitOfWork _unitOfWork;

        private readonly IAdditionalInformationService _additionalInformationService;

        public TerminationService(UnitOfWork unitOfWork,
            IAdditionalInformationService additionalInformationService)
        {
            _unitOfWork = unitOfWork;
            _additionalInformationService = additionalInformationService;
        }

        public VoidResult Apply(Models.TerminationApplication application)
        {
            if (!application.ContractId.HasValue)
            {
                return VoidResult.Failure("Contract ID is not valid");
            }
            var contract = _unitOfWork.ContractRepository.GetById(application.ContractId.Value);
            var terminationEntity = application.Map(contract);
            _unitOfWork.TerminationApplicationRepository.Insert(terminationEntity);
            _unitOfWork.Save();
            return VoidResult.Success();
        }

        public Result<List<Termination>> GetAllByStatus(DocumentStatus status)
            =>
                Result<List<Termination>>.Success(
                    _unitOfWork.TerminationApplicationRepository.GetByStatus(status).ToList().Select
                        (app => app.Map()).ToList());

        public Result<List<Termination>> GetByUserId(Guid userId)
            =>
                Result<List<Termination>>.Success(
                    _unitOfWork.TerminationApplicationRepository.GetByUserId(userId).Select(
                        app => app.Map()).ToList());

        public Result<Termination> GetApplication(Guid id)
        {
            var result = _unitOfWork.TerminationApplicationRepository.GetById(id);
            return result == null
                ? Result<Termination>.Failure("Termination Application ID is not valid")
                : Result<Termination>.Success(result.Map());
        }

        public VoidResult Reject(Guid applicationId)
        {
            var application = _unitOfWork.TerminationApplicationRepository.GetById(applicationId);
            if (application == null)
            {
                return VoidResult.Failure("Termination Application ID is not valid");
            }
            _unitOfWork.TerminationApplicationRepository.UpdateWithStatus(application,
                DocumentStatus.Rejected);
            _unitOfWork.Save();
            return VoidResult.Success();
        }

        public VoidResult Terminate(Guid applicationId)
        {
            var application = _unitOfWork.TerminationApplicationRepository.GetById(applicationId);
            if (application == null)
            {
                return VoidResult.Failure("Termination Application ID is not valid");
            }
            _unitOfWork.TerminationApplicationRepository.UpdateWithStatus(application,
                DocumentStatus.Approved);
            var contract = _unitOfWork.ContractRepository.GetById(application.Contract.Id);
            // need to fix some magic crushes
            contract.InsuranceApplication = contract.InsuranceApplication;
            contract.Status = ContractStatus.Terminated;
            contract.StatusChangedDate = DateTime.Now;
            _unitOfWork.ContractRepository.Update(contract);
            _unitOfWork.Save();
            return VoidResult.Success();
        }

        public VoidResult UpdateTerminationApplication(Guid applicationId,
            Models.TerminationApplication application)
        {
            var entity = _unitOfWork.TerminationApplicationRepository.GetById(applicationId);
            if (entity == null)
            {
                return VoidResult.Failure("Termination Application ID is not valid");
            }
            UpdateApplicationFields(application, entity);
            _unitOfWork.TerminationApplicationRepository.Update(entity);
            _unitOfWork.Save();
            return VoidResult.Success();
        }

        public VoidResult RequestInformationCorrection(Models.InformationRequest request)
            =>
                _additionalInformationService.RequestInformationCorrection<TerminationApplication>(
                    request);

        public VoidResult RequestAdditionalInformation(Models.InformationRequest request)
            =>
                _additionalInformationService.RequestAdditionalInformation<TerminationApplication>(
                    request);

        public VoidResult RespondInformationCorrection(Guid informationId,
            Models.InformationCorrection response)
            =>
                _additionalInformationService.RespondInformationCorrection<TerminationApplication>(
                    informationId, response);

        public VoidResult RespondAdditionalInformation(Guid informationId,
            Models.AdditionalInformation response)
            =>
                _additionalInformationService.RespondAdditionalInformation<TerminationApplication>(
                    informationId, response);

        public VoidResult CompleteValidation(Guid documentId)
            => UpdateTerminationApplicationStatus(documentId, DocumentStatus.Validated);

        private static void UpdateApplicationFields(Models.TerminationApplication application,
            TerminationApplication entity)
        {
            entity.Comment = application.Comment;
        }

        private VoidResult UpdateTerminationApplicationStatus(Guid documentId, DocumentStatus status)
        {
            var application = _unitOfWork.TerminationApplicationRepository.GetById(documentId);
            if (application == null)
            {
                return VoidResult.Failure("Termination Application ID is not valid");
            }
            application.Status = status;
            application.StatusChangedDate = DateTime.Now;
            _unitOfWork.TerminationApplicationRepository.Update(application);
            _unitOfWork.Save();
            return VoidResult.Success();
        }
    }
}
