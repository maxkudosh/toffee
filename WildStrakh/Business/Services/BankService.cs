﻿using System;
using Business.Contracts;
using Business.Mappers;
using Business.Results;
using Common;
using Data;
using Domain.Entities.Bank;
using Domain.Entities.Dictionary;
using Domain.Entities.Insurance;

namespace Business.Services
{
    public class BankService : IBankService
    {
        private readonly UnitOfWork _unitOfWork;

        public BankService(UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public void CreatePersonalAccount(Contract contract, Models.BankCard card)
        {
            var account = contract.Map(card);
            _unitOfWork.AccountRepository.Insert(account);
            _unitOfWork.Save();
        }

        public VoidResult ProcessInstantPaymentToSystem(Contract contract)
        {
            var personAccount = _unitOfWork.AccountRepository.GetByPersonId(contract.Person.Id);
            if (personAccount == null)
            {
                return VoidResult.Failure("Person ID is not valid");
            }
            if (personAccount.Balance - contract.Premium < 0)
            {
                return VoidResult.Failure(Constants.NotEnoughMoneyMessage);
            }
            var systemAccount = _unitOfWork.AccountRepository.GetSystemAccount();
            var transaction = CreateTransaction(contract.Premium, personAccount, systemAccount,
                contract, DateTime.Today, TransactionStatus.Waiting);
            systemAccount.Balance += contract.Premium;
            personAccount.Balance -= contract.Premium;
            contract.InsuranceApplication = contract.InsuranceApplication;
            contract.Status = ContractStatus.Active;
            transaction.Status = TransactionStatus.Completed;
            _unitOfWork.TransactionRepository.Insert(transaction);
            _unitOfWork.ContractRepository.Update(contract);
            _unitOfWork.AccountRepository.Update(systemAccount);
            _unitOfWork.AccountRepository.Update(personAccount);
            _unitOfWork.Save();
            return VoidResult.Success();
        }

        public VoidResult ProcessInstantPaymentToClient(Guid noticeId)
        {
            var notice = _unitOfWork.InsuranceCaseNoticeRepository.GetById(noticeId);
            if (notice == null)
            {
                return VoidResult.Failure("Notice ID is not valid");
            }
            var systemAccount = _unitOfWork.AccountRepository.GetSystemAccount();
            var personAccount =
                _unitOfWork.AccountRepository.GetByPersonId(notice.Contract.Person.Id);
            if (personAccount == null)
            {
                return VoidResult.Failure("Person ID is not valid");
            }
            var transaction = CreateTransaction(notice.Payment.Value, systemAccount, personAccount,
                notice.Contract, DateTime.Today, TransactionStatus.Completed);
            systemAccount.Balance -= notice.Payment.Value;
            personAccount.Balance += notice.Payment.Value;
            notice.Status = DocumentStatus.Closed;
            _unitOfWork.TransactionRepository.Insert(transaction);
            _unitOfWork.InsuranceCaseNoticeRepository.Update(notice);
            _unitOfWork.AccountRepository.Update(systemAccount);
            _unitOfWork.AccountRepository.Update(personAccount);
            _unitOfWork.Save();
            return VoidResult.Success();
        }

        public VoidResult ProcessScheduledPayments()
        {
            var systemAccount = _unitOfWork.AccountRepository.GetSystemAccount();
            var transactions =
                _unitOfWork.TransactionRepository.GetCurrentPeriodTransactionsToProcess();
            foreach (var transaction in transactions)
            {
                var personAccount = transaction.SourceAccount;
                if (personAccount.Balance - transaction.Amount < 0)
                {
                    transaction.PlannedDate = transaction.PlannedDate.AddMonths(1);
                }
                else
                {
                    systemAccount.Balance += transaction.Amount;
                    personAccount.Balance -= transaction.Amount;
                    transaction.Status = TransactionStatus.Completed;
                    transaction.TransactionDate = DateTime.Now;
                    _unitOfWork.AccountRepository.Update(systemAccount);
                    _unitOfWork.AccountRepository.Update(personAccount);
                }
                _unitOfWork.TransactionRepository.Update(transaction);
            }
            _unitOfWork.Save();
            return VoidResult.Success();
        }

        public VoidResult PlanPeriodicPayments(Contract contract)
        {
            var systemAccount = _unitOfWork.AccountRepository.GetSystemAccount();
            var personAccount = _unitOfWork.AccountRepository.GetByPersonId(contract.Person.Id);
            if (personAccount == null)
            {
                return VoidResult.Failure("Person ID is not valid");
            }
            var currentDate = contract.StartDate;
            while (currentDate <= contract.EndDate)
            {
                currentDate = contract.PaymentOrder == PaymentOrder.Monthly
                    ? currentDate.AddMonths(1)
                    : currentDate.AddYears(1);
                var transaction = CreateTransaction(contract.Premium, personAccount, systemAccount,
                    contract, currentDate, TransactionStatus.Waiting);
                _unitOfWork.TransactionRepository.Insert(transaction);
            }
            contract.PaymentsPlanned = true;
            contract.InsuranceApplication = contract.InsuranceApplication;
            _unitOfWork.ContractRepository.Update(contract);
            _unitOfWork.Save();
            return VoidResult.Success();
        }

        private static Transaction CreateTransaction(decimal amount, Account source, Account target,
            Contract contract, DateTime plannedDate, TransactionStatus status) => new Transaction
            {
                Id = Guid.NewGuid(),
                Amount = amount,
                TransactionDate = DateTime.Now,
                Type = contract.PaymentOrder,
                TargetAccount = target,
                SourceAccount = source,
                PlannedDate = plannedDate,
                Status = status
            };
    }
}
