﻿using System.Collections.Generic;
using System.Linq;
using Business.Contracts;
using Business.Mappers;
using Business.ViewModels;
using Common.Cache;
using Data;

namespace Business.Services
{
    public class IllnessService : IIllnessService
    {
        private readonly UnitOfWork _unitOfWork;
        private readonly SearchableCache<Illness> _cache;

        public IllnessService(UnitOfWork unitOfWork, SearchableCacheFactory cacheFactory)
        {
            _unitOfWork = unitOfWork;
            _cache = cacheFactory.Create(GetIllnesses);
        }

        public List<Illness> GetAll()
        {
            return _cache.GetAll();
        }

        public List<Illness> GetWhereNameStartWith(string s)
        {
            var lowered = s.ToLower();
            return _cache.GetByPredicate(i => i.Name.ToLower().StartsWith(lowered));
        }

        private List<Illness> GetIllnesses()
        {
            return _unitOfWork.IllnessRepository.Get().Select(illness => illness.Map()).ToList();
        }
    }
}
