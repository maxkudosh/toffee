﻿using System;
using System.Collections.Generic;
using System.Linq;
using Business.Contracts;
using Business.Mappers;
using Business.Results;
using Common;
using Data;
using Domain.Attributes;
using Domain.Entities.Dictionary;
using Domain.Entities.Insurance;
using Domain.Entities.PersonInformation;
using Domain.Questions;

namespace Business.Services
{
    public class ContractService : IContractService
    {
        private readonly UnitOfWork _unitOfWork;

        private readonly IBankService _bankService;

        private readonly List<IQuestion> _questions = new List<IQuestion>
        {
            new IllnessSeverityQuestion(),
            new OccupationDangerQuestion()
        };

        public ContractService(UnitOfWork unitOfWork, IBankService bankService)
        {
            _unitOfWork = unitOfWork;
            _bankService = bankService;
        }

        public Result<ViewModels.ContractApproval> GetContractVariants(Guid insuranceApplicationId)
        {
            var application =
                _unitOfWork.InsuranceApplicationRepository.GetById(insuranceApplicationId);
            return application == null
                ? Result<ViewModels.ContractApproval>.Failure(
                    "Insurance Application ID is not valid")
                : Result<ViewModels.ContractApproval>.Success(
                    application.ContractVariants.Select(v => v.Map()).Map(application.Map()));
        }

        public VoidResult ApproveContract(Models.Contract model)
        {
            var contractEntity = _unitOfWork.ContractRepository.GetById(model.Id);
            if (contractEntity == null)
            {
                return VoidResult.Failure("Contract ID is not valid");
            }
            var insuranceApplication = GetInsuranceApplicationByContract(contractEntity);
            UpdateApprovedInsuranceApplication(insuranceApplication, contractEntity);
            model.MapToApproved(contractEntity);
            UpdateInsuranceApplication(insuranceApplication, DocumentStatus.Approved);
            contractEntity.InsuranceApplication = contractEntity.InsuranceApplication;
            _unitOfWork.ContractRepository.Update(contractEntity);
            _unitOfWork.Save();
            return VoidResult.Success();
        }

        public VoidResult RejectInsuranceApplication(Guid insuranceApplicationId)
        {
            var application =
                _unitOfWork.InsuranceApplicationRepository.GetById(insuranceApplicationId);
            if (application == null)
            {
                return VoidResult.Failure("Insurance Application ID is not valid");
            }
            foreach (var contract in application.ContractVariants)
            {
                contract.Status = ContractStatus.Cancelled;
                contract.StatusChangedDate = DateTime.Now;
            }
            _unitOfWork.InsuranceApplicationRepository.UpdateWithStatus(application,
                DocumentStatus.Rejected);
            _unitOfWork.Save();
            return VoidResult.Success();
        }

        public VoidResult MakeFirstPayment(Guid contractId, Models.BankCard card)
        {
            var contract = _unitOfWork.ContractRepository.GetById(contractId);
            if (contract == null)
            {
                return VoidResult.Failure("Contract ID is not valid");
            }
            _bankService.CreatePersonalAccount(contract, card);
            if (contract.PaymentOrder != PaymentOrder.Once && !contract.PaymentsPlanned)
            {
                _bankService.PlanPeriodicPayments(contract);
            }
            return _bankService.ProcessInstantPaymentToSystem(contract);
        }

        public VoidResult RejectCreatedContract(Guid contractId)
        {
            var contract = _unitOfWork.ContractRepository.GetById(contractId);
            contract.Status = ContractStatus.Cancelled;
            contract.StatusChangedDate = DateTime.Now;
            contract.InsuranceApplication = contract.InsuranceApplication;
            _unitOfWork.ContractRepository.Update(contract);
            _unitOfWork.Save();
            return VoidResult.Success();
        }

        public Result<ViewModels.ContractPdf> GetContract(Guid contractId)
        {
            var contract = _unitOfWork.ContractRepository.GetById(contractId);
            return contract == null
                ? Result<ViewModels.ContractPdf>.Failure("Contract ID is not valid")
                : Result<ViewModels.ContractPdf>.Success(new ViewModels.ContractPdf
                {
                    Contract = contract.Map(),
                    Beneficiary = contract.InsuranceApplication.Beneficiary.MapToPerson()
                });
        }

        public Result<List<ViewModels.Contract>> GetContractsByUserId(Guid userId)
            =>
                Result<List<ViewModels.Contract>>.Success(
                    _unitOfWork.ContractRepository.GetByUserId(userId).Where(
                        c =>
                            c.InsuranceApplication.Contract != null &&
                                c.InsuranceApplication.Contract.Id == c.Id).Select(c => c.Map())
                        .ToList());

        public Result<ViewModels.QuestionsAsking> PrepareQuestions(Guid insuranceApplicationId)
        {
            var applicationEntity =
                _unitOfWork.InsuranceApplicationRepository.GetById(insuranceApplicationId);
            if (applicationEntity == null)
            {
                return
                    Result<ViewModels.QuestionsAsking>.Failure(
                        "Insurance Application ID is not valid");
            }
            if (applicationEntity.Status != DocumentStatus.Processed)
            {
                applicationEntity.StatusChangedDate = DateTime.Now;
                applicationEntity.Status = DocumentStatus.Processed;
            }
            var model = CreateQuestionsAsking(applicationEntity);
            _unitOfWork.InsuranceApplicationRepository.Update(applicationEntity);
            _unitOfWork.Save();
            return Result<ViewModels.QuestionsAsking>.Success(model);
        }

        public VoidResult GenerateContractCandidates(Models.QuestionsAnswering answering)
        {
            var application =
                _unitOfWork.InsuranceApplicationRepository.GetById(
                    answering.InsuranceApplicationId.Value);
            if (application == null)
            {
                return VoidResult.Failure("Insurance Application ID is not valid");
            }
            var contracts = CreateContracts(application, answering);
            application.Status = DocumentStatus.PendingApproval;
            application.StatusChangedDate = DateTime.Now;
            application.ContractVariants = contracts;
            foreach (var contract in contracts)
            {
                _unitOfWork.ContractRepository.Insert(contract);
            }
            _unitOfWork.InsuranceApplicationRepository.Update(application);
            _unitOfWork.Save();
            return VoidResult.Success();
        }

        private void UpdateInsuranceApplication(InsuranceApplication application,
            DocumentStatus status)
            => _unitOfWork.InsuranceApplicationRepository.UpdateWithStatus(application, status);

        private InsuranceApplication GetInsuranceApplicationByContract(Contract contract)
            => _unitOfWork.InsuranceApplicationRepository.GetById(contract.InsuranceApplication.Id);


        private static void UpdateApprovedInsuranceApplication(InsuranceApplication application,
            Contract contract)
        {
            foreach (var variant in application.ContractVariants.Except(contract.ConvertToList()))
            {
                variant.Status = ContractStatus.Cancelled;
                variant.StatusChangedDate = DateTime.Now;
            }
            application.Contract = contract;
        }

        private ViewModels.QuestionsAsking CreateQuestionsAsking(InsuranceApplication application)
        {
            var model = application.Map().MapToQuestionsAsking();
            model.Questions =
                _questions.Where(q => q.ShouldAsk(application)).Select(q => q.Map()).ToList();
            return model;
        }

        private List<Contract> CreateContracts(InsuranceApplication application,
            Models.QuestionsAnswering answering) => new List<Contract>
            {
                CreateContract(application, answering, 1),
                CreateContract(application, answering, 2)
            };

        private Contract CreateContract(InsuranceApplication application,
            Models.QuestionsAnswering answering, int variant)
        {
            var contract = application.MapToContract();
            var amountIndex = GetAmountIndex(application.Applicant, answering.Answers);
            contract.Amount = CalculateAmount(amountIndex, application.InsuranceSum.Value);
            contract.EndDate = CalculateEndDate(contract.StartDate,
                CalculateEndDateIndex(application.Applicant.DateOfBirth, application.InsuranceType),
                variant);
            contract.Premium = CalculatePremium(contract.Amount, application.PaymentOrder, variant,
                amountIndex, (int) Math.Round((contract.EndDate - contract.StartDate).TotalDays));
            contract.InsuranceApplication = application;
            return contract;
        }

        private int GetAmountIndex(Person applicant, List<Models.QuestionAnswer> answers)
            =>
                CalculateAmountIndex(applicant.Address, applicant.Occupation,
                    _unitOfWork.PersonIllnessRepository.GetByPersonId(applicant.Id).ToList(),
                    GetAnswer(answers, Constants.OccupationDangerQuestion),
                    GetAnswer(answers, Constants.IllnessSeverityQuestion));

        private static int GetAnswer(IEnumerable<Models.QuestionAnswer> answers, string question)
            =>
                answers.Where(qa => qa.Question == question).Select(qa => qa.Answer).SingleOrDefault
                    ();

        private static int CalculateAmountIndex(Address address, Occupation occupation,
            IReadOnlyCollection<PersonIllness> illnesses, int occupationsIndex, int illnessesIndex)
        {
            var addressPart = string.IsNullOrEmpty(address.Region) ? 10 : 0;
            var occupationPart =
                typeof (Occupation).GetField(occupation.ToString()).IsDefined(
                    typeof (DangerousOccupationAttribute), false)
                    ? 10
                    : 0;
            return addressPart + illnesses.Count + illnessesIndex + occupationsIndex +
                occupationPart;
        }

        private static int CalculateEndDateIndex(DateTime dateOfBirth, InsuranceType insuranceType)
            =>
                (DateTime.Now.Year - dateOfBirth.Year) / 10 +
                    (insuranceType == InsuranceType.Health ? 10 : 0);

        private static decimal CalculatePremium(decimal amount, PaymentOrder paymentOrder,
            int variant, int amountIndex, int monthsCount)
        {
            var premium = amount / 1000 * (amountIndex / 17 + 1) / variant;
            switch (paymentOrder)
            {
                case PaymentOrder.Yearly:
                    return monthsCount / 12 > 1 ? premium / ((decimal) monthsCount / 12) : premium;
                case PaymentOrder.Monthly:
                    return premium / monthsCount;
                case PaymentOrder.Once:
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(paymentOrder), paymentOrder, null);
            }
            return premium;
        }

        private static DateTime CalculateEndDate(DateTime startDate, int endDateIndex, int variant)
            =>
                endDateIndex % 10 < 5
                    ? startDate.AddMonths(12 * variant)
                    : startDate.AddMonths(6 * variant);

        private static decimal CalculateAmount(int amountIndex, decimal insuranceSum)
            =>
                ((amountIndex / 17 > 3
                    ? Constants.StartAmount
                    : Constants.StartAmount * (4 - amountIndex / 17)) + insuranceSum) / 2;
    }
}
