﻿using Quartz;
using Quartz.Impl;

namespace Business.Jobs
{
    public static class JobScheduler
    {
        public static void Start()
        {
            var scheduler = StdSchedulerFactory.GetDefaultScheduler();
            scheduler.Start();
            var job = JobBuilder.Create<TransactionsExecuteJob>().Build();
            var trigger =
                TriggerBuilder.Create().WithDailyTimeIntervalSchedule(
                    builder =>
                        builder.WithIntervalInHours(24).OnEveryDay().StartingDailyAt(
                            TimeOfDay.HourAndMinuteOfDay(0, 0))).Build();
            scheduler.ScheduleJob(job, trigger);
        }
    }
}
