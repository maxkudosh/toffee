﻿using Business.Services;
using Data;
using Quartz;

namespace Business.Jobs
{
    public class TransactionsExecuteJob : IJob
    {
        public void Execute(IJobExecutionContext context)
        {
            var bankService = new BankService(new UnitOfWork(new ApplicationDbContext()));
            bankService.ProcessScheduledPayments();
        }
    }
}
